# CPU processing filters

These filter nodes are processed per frame on the CPU. They are calculated before the rasterizer starts. Most of this filters have build in GLSL shaders.

For CPU processing filters see: [CPU executed functions](.cpu_executed_functions.md)

> :warning: The Lisp subset used for GPU and CPU filters are similar, but differs in detail.

[[_TOC_]]

## Generating patterns
TODO

```clojure
(expr :pattern0 [] ... ; expr + pattern: this a source filter
```

## Floating billboards
Moving billboards in 3D space.
This filter is similar to the billboard example from openFrameworks.
https://github.com/openframeworks/openFrameworks/tree/patch-release/examples/gl/billboardExample

```clojure
(expr :bill0 [] ... ; expr + bill: this a source filter
```

| function | number of<br>parameters | description | available input vars |
| --- | --- | --- | --- |
| count | 1 | number of billboards | f |  |
| rx | 1 | camera rotation around x axis | f |
| ry | 1 | camera rotation around y axis | f |
| rz | 1 | camera rotation around z axis | f |
| x | 1 | billboard x-position | i<br>f |
| y | 1 | billboard y-position | i<br>f |
| z | 1 | billboard z-position | i<br>f |
| size | 1 | billboard size | i<br>f |

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [bill0 ras1] [fdiff2 ras3])
(render)

(expr :bill0 []
  ; TODO pattern and palette is NOT implemented
  ; (load pattern "billboard/ring-alpha.png")
  ; (load palette "billboard/Warm_Colors.gpl")
  (roty f)
  (count (+ 1 (* 1500
                 (usin (/ f 60))
                 (usin (/ f 60)))))
  (x (* 1000 (snoise (* 5 (sin (* 0.00073 f)))
                     (* 5 (sin (* 0.00091 f)))
                     (* i 0.01))))
  (y (* 1000 (snoise (* 5 (sin (* 0.00051 f)))
                     (* 5 (sin (* 0.00019 f)))
                     (* i 0.04))))
  (z (* 1000 (snoise (* 5 (sin (* 0.00023 f)))
                     (* 5 (sin (* 0.00031 f)))
                     (* i 0.09))))
  (size (* 150 (unoise (/ i 1000) 0.1 0.1)))
)
(render 0)

(hsv :ras1 [bill0]
  (h bill0.h)
  (s bill0.s)
  (v (* 2 bill0.v))
)
(render 1)

(expr :fdiff2 [ras1])
(render 2)

(rgb :ras3 [fdiff2 ras3]
  (rgb (mix (* 3 fdiff2.rgb)
            ras3.rgb
            0.98))
)
(render 3)
```

Example video: [yellow blue billboard](https://vimeo.com/494682194)

## Motion Trace
```clojure
(expr :mt0 [] ... ; expr + mt: this a processing filter
```

This filter finds feature points in the incoming data stream and tries to trace this points.

| function | number of<br>parameters | description | available input vars |
| --- | --- | --- | --- |
| mode | 1 | different drawing strategies<br>0, 1, 2 | |
| clear | 1 | when positive clear framebuffer before drawing | |
| count | 1 | number of expected feature points | |
| min | 1 | minimal distance between feature points | |
| qual | 1 | quality<br>0.01 .. 0.25 | |
| trace | 1 | length of the trace |  |
| paint | 1 | when positive paint upstream image data too |  |

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 mt1] [ras2 uspl3])
(movie "4movies/*.mp4")

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
  (sy 1.5)
  (rgb c1.rgb)
)
(render 0)

(expr :mt1 [mov0]
  (mode 1)
  (trace 50)
  (paint -1)
  (count 30)
  (qual 0.01)
  (min 5)
)
(render 1)

(rgb :ras2 [mt1 ras2]
  (rgb (mix (* 16 mt1.rgb)
            ras2.rgb
            0.97))
)
(render 2)

(expr :uspl3 [mt1]
  "cl/processVoronoiPoints.cl"
)
(render 3)

(render)
```
![](./stuff/doc/motion-trace.jpg)


## Gunnar Farneback Optical Flow
```clojure
(expr :ffb1 [] ... ; expr + ffb1: this a processing filter
```

This filter computes the magnitude and direction of optical flow. This is a dense optical flow calculation that needs a high amount of CPU time. Frame rate will drop.

Gunnar Farneback Optical Flow parameters are described in more detail in the [OpenCV API doc](https://docs.opencv.org/3.4/dc/d6b/group__video__track.html#ga5d10ebbd59fe09c5f650289ec0ece5af).

| function | number of<br>parameters | description | available input vars |
| --- | --- | --- | --- |
| clear | 1 | when positive clear framebuffer before drawing | |
| mode | 1 | different drawing strategies 0, 1, 2, 3 | |
| step | 1 | grid cell size used for drawing strategies 0, 1, 2 | |
| r | 1 | radius used for drawing strategies 0, 1, 2 |  |
| s | 1 | saturation used in drawing strategies 1, 2 |  |
| a | 1 | opacity only used in drawing strategy 2 |  |
| paint | 1 | when positive paint upstream image data too |  |
| reduce | 1 | reduce the input image size before calculating the dense optical flow. Will improve the frame rate. |  |
| pscale | 1 | calcOpticalFlowFarneback parameter:<br>pyramid scale | |
| levels | 1 | calcOpticalFlowFarneback parameter:<br>pyramid levels | |
| win | 1 | calcOpticalFlowFarneback parameter:<br>window size | |
| iter | 1 | calcOpticalFlowFarneback parameter:<br>number of iterations at each pyramid level | |
| polyn | 1 | calcOpticalFlowFarneback parameter:<br>pixel neighborhood | |
| polys | 1 | calcOpticalFlowFarneback parameter:<br>standard deviation | |
| gauss | 1 | calcOpticalFlowFarneback parameter:<br>when positive use gaussian filter | |

```clojure
(expr :ffb1 [imgs0]
  (reduce 1)
  (mode 3)
  (gauss -1)
  (levels 2)
  (win 16)
  (iter 4)
  (polyn 7)
  (polys 1.2)
  (r 24)

  (step 3)
)
```
![](./stuff/doc/dense-optical-flow.jpg)


##  Background subtraction
```clojure
(expr :bkg0 [] ... ; expr + bkg: this a processing filter
```

This filter is using background subtraction from OpenCV. The effect depends on the moment when `(expr :bkg...` is triggered.

| function | number of<br>parameters | description | available input vars |
| --- | --- | --- | --- |
| thres | 1 | threshold | |
| learn | 1 | learning time | |

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 mov2 mopr3]
               [bkg1 bkg3 ras4])
(movie "bkgrd/*.MOV")

(hsv :mov0 [c0 c1]
  (sy 2.5)
  (ty 0.1)
  (v (sqr c0.v))
)

(expr :bkg1 [mov0]
  (thres 0.2)
  (learn 1)
  (paint -1)
)

(expr :mopr3 [bkg3]
  "bb"
)

(hsv :mov2 [c0 c1]
  (sy 2.1)
  (ty 0.2)
  (hsv c1.hsv)
)
(render 0)

(expr :bkg3 [mov2]
  (thres 0.1)
  (learn 2)
  (paint -1)
)
(render 1)

(hsv :ras4 [mov0 mopr3]
  (v (- (* 1 mopr3.v) (* 0.5 mov0.v)))
)
(render 5)
(render)
```
![](./stuff/doc/background-subtraction.jpg)


## Contour finder
```clojure
(expr :cline0 [] ... ; expr + cline: this a processing filter
```

This filter analyses the incoming stream to find contours. It can draw contour lines or filled areas.

Image data are converted to gray scale and then processed by OpenCV. OpenCV can retrieve outer and optionally inner boundaries of white (non-zero) connected components in the black (zero) background.

| function | number of<br>parameters | description | available input vars |
| --- | --- | --- | --- |
| count | 1 | estimated number of boundaries |  |
| max | 1 | maximum area size | f<br>u0<br>...<br>u7 |
| min | 1 | minmum area size | f<br>u0<br>...<br>u7 |
| thres | 1 | threshold<br>range 0.0 to 1.0| f<br>u0<br>...<br>u7 |
| fill | 1 | when positive draw filled areas else draw lines | f<br>u0<br>...<br>u7 |
| holes | 1 | when positive search for holes too | f<br>u0<br>...<br>u7 |
| paint | 1 | when positive paint both contour and upstream image | f<br>u0<br>...<br>u7 |

```clojure
(ns lv.demo)
(use 'lv.core)
; see: http://doc.sccode.org/Classes/MFCC.html

(layout "grid" [img0 cline1] [mopr2 ras4])

; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; rotate input image
(hsv :img0 [c0 c1 c2 c3]
  (rot (* 0.01 f))
  (hsv c2.hsv)
)

; find contours
(expr :cline1 [img0]
  (count 250)
  (max 5000)
  (thres (+ 0.35 u6))
  (fill (* -1 u5))
  (paint (- u1 0.1))
  (holes -1)
)

; use dilation to expand 1 pixel wide lines
(expr :mopr2 [cline1]
  "dddb"
)

; a kaleidoscope effect
(rgb :ras4 [mopr2]
  (kaleid (* 27 (- u4 0.3)))
  (rgb mopr2.rgb)
)
; (render 3)
```
![](./stuff/doc/contour-finder-1.png)

## Deforming a mesh
```clojure
(expr :dmesh0 [] ... ; expr + dmesh: this a source filter
```

This filter loads a PLY mesh from file and deform its vertices.
openFrameworks expects that the file will be in the [PLY Format](http://en.wikipedia.org/wiki/PLY_(file_format)). It will only load meshes saved in the PLY ASCII format. The binary format is not supported.

| function | number of<br>parameters | description | available input vars |
| --- | --- | --- | --- |
| mesh | 1 string | path to the file | |
| scale | 1 | scale factor to resize the vertices.<br>used during loading<br>can not be animated | |
| wire | 1 | if positive view maesh as wire frame else show faces.<br>used during loading<br>can not be animated | |
| ambient | 1 | ambient color used in the phong shader | f<br>u0<br>...<br>u7 |
| diffuse | 1 | diffuse color used in the phong shader | f<br>u0<br>...<br>u7 |
| specular | 1 | specular color used in the phong shader | f<br>u0<br>...<br>u7 |
| amp | 1 | amplitude for deforming | f<br>u0<br>...<br>u7 |
| liqu | 1 | liquidness of deforming | f<br>u0<br>...<br>u7 |
| speed | 1 | speed of deforming | f<br>u0<br>...<br>u7 |
| rx | 1 | camera rotation around x axis | f<br>u0<br>...<br>u7 |
| ry | 1 | camera rotation around y axis | f<br>u0<br>...<br>u7 |
| rz | 1 | camera rotation around z axis | f<br>u0<br>...<br>u7 |

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [dmesh0 dmesh1] [mopr2 ras3])
(render)

(expr :dmesh0 []
  (mesh "mesh-deform/noise-scale+3-triangulated.ply")
  (scale 25)
  (ambient 0x0f0f0f)
  (diffuse 0x39bdcc)
  (specular 0xa8eaf1)

  (amp 5)
  (speed 0.5)
  (liqu 0.5)
  (rx (* 0.001 f))
)
(render 0)

(expr :dmesh1 []
  (mesh "mesh-deform/noise-scale+3-triangulated.ply")
  (scale 25)
  (wire -1)
  (ambient 0x0f0f0f)
  (diffuse 0xe5a039)
  (specular 0xe0cdb0)

  (speed 0.5)
  (liqu 0.5)
  (rz (* 0.002 f))./stuff/doc/mesh-deform.jpg)
)
(render 1)

(expr :mopr2 [dmesh0] "d")
(render 2)

(rgb :ras3 [dmesh1 mopr2 ras3]
  (rgb (mix (+ (* 0.7 dmesh1.rgb)
               (* 0.3 mopr2.rgb))
            ras3.rgb
            0.98))
)
(render 3)
```
![](./stuff/doc/mesh-deform.jpg)

## TODO
```clojure
(expr :TODO0 [] ... ; expr + TODO: this a processing filter
```

This filter TODO

| function | number of<br>parameters | description | available input vars |
| --- | --- | --- | --- |
|  |  |  | |
|  |  |  | |
|  |  |  | |
|  |  |  | |
|  |  |  | |
|  |  |  | |

```clojure
```
![](./stuff/doc/TODO.jpg)
