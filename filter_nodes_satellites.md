# Filter nodes for satellites

These filter are sending data to satellites.

Filters for satellites and CPU processing filters are using much the same Lisp subset. see: [CPU executed functions](.cpu_executed_functions.md)

> :warning: The Lisp subset used for GPU and satellites filters are similar, but differs in detail.

[[_TOC_]]

## Output nodes for satellites
TODO

```clojure
(expr :pihat3 [ras1] ... ; expr + pihat: this a output filter sending OSC messages
```
