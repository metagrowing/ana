(
h = OSCFunc({ |msg, time, addr, recvPort|
	[time, addr, recvPort].postln;
	[ "msg  ", msg].postln;
	[ "msg 0 msg 1", msg[0], msg[1]].postln;
	[ "bright 7 13 19 25", msg[7], msg[7+6], msg[7+12], msg[7+18]].postln;
  }, '/f_hsb4c');
)
h.free;