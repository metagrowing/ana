(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1]
               [ras2 ras3])
(render)
; The origin of the coordinate system is
; in the center of the square drawing area.
; The x axis and the y axis runs from -1.0 to +1.0

; All rgb and hsv components have a
; value range between 0.0 and 1.0.

; different strategies to avoid negative color values.
(rgb :ras0 []
  (r (abs (sin (* 17 x))))
  (g (pos? x 0.8 0))
  (b (sqr y))
)

; r is the distance from the origin of the coordinate system.
(hsv :ras1 []
  (v (+ (* 0.5 (cos (* 51 r)))
        (ucos (* 17 r))))
)

; a is the rotation angle around the origin of the coordinate system.
(hsv :ras2 []
  (h (/ a 2 3.14159))
  (s (/ (floor (* 5 r)) 5))
  (v 1)
)

; playing around with noise functions.
(hsv :ras3 []
  (h (snoise x y))
  (s (unoise (* 3 a) (* 3 r)))
  (v 1)
)
