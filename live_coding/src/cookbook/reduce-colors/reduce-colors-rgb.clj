(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1]
               [img2 img3])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; Do not modify the image.
; Copies red green and blue without modification from image c0.
(rgb :img0 [c0 c1 c2 c3]
  (rgb c2.rgb)
)

; work with hsv color space
; to transform a rgb color into its corresponding gray scale
; weigth the contribution of each channel separately.
(hsv :img1 [c0 c1 c2 c3]
  (v (+ (* 0.299 c2.r)
        (* 0.587 c2.g)
        (* 0.114 c2.b)))
)

; work with hsv color space
; use value / brightness from hsv color model
; and add an vignette effect.
; r is the distance from the center.
(hsv :img2 [c0 c1 c2 c3]
  (v (* 1.5 (- 1 r) c2.v))
)

; work with rgb color space
; reduce each rgb channel to only 2 steps.
(rgb :img3 [c0 c1 c2 c3]
  (rgb (/ (floor (* 2 c2.rgb)) 2))
)
