(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1]
               [img2 ras3])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; Do not modify the image.
; Copies red green and blue without modification from image c0.
(rgb :img0 [c0 c1 c2 c3]
  (rgb c0.rgb)
)

; Invert the blue channel.
(rgb :img1 [c0 c1 c2 c3]
  (r c0.r)
  (g c0.g)
  (b (- 1 c0.b))
)

; Mix two images,
; but use different blending calculations for each channel.
(rgb :img2 [c0 c1 c2 c3]
  (r (+ c2.r c0.r))
  (g (* c2.g c0.g))
  (b (- c2.b c0.b))
)

; Mix the output of node img0 and img1.
; Blending is controlled by the green channel of img0
; (img0 = the original unmodified image).
(rgb :ras3 [img0 img1]
  (rgb (mix img0.rgb
            img1.rgb
            img0.g))
)
