(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1]
               [ras2 ras3])
(render)

(rgb :ras0 []
  (r (sin (* x 0.011 f)))
  (g (+ x y (usin (* 0.023 f))))
  (b (sin (* 0.071 f)))
)

(hsv :ras1 []
  (v (* r 2 (usin (* 0.21 f))))
)

(hsv :ras2 [ras0 ras1]
  (s ras0.h)
  (h 0.45)
  (v (pos? (sin (* 0.51 f)) ras0.v ras1.v))
)

(hsv :ras3 [ras0 ras1]
  (h ras0.h)
  (s ras0.s)
  (v (- 1 ras1.v))
)
