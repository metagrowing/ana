(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 vid1] [ras2 ras3])
(render)

(rgb :ras0 [vid1]
  (rot (* 0.01 f))
  (r (distance (vec2 x y) (vec2 0.5 0)))
  (g (distance (vec2 x y) (vec2 0 0.5)))
  (b (distance (vec2 x y) (vec2 0 0)))
)

(hsv :vid1 [c0]
  ; (hsv c0.hsv)
  (hsv (* (vec3 (usin (* 0.01 f)) 1 1) c0.hsv))
)

(rgb :ras0 [vid1]
  (rot (* 0.01 f))
  (r (distance (vec2 x y) (vec2 (sin (* 0.13 f)) 0)))
  (g (distance (vec2 x y) (vec2 0 (sin (* 0.11 f)))))
  (b (distance (vec2 x y) (vec2 0.1 0.3)))
)

(rgb :ras2 [ras0 vid1]
  (kaleid 3)
  (rot (* -0.013 f))
  (rgb (max ras0.rgb vid1.rgb))
  (rgb (mix ras0.rgb vid1.rgb vid1.v))
)

(rgb :ras3 [ras2 ras3]
  (rgb (mix ras2.rgb ras3.rgb 0.9))
)
