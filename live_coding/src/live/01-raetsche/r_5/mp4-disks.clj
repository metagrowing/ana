(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 mov1] [ras2 ras3])
(movie "4movies/*.mp4")

(render 0)
(rgb :ras0 [mov1]
  ; (rot (* 0.01 f))
  (r (distance (vec2 x y) (vec2 0.5 0)))
  ; (g (distance (vec2 x y) (vec2 0 0.5)))
  ; (b (distance (vec2 x y) (vec2 0 0)))
)

(rgb :ras0 [mov1]
  (rot (* 0.01 f))
  (r (distance (vec2 x y) (vec2 (sin (* 0.13 f)) 0)))
  (g (distance (vec2 x y) (vec2 0 (sin (* 0.11 f)))))
  (b (distance (vec2 x y) (vec2 0.1 0.3)))
)

(render 1)
(hsv :mov1 [c0 c1 c2 c3]
  (sy 1.4)
  ; (hsv c0.hsv)
  (hsv (* (vec3 (usin (* 0.01 f)) 1 1) c0.hsv))
)

(render 2)
(rgb :ras2 [ras0 mov1]
  (kaleid 3)
  (rot (* -0.013 f))
  (rgb (max ras0.rgb mov1.rgb))
  (rgb (mix ras0.rgb mov1.rgb mov1.v))
)

(render 3)
(rgb :ras3)
(rgb :ras3 [ras2 ras3]
  (rgb (mix ras2.rgb ras3.rgb 0.8))
)

(render)
