(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 fdiff1 mopr2] [ras3 ras4])

(render)

; generator in hsv color space
; turb = turbulence based on Perlin noise
(hsv :ras0 []
  (h (low? (+ -0.5 y (turb (* 3.31 x) (* 0.013 f) 1)) 0.05 0.05 0.06))
  (s (low? (+ -0.5 y (turb (* 3.13 x) (* 0.013 f) 1)) 0.02 0.25 0.75))
  (v (low? (+ -0.5 y (turb (* 2.97 x) (* 0.011 f) 1)) 0.02 0 1))
)

; difference between this frame and the last frame1
(expr :fdiff1 [ras0])

; morphological operator.
; d = dilate
; e = erode
; b = blur
(expr :mopr2 [fdiff1] "ddebb")

; Feedback loop
; reads from previous buffer ras3 and writes to buffer ras3
; ks7    0.89764
(rgb :ras3 [mopr2 ras3]
  (rot (* r -0.05))
  (sx (* r 2.995))
  (rgb (mix mopr2.rgb ras3.rgb ks7))
)

; mixer in rgb color space
(rgb :ras4 [mopr2 ras3]
  (rgb (+ mopr2.rgb ras3.rgb))
)
