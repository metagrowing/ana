(ns lv.demo)
(use 'lv.core)

(layout "grid" [vid0 fdiff1] [ras2 ras3])
(render)

; read from a WEB camera and shift color hue.
(hsv :vid0 [c0]
  (hsv (* (vec3 (usin (* 0.01 f)) 1 1) c0.hsv))
  ; (hsv c0.hsv)
)

; read from a WEB camera and invert brightness.
(expr :fdiff1 [vid0]
)

; mix the upstream video with the current frame buffer content.
; reads from buffer ras2 and writes the result back to buffer ras2.
(rgb :ras2 [ras3 ras2]
  ; (pixelate-y (* kp0 100))
  (kaleid (* kp1 11))
  (mirror)
  (rgb (mix ras3.rgb ras2.rgb ks0))
)

; mix upsteam video with the own frame buffer content
(rgb :ras3 [fdiff1 vid0 ras3]
  (rgb (mix ras3.rgb fdiff1.rgb (sqrt ks1)))
)
