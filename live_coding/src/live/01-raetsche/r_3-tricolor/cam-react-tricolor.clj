(ns lv.demo)
(use 'lv.core)

(layout "grid" [vid0 ras1] [ras2 ras3])
(sound "sound/Crowander-Arriving_To_Mars.mp3")

(texpr
  (t1 (+ 0 0.1))
)

; read from a WEB camera and shift color hue.
(render 0)
(hsv :vid0 [c0]
  (sy 1.4)
  (hsv c0.hsv)
  (h (+ c0.h (* -1 t1)))
)

(render 1)
(rgb :ras1 [vid0]
  (r vid0.r)
  (tx f1)
  (g vid0.g)
  (tx (* -1 f1))
  (b vid0.b)
)

(render 2)
(hsv :ras2 [ras1]
  (hsv ras1.hsv)
  (h (+ ras1.h t1))
)

(layout "grid" [vid0 ras1] [ras2 cline3])
(render 3)
(expr :cline3 [ras2]
  (count 50)
  (max 2000)
  (thres 0.5)
  (fill -1)
  (names -1)
  (clear (zero? (% f 60) 1 -1))
  ; (clear -1)
  (paint -1)
  (holes 1)
)
; (render)
