(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 ras1] [ras2 ras3])
(movie "4movies/*.mp4")
(render)

(texpr
  (t0 (+ 0 0.02))
  (t1 (+ 0 0.2))
)
; read from a movie file and shift color hue.
(render 0)
(hsv :mov0 [c0 c1 c2 c3]
  (sy 1.4)
  (hsv c2.hsv)
;  (h (+ c2.h (* -1 t1)))
)

(render 1)
(rgb :ras1 [mov0]
  (r mov0.r)
  (ty t0)
  (g mov0.g)
  (ty (* -1 t0))
  (b mov0.b)
)

(render 2)
(hsv :ras2 [ras1]
  (hsv ras1.hsv)
  (h (+ ras1.h t1))
)

(layout "grid" [mov0 ras1] [ras2 cline3])
(render 3)
(expr :cline3 [ras2]
  (count 50)
  (max 2000)
  ; (thres ks7)
  (thres 0.2)
  (fill -1)
  (names -1)
  (clear (zero? (% f 60) 1 -1))
  ; (clear -1)
  (paint -1)
  (holes 1)
)
(render)
