(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 frag1]
               [ras1 ras2])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")

(render 0)
(hsv :img0 [c0 c1 c2 c3]
  ; (hsv (* c0.hsv c2.hsv))
  ; end with c3
  (hsv (* c2.hsv))
)

; mapping the complex plane
; using the whole color wheel
(render 1)
(expr :frag1 []
 "frag/cmplx/cmplx-03-no-upstream.frag"
 (t (* 2 (sin (* 0.01 f))))
)
(expr :frag1 [img0]
 "frag/cmplx/cmplx-03.frag"
 (t (* 2 (sin (* 0.01 f))))
)

(render 3)
; PT-1
(rgb :ras2 [frag1 ras2]
  (rgb (mix frag1.rgb ras2.rgb 0.9))
)
; (render)
