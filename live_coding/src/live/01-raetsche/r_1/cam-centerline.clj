(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 vid1] [ras2 ras3])

(render 0)
(hsv :ras0 [] (v y))
(hsv :ras0 [] (v (low? y 0.1 1 0)))
(hsv :ras0 [] (v (low? (+ (sin (* 0.05 f)) y) 0.1 1 0)))
(hsv :ras0 []
  (tx (* 0.5 (cos (* 0.0131 f))))
  (ty (* 0.5 (sin (* 0.011 f))))

  (v (low? (sin (+ (* 0.0133 f) a))
              0.3
              1
              0))
  ; (v (low? y 0.05 vid1.v 0 ))
)

(render 0)
(hsv :ras0 [vid1]
  (tx (* 0.5 (cos (* 0.0131 f))))
  (ty (* 0.5 (sin (* 0.011 f))))

  (hsv (low? (sin (+ (* 0.0133 f) a))
              0.2
              vid1.hsv
              (vec3 0 0 0)))
)

; read from a WEB camera and shift color hue.
(render 1)
(hsv :vid1 [c0]
  (sy 1.4)
  (hsv c0.hsv)
)

(render 0)
(hsv :ras0 [vid1]
  (tx (* 0.5 (cos (* 0.0131 f))))
  (ty (* 0.5 (sin (* 0.011 f))))

  (hsv (low? (sin (+ (* 0.0133 f) a))
              0.2
              vid1.hsv
              (vec3 0 0 0 )))
  ; (v (low? y 0.05 vid1.v 0 ))
)

(render 2)
(rgb :ras2 [vid1 ras0 ras2]
  (rgb (max (* 0.95 ras2.rgb) ras0.rgb))
)
(rgb :ras2 [vid1 ras0 ras2]
  (rgb (mix (* 0.999 ras2.rgb)
            (* 1.2 ras0.rgb)
            (* 0.2 ks7)))
)
(render)

(sound "sound/Crowander-Arriving_To_Mars.mp3")
(rgb :ras3 [vid1 ras0 ras3]
  (rgb (mix (* 0.99 ras3.rgb)
            (* f0 ras0.rgb)
            ks7))
)
(render 3)
