(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 mov1] [ras2 ras3])
(movie "4movies/*.mp4")


; read from a WEB camera and shift color hue.
(render 0)
(hsv :ras0 [] (v y))
(hsv :ras0 [] (v (low? y 0.1 1 0)))
(hsv :ras0 [] (v (low? (+ (sin (* 0.05 f)) y) 0.1 1 0)))
(hsv :ras0 []
  (tx (* 0.5 (cos (* 0.0131 f))))
  (ty (* 0.5 (sin (* 0.011 f))))

  (v (low? (sin (+ (* 0.0133 f) a))
                0.2
                1
                0))
)
(render 0)
(hsv :ras0 [mov1]
  (tx (* 0.5 (cos (* 0.0131 f))))
  (ty (* 0.5 (sin (* 0.011 f))))
  ; (sx 0.5)

  (hsv (low? (sin (+ (* 0.0133 f) a))
              0.5
              mov1.hsv
              (vec3 0 0 0 )))
)

(render 1)
(hsv :mov1 [c0 c1 c2 c3]
  (sy 1.5)
  (hsv c3.hsv)
)

(render 2)
(rgb :ras2 [mov1 ras0 ras2]
  (rgb (clamp
          (+ (* 0.95 ras2.rgb) (* 0.3 ras0.rgb))
          (vec3 0 0 0)
          (vec3 1 1 1)))
)
(render 3)
(rgb :ras3 [mov1 ras0 ras3]
  (rgb (mix (* 0.99 ras3.rgb) (* 1.1 ras0.rgb) 0.05))
)
(render)
