(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 tile1]
               [tile2 ras3])
(image "Mastodon-#ArtReference/mountains-615428/mountains-615428_1920.jpg")

(rgb :img0 [c0]
	(rgb c0.rgb)
)

(expr :tile1 [img0]
	(x 60)
	(y 60)
)

(expr :tile2 [img0]
	(x 1920)
	(y 30)
	; (x 30)
	; (y 30)
	(comp (- v))
)

(rgb :ras3 [tile1 tile2]
	(rgb (mix tile1.rgb tile2.rgb 0.5))
)
(render 3)
