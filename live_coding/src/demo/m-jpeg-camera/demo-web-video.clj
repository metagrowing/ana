(ns lv.demo)
(use 'lv.core)

(layout "grid" [wvid0 wvid1] [ras3 ras2])
(ipcam
       "http://192.168.2.64:8080/?action=stream" ; local
       "http://212.142.228.68/axis-cgi/mjpg/video.cgi"
       "http://webkamera.overtornea.se/mjpg/video.mjpg?webcam.jpg"
       "http://79.141.146.81/mjpg/video.mjpg"
)
(render)

; read from a Motion JPEG IP camera and increase saturation
(hsv :wvid0 [c0 c1 c2 c3]
  (sx 1.4) (sy 2.6)
  (hsv (* (vec3 1 1.2 1) c1.hsv))
)

; read from two motion jpeg MJPG streams
(rgb :wvid1 [c0 c1 c2 c3]
  (sx 2.2) (sy 2.2)
  (rgb (mix c2.rgb c0.rgb (+ 0.5 y)))
)

(rgb :ras2 [wvid0 wvid1]
  (rgb (+ -0.5 (sqr (pow wvid0.rgb wvid1.rgb))))
)

(rgb :ras3 [wvid0 ras2]
  (rgb (pow (* 1.5 ras2.rgb (+ 0.1 wvid0.rgb)) (vec3 4 4 4)))
)
