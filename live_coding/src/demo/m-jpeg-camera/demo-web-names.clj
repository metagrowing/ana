(ns lv.demo)
(use 'lv.core)

(layout "grid" [wvid0 cline1])
(ipcam
       "http://webkamera.overtornea.se/mjpg/video.mjpg?webcam.jpg"
)
(render)

; read from a Motion JPEG IP camera and increase saturation
(hsv :wvid0 [c0 c1 c2 c3]
  (sx 1.7) (sy 2.6)
  (hsv (* (vec3 1 1.2 1) c1.hsv))
)

; find contours
(expr :cline1 [wvid0]
  (count 250)
  (max 5000)
  (thres 0.5)
  (fill 1)
  (paint -1)
  (holes 1)
)
