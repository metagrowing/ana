(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 ras1]
               [mopr2 frag1])

(movie "4movies/*.mp4")
; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
 (sy 1.5)
 ; (rgb (- 1 c1.rgb))
 (rgb c3.rgb)
)

; (expr :mopr2 [ras1] "eeeeeeeeeeee")
(expr :mopr2 [frag1] "eedeedeedeedeedeed")
; (expr :mopr2 [ras1] "bbbbbbbbbbbb")
; (expr :mopr2 [ras1] "dddddddddddd")

(texpr
  (t5 (+ (* 0.99 t5) (* 0.01 ks5)))
  (t6 (+ (* 0.99 t6) (* 0.01 ks6)))
)
(rgb :ras1 [frag1 mov0]
  ; (mirror)
  ; (inv)
  ; (rot (* 0.01 f))
  ; (rot (* 50 t6))
  ; (kaleid 3)
  (tx (* -1 ks7))(ty (* -1 ks7))
  (sx t5)(sy t5)
  (tx ks7)(ty ks7)
  (rot (* 50 t6))
  (rgb (mix (- 1 frag1.rgb) mov0.rgb ks1))
)

; (hsv :ras3 [])
;
; (hsv :ras3 [ras1]
;   ; (inv)
;   (tx (* -1 ks7))(ty (* -1 ks7))
;   ; (rot ks6)
;   (sx t5)(sy t5)
;   ; (mirror)
;   (tx ks7)(ty ks7)
;   ; (inv)
;   (h ras1.h)
;   (s (* 1.01 ras1.s))
;   (v (* 1.2 ras1.v))
; )

(expr :frag1 [ras1]
 "frag/cmplx/cmplx-03.frag"
 (t (* 2 (sin (* 0.01 f))))
)

(render 2)
