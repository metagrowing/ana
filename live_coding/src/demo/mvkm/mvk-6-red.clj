(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1]
               [ras2 ras3])

(hsv :ras0 []
 (h 0.05)
 (s (- 2 r))
 (v (round (length (vec2 x y))))
)

(texpr
  ; (t5 0)
  (t4 (+ (* 0.99 t4) (* 0.01 ks4)))
  (t5 (+ (* 0.99 t5) (* 0.01 ks5)))
  (t6 (+ (* 0.99 t6) (* 0.01 ks6)))
)

(rgb :ras1 [])
(rgb :ras1 [ras1 ras0]
  (tx (* -1 ks7)) (ty (* -1 ks7))
  (sx (* 1.1 t5))(sy (* 1.1 t5))
  (rot (* 10 t6))
  (tx ks7) (ty ks7)
  (rgb (mix ras1.rgb ras0.rgb ks1))
)

(rgb :ras2 [ras2 ras0]
  (tx (* -1 ks7)) (ty (* -1 ks7))
  (sx (* 1.1 t5))(sy (* 1.1 t5))
  (rot (* -10 t4))
  (tx ks7) (ty ks7)
  ; (rgb (mix (- 0.5 ras1.rgb) ras0.rgb ks1))
  (rgb (+ ras2.rgb ras0.rgb))
)

(rgb :ras3 [ras1 ras2]
  (rgb (mix ras1.rgb ras2.rgb ks0))
)

(render 3)
