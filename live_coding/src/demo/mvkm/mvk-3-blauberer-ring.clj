(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 ras1]
               [mopr2 ras3])

(movie "4movies/*.mp4")
; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
 (sy 1.5)
 ; (rgb (- 1 c1.rgb))
 (rgb c2.rgb)
)

; (expr :mopr2 [ras1] "eeeeeeeeeeee")
(expr :mopr2 [ras1] "eedeedeedeedeedeed")
; (expr :mopr2 [ras1] "bbbbbbbbbbbb")
; (expr :mopr2 [ras1] "dddddddddddd")
; ks0    0.60156
; ks1    0.023438
; ks2    0.99219
; ks3    0.71875
; ks4    0.50781
; ks5    0.85938
; ks6    0.5
; ks7    0

(rgb :ras1 [ras3 mov0]
  (rgb (mix ras3.rgb mov0.rgb ks1))
)

(hsv :ras3 [])

(hsv :ras3 [ras1]
  (tx ks7)(ty ks7)
  ; (rot (* 0.01 f))
  (rot ks6)
  (sx ks5)(sy ks5)
  (tx (* -1 ks7))(ty (* -1 ks7))
  (h ras1.h)
  (s (* 1.01 ras1.s))
  (v (* 1.2 ras1.v))
)

(render 2)
