(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 ras1]
               [mopr2 ras3])

(movie "4movies/*.mp4")
; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
 (sy 1.5)
 ; (rgb (- 1 c1.rgb))
 (rgb c3.rgb)
)

; (expr :mopr2 [ras1] "eeeeeeeeeeee")
(expr :mopr2 [ras1] "eedeedeedeedeedeed")
; (expr :mopr2 [ras1] "bbbbbbbbbbbb")
; (expr :mopr2 [ras1] "dddddddddddd")
; ks0    0.60156
; ks1    0.11719
; ks2    0.99219
; ks3    0.71875
; ks4    0.78906
; ks5    0.84375
; ks6    0.60156
; ks7    0.44531

(texpr
  (t5 (+ (* 0.99 t5) (* 0.01 ks5)))
  (t6 (+ (* 0.99 t6) (* 0.01 ks6)))
)
(rgb :ras1 [ras3 mov0]
  ; (mirror)
  ; (inv)
  ; (rot (* 0.01 f))
  (rot (* 50 t6))
  (rgb (mix ras3.rgb mov0.rgb ks1))
)

(hsv :ras3 [])

(hsv :ras3 [ras1]
  ; (inv)
  (tx (* -1 ks7))(ty (* -1 ks7))
  ; (rot ks6)
  (sx t5)(sy t5)
  ; (mirror)
  (tx ks7)(ty ks7)
  ; (inv)
  (h ras1.h)
  (s (* 1.01 ras1.s))
  (v (* 1.2 ras1.v))
)

(render 3)
