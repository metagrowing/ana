(ns lv.demo)
(use 'lv.core)

(layout "grid" [pl0 ras1])
(render)

(expr :pl0 []
  (count (min 300 f))
  (x (* 2 i (cos (* (+ f i) 2.3998))))
  (y (* 2 i (sin (* (+ f i) 2.3998))))
  (w (* 0.05 (distance 0 0 x y)))
)

(rgb :ras1 [pl0 ras1]
  (rot (* 0.01 f))
  (rgb (mix pl0.rgb ras1.rgb (- 1.1 r)))
  ; (rgb (mix pl0.rgb ras1.rgb 0))
)
