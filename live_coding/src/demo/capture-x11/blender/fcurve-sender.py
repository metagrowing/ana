import bpy
from osc4py3.as_eventloop import *
from osc4py3 import oscbuildparse

osc_startup()
osc_udp_client("127.0.0.1", 57320, "analog_port")

def send_fcurve(scene):
    fcurves = bpy.data.actions["EmptyAction"].fcurves
    u0 = fcurves[0].evaluate(scene.frame_current)
    u1 = fcurves[1].evaluate(scene.frame_current)
    u2 = fcurves[2].evaluate(scene.frame_current)
#    print(str(u0) + " " + str(u1) + " " + str(u2))
    msg = oscbuildparse.OSCMessage("/ana_u0", ",ffff", [u0, u1, u2, scene.frame_current])
    osc_send(msg, "analog_port")
#    msg = oscbuildparse.OSCMessage("/ana_u0", ",f", [u0])
#    osc_send(msg, "analog_port")
#    msg = oscbuildparse.OSCMessage("/ana_u1", ",f", [u1])
#    osc_send(msg, "analog_port")
#    msg = oscbuildparse.OSCMessage("/ana_u2", ",f", [u2])
#    osc_send(msg, "analog_port")
    osc_process()

bpy.app.handlers.frame_change_post.append(send_fcurve)

#bpy.app.handlers.frame_change_post.remove(send_fcurve)
