(ns lv.demo)
(use 'lv.core)

(layout "grid" [win0 ras1 ras4]
               [ras2 ras3])
(render)

; read from a visible X11 window
(expr :win0 []
 ; "analog Not analog"
 "bespoke synth - osc-test-2021-12-06_19-24.bsk"
 ; (i -1)
 (i 8)
 ; (i (* 0.05 f))
)


(hsv :ras1 []
  (h (pos? (- r u1)
           0.5
          (+ 0.3 (* 3 u0))))
  (s 0.5)
  (v (pos? (- r u1) 0.0 0.5))
)

; mix the upstream video with the current frame buffer content.
; reads from buffer ras2 and writes the result back to buffer ras2.
(rgb :ras2 [win0 ras2]
  ; (mirror)
  (kaleid 3)
  (mirror)
  (rot (* 0.005 f))
  (rgb (mix win0.rgb ras2.rgb 0.3))
)

(rgb :ras3 [ras2 ras4]
  ; (rgb (sqrt (mix (* 1 ras2.rgb) (* 2 ras4.rgb) ras4.g)))
  (rgb (mix (* 1 ras2.rgb) (* 2 ras4.rgb) (sqrt ras4.g)))
  ; (rgb (mix (* 1 ras2.rgb) (* 2 ras4.rgb) 0.5))
)

(rgb :ras4 [ras1 ras4]
  (rgb (mix ras1.rgb ras4.rgb 0.9))
)
