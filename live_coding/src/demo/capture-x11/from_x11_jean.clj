(ns lv.demo)
(use 'lv.core)

(layout "grid" [win0 ras2]
               [ras3 ras4])
(render)

; read from a visible X11 window
(expr :win0 []
 "Jean"
)

; mix the upstream video with the current frame buffer content.
; reads from buffer ras2 and writes the result back to buffer ras2.
(rgb :ras2 [win0 ras2]
  (rot (* 0.005 f))
  (tx (* 0.0113 (snoise (* 0.011 f) y)))
  (ty (* 0.023 (snoise (* 0.017 f) x)))
  ; (mirror)
  (rgb (mix win0.rgb ras2.rgb 0.3))
)

(rgb :ras3 [win0 ras3]
  ; (mirror)
  ; (kaleid 3)
  ; (rot (* 0.005 (snoise (* 0.0131 f))))
  (ty (* 0.1 (snoise (* 0.017 f) x)))
  (inv)
  (rot (* -0.005 f))
  (tx (* 0.087 (snoise (* 0.011 f) y)))
  ; (inv)
  ; (mirror)
  (rgb (mix win0.rgb ras3.rgb 0.3))
)

(rgb :ras4 [ras2 ras3]
  (rgb (mix ras2.rgb ras3.rgb 0.3))
)
