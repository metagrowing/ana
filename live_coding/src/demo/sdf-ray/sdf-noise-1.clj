(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 frag1])
(movie "4movies/*.mp4")
(render 1)

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
 (sy 1.45)
 (rgb (* 1.2 c3.rgb))
)

(expr :frag1 [mov0]
 "frag/sdf-noise-1.frag"
 (r 0.7)
 (h (+ 0.05 (* 5 (usin (* 0.003 f)))))
 (rx (* 11 (snoise (* 0.0011 f)  0.4 -0.3)))
 (ry (* 10 (snoise (* 0.0013 f)  0.2 -0.1)))
 (rz (* 2.5 (snoise (* 0.0017 f)  0.3 -0.2)))
)
