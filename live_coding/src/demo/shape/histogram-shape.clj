(ns lv.demo)
(use 'lv.core)

(layout "horizontal" [shape0 shape1 ras2 ras3 mopr1])

(expr :shape0 []
  (count 85)
  (x (* 20 i))
	(y 108)
	(size (* 99 (unoise (+ (* 0.011 f)
	                       (* -0.01 x))
	                    0 0)))
	(res 3)
	(h 0.46) (s 0.48)	(v 0.75)
)

(expr :shape1 []
  (count 85)
  (x (* 20 i))
	(y 108)
	(size (* 99 (unoise (+ (* 0.017 f)
	                       (* -0.01 x))
												 0 0)))
	(res 3)
	(h 0.1) (s 0.48)	(v 0.75)
)
(rgb :ras2 [shape1]
	(sx -1)
	(tx 1)
	(rgb shape1.rgb)
)
(render 2)

(rgb :ras3 [shape0 ras2]
	(rgb (max shape0.rgb ras2.rgb ))
)

; morphological operators to widen the lines
(expr :mopr1 [ras3]
	"de"
)
(render)
