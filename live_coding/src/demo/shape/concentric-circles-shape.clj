(ns lv.demo)
(use 'lv.core)

(layout "grid" [shape0 mopr1]
	             [ras2 ras3])

; paint concentric circles
(expr :shape0 []
  (count 16)
  (x (+ 540 (* (cos (* 0.007 f)) 40 i)))
  (y (+ 540 (* (sin (* 0.011 f)) 40 i)))
	(size (* 40 i))
	(res z)
	(h 0.45) (s 0.48)	(v 0.66)
)

; morphological operators to widen the lines
(expr :mopr1 [shape0]
	"ddb"
)
(render 1)

; merge with former frame
; afterglow effect
(rgb :ras2 [ras2 mopr1]
  (rgb (mix ras2.rgb (* 1.25 mopr1.rgb) 0.2))
)
(render 2)

; disturb in 2d space
(hsv :ras3 [ras2]
	(tx (* 0.05 (snoise (* f 0.0011) y)))
	(ty (* 0.05 (snoise x (* f 0.0011))))
	(mirror)
	(sy 2)
	(hsv ras2.hsv)
)
(render 3)

(render)
