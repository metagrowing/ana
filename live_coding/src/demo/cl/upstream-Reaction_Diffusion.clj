; Karl Sims Reaction-Diffusion Tutorial
; http://www.karlsims.com/rd.html

; width and height must be power of 2
; use a landscape format
; canvas_width = 2048
; canvas_height = 512
; filter_width = 2048
; filter_height = 512

(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 clup1]
               [ras2 ras3])

(rgb :ras0 []
  ; (r 0.055) ; f
  (r (+ 0.052 (* 0.008 (+ 0.5 (/ y 2))))) ; f

  ; (g 0.062)
  ; (g (+ 0.061 (* 0.008 (+ 0 (/ r 2))))) ; k
  (g (+ 0.061 (* 0.008 (unoise 0.1 (* 5 y))))) ; k
)

(expr :clup1 [ras0]
  "cl/upstream-Reaction_Diffusion.cl"
  (loops 30)
  (p0 0.95)  ; timestep
  (p1 1.0)   ; D_a
  (p2 0.5)   ; D_b
)

(hsv :ras2 [clup1]
  (v (sqrt clup1.r))
)
(hsv :ras3 [clup1]
  (v (* 1.1 (atan clup1.g clup1.r)))
)
(render 3)
