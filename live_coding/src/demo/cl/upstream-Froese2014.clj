; width and height must be power of 2
(ns lv.demo)
(use 'lv.core)

(layout "grid" [clup1 ras2]
               [img3 ras4]
               [fdiff5 ras5])
(image "DistortImage/edgeDSCN3878-blue.png")

(expr :clup1 [img3]
  "cl/upstream-Froese2014.cl"
  (loops 25)
)

(hsv :ras2 [clup1]
  (v (sqr (log clup1.b)))
)

(rgb :img3 [c0 c1]
  (ty 0.6)
  (sx 0.7)
  (sy 0.7)
  (r (* 0.29 c1.r))
  (g (* 0.15 c1.b))
)

(hsv :ras4 [img3 clup1 ras2]
  (h img3.h)
  (s (* 0.9 img3.s))
  (v (* clup1.b img3.v))
)

(expr :fdiff5 [clup1])
(rgb :ras5 [ras4 fdiff5]
  (rgb (+ ras4.rgb (* 3 fdiff5.rgb)))
)
(render 5)
