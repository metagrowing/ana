(ns lv.demo)
(use 'lv.core)

(layout "grid" [clpp0 clpp1]
               [ras2])
; ks0    0.82677
; ks1    0.35433
; ks7    0.87402

(expr :clpp0 []
  "cl/TurbulencePal.cl"
  (p0 (* ks0 4 (snoise (* 0.0023 f) 0.1 0.2)))
  (p1 (* ks1 4 (snoise (* 0.0031 f) 0.3 0.4)))
  (p6 (+ 1 (* 7 ks6)))
  (p7 (+ 1 (* 7 ks7)))
)

(expr :clpp1 []
  "cl/TurbulenceSimplePal.cl"
  (p0 (* ks0 4 (snoise (* 0.0011 f) 0.5 0.6)))
  (p1 (* ks1 4 (snoise (* 0.0013 f) 0.7 0.8)))
  (p6 (+ 1 (* 7 ks6)))
  (p7 (+ 1 (* 3 ks7)))
)

(rgb :ras2 [clpp0 clpp1]
  ; (rgb (* 0.7 (+ clpp0.rgb clpp1.rgb)))
  (rgb (* 2 clpp0.rgb clpp1.rgb))
)
