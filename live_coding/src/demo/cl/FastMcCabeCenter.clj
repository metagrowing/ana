(ns lv.demo)
(use 'lv.core)

(layout "grid" [clpp0 numb1]
               [ras2])

; (expr :clpp0 []
;   "cl/FastMcCabeCenter.cl"
;   (loops (min 25 (sqrt (sqrt f))))
;   (p0 (usin (* 0.01 f)))
; )

; (expr :clpp0 []
;   "cl/FastMcCabeHole.cl"
;   (loops (min 25 (sqrt (sqrt f))))
; )

(expr :clpp0 []
  "cl/FastMcCabeHole.cl"
  (loops 5)
  (p0 (sqrt (sqrt ks0)))
)

(expr :numb1 []
  "%1.0f"
  (value (/ f 10))
  (size 450)
)

(rgb ras2 [clpp0 numb1]
  ; (rgb (* clpp0.rgb (- 1 numb1.rgb)))
  (rgb (max clpp0.rgb numb1.rgb))
)
