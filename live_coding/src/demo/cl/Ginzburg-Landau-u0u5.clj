(ns lv.demo)
(use 'lv.core)

(layout "grid" [clpp0 clspp1]
               [ras2])

(expr :clpp0 []
  ; paramters used by Dan Wills
  ; https://www.youtube.com/watch?v=0D2YIwn6TtQ
  "cl/Ginzburg-Landau-Dan_Wills.cl"
  (p0 u0)  ; D_a   = 0.3f;
  ; (p0 (+ u0 (* 0.3 (sin (* 0.01 f)))))   ; D_a   = 0.3f;
  (p1 u1)  ; D_b   = 0.151f;
  (p2 u2)  ; alpha = 0.076;
  (p3 u3)  ; beta  = 0.8;
  (p4 (+ 1 u4))   ; delta = 1.31;
  (p5 (* 0.1 u5)) ; gamma = 0.01;
  (loops 3)
)

(expr :clspp1 []
  ; paramters used by Dan Wills
  ; https://www.youtube.com/watch?v=0D2YIwn6TtQ
  "cl/Ginzburg-Landau-Dan_Wills.cl"
  (p0 (+ 0.3 (* 0.3 (sin (* 0.01 f)))))   ; D_a   = 0.3f;
  (p1 0.151) ; D_b   = 0.151f;
  (p2 0.076) ; alpha = 0.076;
  (p3 0.8)   ; beta  = 0.8;
  (p4 1.31)  ; delta = 1.31;
  (p5 0.01)  ; gamma = 0.01;
  (loops 11)
)

(hsv :ras2 [clpp0 clspp1]
  (kaleid 3)
  (rot (* 0.001 f))
  (h (+ u6 clpp0.r))
  (s (+ u7 clspp1.g))
  (v (+ (* 4.5 clpp0.g)
        (* 0.5 clspp1.g)))
)
(render)
