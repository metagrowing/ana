(ns lv.demo)
(use 'lv.core)

(layout "grid" [clpp0 ras0]
               [ras1])

(expr :clpp0 []
  "cl/FastMcCabeColorPal.cl"
  (loops 15)
)

(rgb :ras0 [clpp0]
  (kaleid 3)
  (rgb clpp0.rgb)
)
(rgb :ras1 [ras0 ras1]
  (rgb (mix ras0.rgb ras1.rgb 0.9))
)
(render 2)
