(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 igen1]
               [mopr2 frag3])

(image "flower-sampler/*.png")

(rgb :img0 [c0 c1 c2 c3]
  (rgb (mix c3.rgb c2.rgb (usin (* 0.031 f))))
)

(expr :igen1 [img0]
  "r2/r2biblur-test.cl"
  (p0 (* ks0 3))
  (p1 (* ks1 3))
  (p2 (+ 1.1 (sin (* 0.01 f))))
  ; (p2 (* ks2 3))
  (p3 (* ks3 3))
  (p4 (* ks4 3))
  (p5 (sqrt ks5))
  (p7 (* ks7 3))
)

(expr :mopr2 [igen1] "bb")

; kp6    0.1875
; kp7    0.42188
(expr :frag3 [igen1]
 "frag/bilateral_blur.frag"
 (csigma (* 20 kp6))
 (bsigma (* 2 kp7))
)

(render)
