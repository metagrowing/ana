(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 igen0]
               [ras1 mopr3])

(image "wave/wave*.png")

(rgb :img0 [c0 c1 c2 c3]
  (rgb (mix c0.rgb c1.rgb (usin (* 0.031 f))))
)
; kp0    0.11719
; kp1    0.25781
; kp2    0.11719
; kp3    0.50781
; kp4    0.58268
; kp5    0.75591
; kp6    0.5
; kp7    0.75
;
; ks0    0.53906
; ks1    0.45312
; ks2    0.60156
; ks3    0.67969
; ks4    0.30469
; ks5    0.41406
; ks6    0.99219
; ks7    0

; calculate raw image
(expr :igen0 [img0]
  "cl/ripples-concentric.cl"
  (p0 (* ks0 1))    ; xspace
  (p1 (* ks1 1))    ; yspace
  (p2 (* ks2 0.1))  ; xnoise
  (p3 (* ks3 0.1))  ; ynoise
  (p4 (* ks4 0.01)) ; xscatter
  (p5 (* ks5 0.01)) ; yscatter
  (p6 (+ ks6 0.0))  ; density
  (p7 (* ks7 4))    ; amplitude
)

; "temporal blur"
(rgb :ras1 [mopr3 ras1]
  (rgb (mix mopr3.rgb ras1.rgb 0.95))
)

; two blur passes
(expr :mopr3 [igen0] "bb")

(render)
