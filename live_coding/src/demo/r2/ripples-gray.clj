(ns lv.demo)
(use 'lv.core)

(layout "grid" [igen0 ras1]
               [ras2 mopr3])

; nanoKONTROL2
; kp0    0.62205
; kp1    0.30709
;
; ks0    0.99219
; ks1    0.40625
; ks2    0.53125
; ks3    0.82812
; ks4    0.375
; ks5    0.44531
; ks6    0.71094
; ks7    0.59375

; calculate raw image
(expr :igen0 [ras1]
  "cl/ripples-gray.cl"
  (p0 (* ks0 1))    ; xspace
  (p1 (* ks1 1))    ; yspace
  (p2 (* ks2 0.1))  ; xnoise
  (p3 (* ks3 0.1))  ; ynoise
  (p4 (* ks4 0.01)) ; xscatter
  (p5 (* ks5 0.01)) ; yscatter
  (p6 (+ ks6 0.0))  ; density
  (p7 (* ks7 4))    ; amplitude
)

; "temporal blur"
(rgb :ras1 [igen0 ras1]
  (rgb (mix igen0.rgb ras1.rgb 0.95))
)

; invert image
(hsv :ras2 [igen0]
  (v (- 1 igen0.v))
)

; two blur passes 
(expr :mopr3 [igen0] "bb")

(render)
