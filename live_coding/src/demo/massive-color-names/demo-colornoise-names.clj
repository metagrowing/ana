(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 cline1] [ras2 ras3])
(render)

(hsv :ras0 []
  (rot (* 0.002 f))
  (tx (snoise (* 0.001 f) y))
  (ty (snoise x (* 0.001 f)))
  (h (/ (floor (* r 15)) 15))
  (s 0.5)
  (v (/ (floor (* r 5)) 5))
)

; find contours center points
; and display color names
(expr :cline1 [ras0]
  (count 50)
  (max 1000)
  (thres 0.4)
  (fill -1)
  (names 1)
  (clear 1)
  (paint -1)
  (holes 1)
)

(rgb :ras2 [ras2 cline1]
  (rgb (* 1.05 (mix ras2.rgb cline1.rgb 0.05)))
)

(hsv :ras3 [ras2]
  (hsv (* (vec3 1 1 2) ras2.hsv))
)
(render 3)
