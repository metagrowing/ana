(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1] [ras2 ras3])
(sound "sound/beat1.mp3")
(render)

(hsv :ras0 []
  (rot (* 300 b0))
  (h b0)
  (s 0.7)
  (v (* (step x 0.1)
        (step a 0.9)
        25
        (sqrt vol)))
)

(hsv :ras1 []
  (h b0) ; color from current position in soundfile
  (s 0.7)
  (v (* 1 f0)) ; lowest frequency from fft bin 0
)

(hsv :ras2 []
  (h b0)
  (s 0.7)
  (v (* 1 f1)) ; fft bin 1
)

(rgb :ras3 [ras0 ras1 ras2]
  (kaleid (+ 2 (ceil (* 7 b0))))
  (mirror)
  (rgb (mix (+ ras0.rgb ras1.rgb)
            (+ ras0.rgb ras2.rgb)
            0.5))
)
;(render 3)
