(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0])

(hsv :ras0 []
  (v (smoothstep -0.2 0.2
      (sin (* 31 (+ (* 0.001 x f) a (* 0.07 (sin (* 7 r))))))))
)

(render 0)
