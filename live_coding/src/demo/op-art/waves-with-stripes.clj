(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras1])

(hsv :ras1 []
  (v (pos? (- y (snoise (* 2 x) (* 0.002 f)))
           (pos? (sin (* 80 x)) 1 0)
           (pos? (sin (* 80 y)) 1 0)))
)

(render 0)
