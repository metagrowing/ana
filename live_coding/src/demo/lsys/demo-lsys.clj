(ns lv.demo)
(use 'lv.core)

(layout "grid" [lsys0])

; "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
; page 35, figure 1.31 b
; Hogeweg and Hesper
(expr :lsys0 []
  (depth  30)
  (step   1)
  (angel  (* (/ 22.5 180) 3.14159))
  (ignore "+-F")
  (axiom  "F1F1F1F1")
  (r "0 < 0 > 0 -> 1")
  (r "0 < 0 > 1 -> 1[-F1F1]")
  (r (l "0") (p "0") (r "0") (s "1"))
  (r (l "0") (p "0") (r "1") (s "1[-F1F1]"))
  (r "0<0>0" "1")
  (r "0<0>1" "1[-F1F1]")
)
