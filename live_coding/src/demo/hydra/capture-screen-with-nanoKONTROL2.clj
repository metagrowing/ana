(ns lv.demo)
(use 'lv.core)

(layout "grid"
 [screen0 ras3 mopr3]
 [ras5 ras4 cline2])
(render)

; Requires that Hydra's window is above aNa's window.
; Because aNa actually copies its own window
; and as a side effect all windows that are above it.
(expr :screen0 [c0]
  (x (* ks0 1920))
  (y (* ks1 1080))
  (w (* kp0 1920))
  (h (* kp1 1080))
)

(expr :cline2 [screen0]
  (count 250)
  (max 2000)
  (thres ks7)
  (fill -1)
  (names -1)
  (clear (zero? (% f 30) 1 -1))
  (paint -1)
  (holes 1)
)

(expr :mopr3 [cline2] "dbb")

(hsv :ras3 [screen0]
  (hsv (+ (even? (int (* 123 y x))
                 (vec3 0.5 0 0)
                 (vec3 0 0 1))
          screen0.hsv))
)

(rgb :ras4 [screen0 mopr3 ras3]
  (kaleid 3)
  (rot (* f ks2 0.005))
  (rgb (mix ras3.rgb
            (+ screen0.rgb mopr3.rgb)
            ks1))
)

(rgb :ras5 [screen0]
  (rgb screen0.rgb)
)
