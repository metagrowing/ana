(ns lv.demo)
(use 'lv.core)

(layout "grid" [shape0 del1]
               [ras2 ras3])
(render)
; similar to "Schotter" from Georg Nees
(expr :shape0 []
  (count (* 12 24))
  (x (+ 50 (* 34 (% i 12))))
  (y (+ 120 (* 34 (floor (/ i 12)))))
  (size 21)
	(rot (+ 45 (* 0.4
		            (- y 900)
		            (sin (* 0.031 f))
								(snoise x y 0.1))))
	(res 4)
	(h 0.46) (s 0.48) (v 0)
)
; delay loop
(rgb :del1 [ras2]
  (k (mod (* sframe (unoise x y)) slength))
	(rgb (- 1 ras2.rgb))
)
; to achieve this effect switch between this
; two filters manually from time to time
; 2. use the delay loop
(rgb :ras2 [del1]
  (rot (* -0.1 del1.v))
	(rgb del1.rgb)
)
; 1. draw the pattern
(rgb :ras2 [shape0] (rgb shape0.rgb))
