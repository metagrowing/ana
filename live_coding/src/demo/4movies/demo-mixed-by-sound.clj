(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0])
; assume there are 4 movies
(movie "4movies/*.mp4")
; sound file to analyse
(sound "sound/beat1.mp3")

; f0 to f7 are fft bins
(rgb :mov0 [c0 c1 c2 c3]
	(sy 1.5) ; scale video
  (rgb ; mix rgb channels
    (mix
      (mix c2.rgb c3.rgb (* 5 f1))
      (mix c0.rgb c1.rgb (* 10 f2))
      f0))
)
