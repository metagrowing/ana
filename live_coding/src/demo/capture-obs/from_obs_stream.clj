(ns lv.demo)
(use 'lv.core)

(layout "grid" [vid0 ras1 ras4]
               [ras2 ras3])
(render)

; read from a loopback camera and shift color hue.
(hsv :vid0 [c0]
  (hsv (* (vec3 (usin (* 0.01 f)) 1 1) c0.hsv))
)

(hsv :ras1 []
  (h (pos? (- r u1)
           0.5
          (+ 0.3 (* 3 u0))))
  (s 0.5)
  (v (pos? (- r u1) 0.0 0.5))
)

; mix the upstream video with the current frame buffer content.
; reads from buffer ras2 and writes the result back to buffer ras2.
(rgb :ras2 [vid0 ras2]
  ; (mirror)
  (kaleid 3)
  (mirror)
  (rot (* 0.005 f))
  (rgb (mix vid0.rgb ras2.rgb 0.3))
)

(rgb :ras3 [ras2 ras4]
  ; (rgb (sqrt (mix (* 1 ras2.rgb) (* 2 ras4.rgb) ras4.g)))
  (rgb (mix (* 1 ras2.rgb) (* 2 ras4.rgb) (sqrt ras4.g)))
  ; (rgb (mix (* 1 ras2.rgb) (* 2 ras4.rgb) 0.5))
)

(rgb :ras4 [ras1 ras4]
  (rgb (mix ras1.rgb ras4.rgb 0.9))
)
