# Animation with patterns on a grid

A 40 x 40 grid filled with 4 small PNG images.
The input images have only 3 different colors: black, orange and turquoise. The two filters at the end of the graphics pipeline are changing tint.

![](./pattern-grid.png)

```mermaid
graph LR
:pattern0 -- pattern0 --> :ras2
:ras2 -- ras2 --> :ras3
:pattern1 -- pattern1 --> :ras3
```

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [pattern0 pattern1]
	             [ras2 ras3])
(image "pattern/*.png")
(render)

(expr :pattern0 []
  (count (* 40 40))
  (x (* 27 (% i 40)))
  (y (* 27 (floor (/ i 40))))
  (size (* 7 (pow (unoise (* 0.0050 x)
                          (* 0.0050 y)
                          (* 0.005 f))
                  1.75)))
)

(expr :pattern1 []
  (count (* 40 40))
  (x (* 27 (% i 40)))
  (y (* 27 (floor (/ i 40))))
  (size (* 4 (pow (unoise (* 0.050 x)
                          (* 0.0050 y)
                          (* 0.05 f))
                  1.1)))
)

(hsv :ras2 [pattern0]
  (h (+ pattern0.h (sin (* f 0.013))))
  (s pattern0.s)
  (v pattern0.v)
)

(rgb :ras3 [ras2 pattern1]
  (rgb (mix ras2.rgb pattern1.rgb 0.5))
)
(render 3)
```
