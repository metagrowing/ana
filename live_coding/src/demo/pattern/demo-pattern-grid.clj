(ns lv.demo)
(use 'lv.core)

(layout "grid" [pattern0 pattern1]
	             [ras2 ras3])
(image "pattern/*.png")
(render)

(expr :pattern0 []
  (count (* 40 40))
  (x (* 27 (% i 40)))
  (y (* 27 (floor (/ i 40))))
  (size (* 7 (pow (unoise (* 0.0050 x)
                          (* 0.0050 y)
                          (* 0.005 f))
                  1.75)))
)

(expr :pattern1 []
  (count (* 40 40))
  (x (* 27 (% i 40)))
  (y (* 27 (floor (/ i 40))))
  (size (* 4 (pow (unoise (* 0.050 x)
                          (* 0.0050 y)
                          (* 0.05 f))
                  1.1)))
)

(hsv :ras2 [pattern0]
  (h (+ pattern0.h (sin (* f 0.013))))
  (s pattern0.s)
  (v pattern0.v)
)

(rgb :ras3 [ras2 pattern1]
  (rgb (mix ras2.rgb pattern1.rgb 0.5))
)
(render 3)
