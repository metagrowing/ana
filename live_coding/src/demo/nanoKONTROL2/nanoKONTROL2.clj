(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras1 ras2]
               [ras4 sline5])
(render)

; nanoKONTROL2
; potentiometer: kp0 to kp7, range 0.0 .. 1.0
; silder:        ks0 to ks7, range 0.0 .. 1.0
; S-M-R button:  kb0 to kb7, discret values +1, 0, -1

; global expressions
(texpr
    (t2 (sqr (* 5 ks2)))
)

(hsv :ras1 []
    (h ks0)
    (s ks1)
    (v (/ (floor (* a x t2)) t2))
)

(hsv :ras2 []
    (h ks3)
    (s ks4)
    (v (unoise (* 0.1 f ks5) (* 3 r)))
)

(hsv :ras3 [] (v 0))

(rgb :ras4 [ras1 ras2]
    (rgb (mix ras1.rgb ras2.rgb ks7))
)

; concentric scan lines
(expr :sline5 [ras4]
    (mode 2)
    (clear kb7)
    (dy (/ 3.14149 300))
    (dx (+ 0.005 (* 0.05 kp6)))
    (jitter (* -0.2 kp7 (+ r g b)))
)
