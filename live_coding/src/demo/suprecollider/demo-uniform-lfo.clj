(layout "grid" [img0 ras1] [ras2 ras3])

; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; convert to gray-scale and invert
(hsv :img0 [c0 c1 c2 c3]
  (v (- 1 c3.v))
)

; a simple gradient using lfo \par_u1
(hsv :ras1 []
  (v (* (- 1 y u1)))
)

; concentric discs using lfo \par_u2
(hsv :ras2 []
  (v (/ (floor (* 7 (- 1 r u2))) 7))
)

; blending upstream images with \par_u3
(hsv :ras3 [img0 ras1 ras2]
  (rot (* 0.002 (pos? (- u3 0.01) f 0)))
  (v (* img0.v (mix ras1.v ras2.v (- 1 a u3))))
)
;(render 3)
