(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 ras1] [pihat3])
(movie "4movies/*.mp4")
(render)

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
  (sy 1.5)
  (rgb c0.rgb)
)

; make it darker
(hsv :ras1 [mov0]
  (h mov0.h)
  (s mov0.s)
  (v (sqr mov0.v))
)

; send to Raspberry Pi SenseHAT
(expr :pihat3 [ras1]
  (mode 0) ; draw mode 0:
           ; use the color from the upstream filter node
)
