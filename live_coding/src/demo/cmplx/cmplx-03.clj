(ns lv.demo)
(use 'lv.core)

(layout "grid" [frag0 img0]
               [ras1 ras2])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

(hsv :img0 [c0 c1 c2 c3]
  ; (hsv (* c0.hsv c2.hsv))
  (hsv (* c2.hsv))
)

; mapping the complex plane
; using the whole color wheel
(expr :frag0 [img0]
 "frag/cmplx/cmplx-03.frag"
 (t (* 2 (sin (* 0.01 f))))
)

; PT-1
(rgb :ras2 [frag0 ras2]
  (rgb (mix frag0.rgb ras2.rgb 0.9))
)
(render 3)
