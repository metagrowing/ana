(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 del1]
               [ras2 ras3])
(render)
; (render 1)
; global expressions
(texpr
  (t0 (+ t0 (* 0.2 (- ks0 0.5)))) ; speed
  (t1 (+ t0 (* 0.5 (- kp0 0.5)))) ; phase + speed
  (t2 (+ t2 (* 0.2 (- ks1 0.5)))) ; speed
  (t3 (+ t2 (* 0.5 (- kp1 0.5)))) ; phase + speed
  (t4 (+ t4 (* 0.2 (- ks2 0.5)))) ; speed
  (t5 (+ t4 (* 0.5 (- kp2 0.5)))) ; phase + speed
)
; generate hue, saturation and value
; modulated by the delay loop
(hsv :ras0 [del1]
  (sx 0.5)
  (rot (* 0.01 f))
  (h (* 0.3 (sin (+ (* 3 del1.v) (* 10 x) t1))))
  (s (* 0.7 (usin (+ (* 2 del1.v) (* 10 x) t3))))
  (v (usin (+ del1.v (* 10 r) t5)))
)
; delay loop
(rgb :del1 [ras0]
  (k (mod (+ 51 (* x r sframe)) slength))
  (rgb (- 1 ras0.rgb))
)
