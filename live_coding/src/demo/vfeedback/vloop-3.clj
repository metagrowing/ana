(ns lv.demo)
(use 'lv.core)

(layout "vertical" [ras0 ras5 ras4 ras2 ras3 ras1])
; (render 5)
(render)

; global expressions
(texpr
  (t0 (+ t0 (* 0.2 (- ks0 0.5)))) ;speed
  (t1 (+ t0 (* 0.5 (- kp0 0.5)))) ; phase + speed
  (t2 (+ t2 (* 0.2 (- ks1 0.5)))) ;speed
  (t3 (+ t2 (* 0.5 (- kp1 0.5)))) ; phase + speed
  (t4 (+ t4 (* 0.2 (- ks2 0.5)))) ;speed
  (t5 (+ t4 (* 0.5 (- kp2 0.5)))) ; phase + speed
)
; red channel
(rgb :ras0 [ras4]
    (r (usin (+ (* 3 ras4.v) (* 10 x) t1)))
)

; green channel
(rgb :ras1 [ras0]
    (g (usin (+ (* 2 ras0.v) (* 10 x) t3)))
)

; blue channel
(rgb :ras2 [ras1]
    (b (usin (+ ras1.v (* 10 y) t5)))
)

; mixer: combine red green and blue channels
(rgb :ras3 [ras0 ras1 ras2]
    (rgb (vec3 (* ks3 ras0.r) (* ks4 ras1.g) (* ks5 ras2.b)))
)
; effect: video rotation and scale
(hsv :ras4 [ras3]
  (rot (* 0.001 f))
  (sy 1.1)
  (sx 0.9)
  (hsv (* ras3.hsv (vec3 1 0.5 1)))
)
; mixer: combine video effect with blue channel
(hsv :ras5 [ras2 ras4]
  (hsv (* (mix ras2.hsv ras4.hsv ks7) (vec3 1 0.5 1)))
  ; (rgb (mix ras2.rgb ras4.rgb ks7))
)
