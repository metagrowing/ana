(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1 ras2 ras3]
               [ras4 ras5 ras6 ras7])
(render)

; global expressions
(texpr
  (t0 (+ t0 (* 0.1 ks0))) ;speed
  (t1 (+ t0 (* 0.1 (- kp0 0.5)))) ; phase + speed
  (t2 (+ t2 (* 0.1 ks1))) ;speed
  (t3 (+ t2 (* 0.1 (- kp1 0.5)))) ; phase + speed
  (t4 (+ t4 (* 0.1 ks2))) ;speed
  (t5 (+ t4 (* 0.1 (- kp2 0.5)))) ; phase + speed
)
; red channel
(rgb :ras0 [ras4]
    (r (usin (+ ras4.v (* 10 x) t1)))
)

; green channel
(rgb :ras1 [ras4]
    (g (usin (+ ras4.h (* 10 y) t3)))
)

; blue channel
(rgb :ras2 [ras4]
    (b (usin (+ ras4.s (* 10 x) t5)))
)

; mixer: combine red green and blue channels
(rgb :ras3 [ras0 ras1 ras2]
    (rgb (vec3 (* ks3 ras0.r) (* ks4 ras1.g) (* ks5 ras2.b)))
)
; effect: video rotation
(rgb :ras4 [ras3]
  (rot 0.5)
  (rgb ras3.rgb)
)
; mixer: combine video rotation with blue channel
(rgb :ras5 [ras2 ras4]
  (rgb (pow ras2.rgb ras4.rgb))
)
