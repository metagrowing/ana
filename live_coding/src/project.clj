(defproject lv "0.1.0-SNAPSHOT"
  :description "analog Not analog: aNa is a live coding system for visuals."
  :url "https://gitlab.com/metagrowing/ana/-/tree/master"
  :license {:name "GPL-3.0"
            :url "https://gitlab.com/metagrowing/ana/-/blob/master/LICENSE"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [overtone/osc-clj "0.9.0"]
                 [proto-repl "0.3.1"]
                 [org.clojure/test.check "1.1.0"]]
  :main ^:skip-aot lv.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
  
