(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 mov0 cline2]
               [mopr1 ras3 ras4])
(render)

(image "../../../timelapse/series/20230717-ice-bleeptrack/split/clip_00002.png")
; (movie "../../../timelapse/series/20230320-glass-pipe-led/P1150735.MOV")
; (movie "../../../timelapse/series/20230310-servo-glass-marble/IMG_2070.MOV")
(movie "../../../timelapse/series/20220615-ribbon-1/ribbon-1-24fps-2400.mp4")
(hsv :mov0 [c0]
  ; (sx 0.5) (sy 0.5)
  (hsv (* (vec3 1 1.2 1) c0.hsv))
)

(rgb :img0 [c0]
  (sx 2.8) (sy 3) (ty kp7)
  (rgb c0.rgb)
)

; find contours
(expr :cline2 [mov0]
  (count 50)
  (max 5000)
  ; (thres kp1)
  (thres (+ 0.3 (* 0.23 (usin (* 0.1 f)))))
  (clear kb0)
  (fill -1)
  (paint -1)
  (holes 1)
)

(expr :mopr1 [cline2] "ddddbbbb")

(rgb :ras3 [ras3 mopr1]
  ; (rgb (mix mov0.rgb mopr1.rgb ks6))
  (rgb (max (* 0.97 ras3.rgb) mopr1.rgb))
)
(rgb :ras4 [mov0 ras3]
  ; (rgb (mix mov0.rgb mopr1.rgb ks6))
  (rgb (+ (* 0.3 mov0.rgb) ras3.rgb))
)
