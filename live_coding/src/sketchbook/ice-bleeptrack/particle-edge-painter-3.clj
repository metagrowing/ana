(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 mopr2 clpar1 ras4]
               [cline1 cline2 ras3])
(render)

(movie "../../../timelapse/series/20230320-glass-pipe-led/P1150735.MOV")
(hsv :mov0 [c0]
  (sx 1.5) (sy 1.5)
  (hsv (* (vec3 1 1.2 1) c0.hsv))
)
; kp0    0.94531
; kp1    0.0625
; kp2    0.023622
; kp3    0.11024
; kp4    0
; kp5    0.36719
; kp6    0.86719
; kp7    0.88281
;
; ks0    0.99219
; ks1    0.14844
; ks2    0.60938
; ks3    0.71875
; ks4    0.50781
; ks5    0.71094
; ks6    0.875
; ks7    0.90625

; find contours
(expr :cline1 [mov0]
  (count 50)
  (max 5000)
  (thres (* 0.5 kp0))
  (fill -1)
  (paint -1)
  (holes 1)
)

; find contours
(expr :cline2 [mov0]
  (count 50)
  (max 5000)
  (thres kp1)
  (fill -1)
  (paint -1)
  (holes 1)
)

(rgb :ras3 [cline1 cline2]
  (rgb (+ cline1.rgb cline2.rgb))
)

(expr :mopr2 [ras3] "ddddbbbbbbbb")

(expr :clpar1 [mopr2]
  "particle-model/particle-model-6.cl"
  (paint 1)
  (p0 (* 1 ks0)) ; acc
  (p1 (* 0.1 ks1)) ; vel
  (p2 (* 0.01 ks2)) ; pos
  (p3 (* 0.001 ks3))
  (p4 (* 4 ks4))   ; disipate
  (p7 (+ 0.001 (* 0.1 ks7)))
)

(rgb :ras4 [mov0 clpar1 ras4]
  ; (rgb (mix mov0.rgb (mix clpar1.rgb ras4.rgb 0.97) 0.9))
  (rgb (* (vec3 kp6 kp6 kp6) (max mov0.rgb (mix clpar1.rgb ras4.rgb kp7))))
  ; (rgb (vec3 0 0 0))
)
