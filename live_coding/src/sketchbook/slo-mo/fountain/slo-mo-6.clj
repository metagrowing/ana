(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 ras1][ras2 ras3])
; assume there are 4 movies
(movie "slo-mo/*.mp4")
; sound file to analyse
(sound "sound/beat1.mp3")

(render)
(render 2)

(rgb :mov0 [c0 c1 c2 c3 c4 c5 c6 c7]
	(rgb (low? (sin (- y (* 0.031 f)))
	     0.7
			 (- 1 c5.rgb)
			 (- 1 c3.rgb)))
)

(rgb :ras1 [mov0 ras1]
	(rgb (low? (sin (+ (unoise (* 10 x) (unoise (* 10 y) f)) (* 0.013 f)))
	           0.3
						 mov0.rgb
						 ras1.rgb))
)

(rgb :ras2 [mov0 ras1]
	; (rgb (mix (- 1 mov0.rgb) ras1.rgb ks0))
	(rgb (* (- 1 mov0.rgb) ras1.rgb (- 1.2 r)))
)
