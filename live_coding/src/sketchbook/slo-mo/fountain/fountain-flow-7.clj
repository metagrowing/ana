(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 ffb1]
               [fdiff2 ras3])

(movie "slo-mo/fountain/IMG_0419-24fps-2400.mp4")
(render)

; ks0    0.77953
; ks1    0.055118
; ks2    0.37008
; ks3    0.37795
; ks4    0.62992
; ks5    0
; ks6    0.30709
; ks7    0.007874

(hsv :mov0 [c0]
  ; (mirror)
  (sx (* 2 ks4))
  ; (tx (pos? (sin (* 10 y)) 0.1 0.2))
  ; (tx (pos? (- 0.5 c0.v) 0.1 0.2))
  (tx (* ks5 (- 0.5 c0.s)))
  (sy (* 5 ks6))
  ; (ty (* 1 ks7))
  (ty (* ks7 (- 0.5 c0.v)))
  (hsv c0.hsv)
)

; motion detection
(expr :ffb1 [mov0]
  (reduce 4)
  (mode 3)
  (gauss -1)
  (levels 1)
  (win 8)
  (iter 4)
  (polyn 7)
  (polys 1.2)
  (r 10)
  (step 3)
)

(expr :fdiff2 [ffb1])

(hsv :ras3 [mov0 fdiff2 ffb1 ras3]
  (hsv (mix (* 6 (min fdiff2.hsv ffb1.hsv))
            ras3.hsv
            ks0))
)
(rgb :ras3 [mov0 fdiff2 ffb1 ras3]
  (rgb (- 1 (mix (* ks0 5 (max fdiff2.rgb ffb1.rgb))
            ras3.rgb
            mov0.s)))
)
