(ns lv.demo)
(use 'lv.core)

(layout "grid" [bill0 ras1] [ras2 ras3])
(render)

(expr :bill0 []
  ; TODO pattern and palette is NOT implemented
  ; (load pattern "billboard/ring-alpha.png")
  ; (load palette "billboard/Warm_Colors.gpl")
  ; (roty f)
  (count 38)
  ; (x (- 1980 (* 50 i)))
  (x (+ -960 (* 50 i)))
  (y (* (- (+ i -19))
        15
        (usin (* 0.01 f))
        (sin (+ (* 0.05 f) i))))
  (size 120)
)
(render 0)

(hsv :ras1 [bill0]
  (h bill0.h)
  (s bill0.s)
  (v (* 1.2 bill0.v))
)
(render 1)

(rgb :ras2 [ras1 ras2]
  (rgb (mix (* 3 ras1.rgb)
            ras2.rgb
            0.98))
)
(render 2)

(rgb :ras3 [bill0 ras2]
  (rgb (+ (* 2 bill0.rgb) ras2.rgb))
)
(render 3)
