(ns lv.demo)
(use 'lv.core)
; video: condensed tomato soup
(layout "horizontal" [imgs0 imgs1 ras2 cline3])
(imagesequ "../../../timelapse/series/20210101-condensed/split-P1140104/*.png")

(hsv :imgs0 [c0]
  (k (% (int (+ i sframe)) slength))
  (hsv c0.hsv)
)
(render 0)

(hsv :imgs1 [c0]
  (rot (/ pi 2))
  (k (% (int (* y sframe)) slength))
  ; (k (- slength sframe))
  (hsv c0.hsv)
)
(render 1)

(rgb :ras2 [imgs0 imgs1 cline3]
  (rgb (+ (* 0.3 cline3.rgb)
          (+ (* 0.55 imgs0.rgb)
             (* 0.25 imgs1.rgb))))
)
(render 2)

(expr :cline3 [imgs0]
  (count 50)
  (max 500)
  (thres 0.5)
  (paint -1)
  (fill 1)
  (holes 1)
)
(render 3)
(render)
