(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 ras1]
               [del3 ras4])
(render)

(imagesequ "ga-br-22/*.jpg")

(hsv :imgs0 [c0]
  (k (mod (* 0.01 f) slength))
  (hsv c0.hsv)
)

(rgb :ras1 [imgs0 ras1]
  (rgb (mix imgs0.rgb ras1.rgb 0.97))
)

(rgb :del3 [ras1]
  (k (pos? y
        (pos? (- y 0.5) (mod (+   0 sframe) slength)
                        (mod (+ (* 0.25 slength) sframe) slength))
        (pos? (+ y 0.5) (mod (+ (* 0.50 slength) sframe) slength)
                        (mod (+ (* 0.75 slength) sframe) slength))
    ))
	(rgb ras1.rgb)
)

(rgb :ras4 [del3 ras1]
  ; (rgb (mix del3.rgb ras1.rgb ks0))
  (rgb (* del3.rgb ras1.rgb 4))
)
