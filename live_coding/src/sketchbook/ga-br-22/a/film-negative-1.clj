(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0])
(render)

(image "../../../Desktop/glitchart-br-2022/neg/P1150555-1598x720.png")

; (hsv :img0 [c0]
;   (sx 0.8)
;   (tx (+ (* -0.3 (unoise (* 0.011 f) 0.1))
;          (* 0.1 (turb x (* 0.013 f) 4))))
;   (hsv (pos? y (- 1 c0.hsv) c0.hsv))
; )

; ks0    0.66929
; ks1    0.43307

(hsv :img0 [c0]
  (sx 0.8)
  (tx (+ (* -0.1 ks0 (unoise (* 0.011 f) 0.1))
         (* -0.1 ks1 (turb x (* 0.013 f) 4))))
  (hsv (pos? y (- 1 c0.hsv) c0.hsv))
)
