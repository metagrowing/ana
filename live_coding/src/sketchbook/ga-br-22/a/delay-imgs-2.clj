(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 ras1]
               [del2 ras3 fx4])
(render)

(imagesequ "ga-br-22/*.jpg")

(hsv :imgs0 [c0]
  (k (mod (* 0.01 f) slength))
  (hsv c0.hsv)
)

(rgb :ras1 [imgs0 ras1]
  (rgb (mix imgs0.rgb ras1.rgb 0.97))
)

(rgb :del2 [ras1]
  (k (pos? y
        (pos? (- y 0.5) (mod (+   0 sframe) slength)
                        (mod (+ (* 0.25 slength) sframe) slength))
        (pos? (+ y 0.5) (mod (+ (* 0.50 slength) sframe) slength)
                        (mod (+ (* 0.75 slength) sframe) slength))
    ))
	(rgb ras1.rgb)
)

(rgb :ras3 [del2 ras1]
  ; (rgb (mix del2.rgb ras1.rgb ks0))
  (rgb (* del2.rgb ras1.rgb 4 0.66929))
)

(fx :fx4 [ras3]
  ; (neg)
  ; (blur)
  ; (solar (* 0.5 (+ 1 y)) (* 2 t0) ks2)
  ; (pixel (* 2 10) (+ 1(* f 0.01)))
  ; (pixel (* 0.02 f) (+ 1 (* 0.01 f )))
  ; (neg)
  ; (black (+ 0.95 y) (+ -0.95 y))
  ; (black (+ 0.95 y t2) (+ -0.95 y t2))
  ; (black y y)
  (roll (+ (* 10 x) (* 3.1 f)) 0)
  (blur)
  (blur)
  (blur)
  ; (tox (* 20 t0 t1) f y 4)
  ; (pixel 20 20)
)
