(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 ras1 del2]
               [ras3 pix9])
(render)

(imagesequ "ga-br-22/*.jpg")

(hsv :imgs0 [c0]
  (k (mod (* 0.01 f) slength))
  (hsv c0.hsv)
)

(rgb :ras1 [imgs0 ras1]
  (rgb (mix imgs0.rgb ras1.rgb 0.97))
)

(rgb :del2 [ras1]
  (k (pos? y
        (pos? (- y 0.5) (mod (+   0 sframe) slength)
                        (mod (+ (* 0.25 slength) sframe) slength))
        (pos? (+ y 0.5) (mod (+ (* 0.50 slength) sframe) slength)
                        (mod (+ (* 0.75 slength) sframe) slength))
    ))
	(rgb ras1.rgb)
)

(rgb :ras3 [del2 ras1]
  ; (rgb (mix del2.rgb ras1.rgb ks0))
  (rgb (* del2.rgb ras1.rgb 4 0.66929))
)

(expr :pix9 [ras3]
  (b (+ 0.6 (* 0.2 (snoise 0.1 (* 0.11 f) (* 0.13 f)))))
  ; (w 1)
  (w (+ 0.5 (usin (* 0.1 f))))
	(comp (- v))
)
