(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 imgs1]
               [del2 ras3]
               [ras4 mopr5])
(render)

(imagesequ "ga-br-22/*.jpg")

(hsv :imgs0 [c0]
  (k (mod (* 0.011 f) slength))
  (hsv c0.hsv)
)

(hsv :imgs1 [c0]
  ; (k (mod (* 0.0071 f) 2))
  ; (k (pos? (snoise (* 0.011 f) 0.1) 0 (* 0.062992 slength)))
  (k (pos? (snoise (* 0.0071 f) 0.1) 0 0.95))
  ; (k (* ks7 slength))
  (hsv c0.hsv)
)

(rgb :del2 [imgs0]
  (k (pos? (+ x (sin f))
        (pos? (- y 0.5) (mod (+   0 sframe) slength)
                        (mod (+ (* 0.25 slength) sframe) slength))
        (pos? (+ y 0.5) (mod (+ (* 0.50 slength) sframe) slength)
                        (mod (+ (* 0.75 slength) sframe) slength))
    ))
	(rgb imgs0.rgb)
)

; (rgb :del3 [imgs1]
;   (k (pos? x
;         (pos? (- x 0.5) (mod (+   0 sframe) slength)
;                         (mod (+ (* 0.25 slength) sframe) slength))
;         (pos? (+ x 0.5) (mod (+ (* 0.50 slength) sframe) slength)
;                         (mod (+ (* 0.75 slength) sframe) slength))
;     ))
; 	(rgb imgs1.rgb)
; )

(rgb :ras3 [del2 imgs1]
  (rgb (atan del2.rgb imgs1.rgb))
)

; (rgb :ras4 [del2 ras3 ras4]
;   (rgb (mix (mix del2.rgb ras3.rgb ks0) ras4.rgb ks1))
; )
(hsv :ras4 [del2 ras3 ras4]
  (hsv (mix (mix del2.hsv ras3.hsv ks0) ras4.hsv 0.94))
)
; (rgb :ras4 [imgs0 imgs1 del2 del3 ras4]
;   (rgb del2.rgb)
; )

(expr :mopr5 [ras4] "bb")
