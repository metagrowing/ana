(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0]
               [fx1  ras1])
(render)

(imagesequ "ga-br-22/*.jpg")

; (rgb :imgs0 [c0]
;   (k (mod (* -0.013 f) slength))
;   (r c0.r)
;   (tx 0.1)
;   (g c0.g)
;   (tx 0.2)
;   (b c0.b)
; )
;
; (rgb :imgs0 [c0]
;   (k (mod (* 0.013 f) slength))
;   (r c0.r)
;   (ty 0.1)
;   (g c0.g)
;   (ty 0.2)
;   (b c0.b)
; )

(texpr
  (t0 (usin (* 0.2 f)))
  (t1 (turb (* 0.23 f) (* 0.11 f) y 3))
  (t2 (* 0.05 (snoise (* 0.13 f) (* 0.071 f)  (* 0.05 f))))
)

(hsv :imgs0 [c0]
  ; (sy 1.1)
  ; (k (mod (+ r (* (usin -0.013 f)) slength))
  (k (mod (* x 0.013 (mod f 300)) slength))
  (h c0.h)
  (sy 1.01)
  (s (* ks1 c0.s))
  (sx 1.02)
  (v c0.v)
)

(fx :fx1 [imgs0]
  (neg)
  ; (solar (* 0.5 (+ 1 y)) (* 2 t0) ks2)
  ; (pixel (* 2 10) (+ 1(* f 0.01)))
  (black (+ 0.95 y t2) (+ -0.95 y t2))
  ; (blur)
  (tox (* 20 t0 t1) f y 4)
  (roll 0 (+ (* 10 x) (* 3.1 f)))
)

(rgb :ras1 [fx1 ras1]
  (rgb (mix fx1.rgb ras1.rgb 0.6))
)
