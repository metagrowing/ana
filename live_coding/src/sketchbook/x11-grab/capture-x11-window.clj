  (ns lv.demo)
  (use 'lv.core)

  (layout "grid" [win0 ras1] [ras2])
  (render)

  (expr :win0 []
   ; "analog Not analog"
   "bespoke synth - osc-test-2021-12-06_19-24.bsk"
   (i -1)
   ; (i (* 0.05 f))
  )

  (hsv :ras1 [win0]
    ; (mirror)
    (rot (* 0.01 f))
    (kaleid 5)
    ; (inv)
    (h (+ (usin (* 0.05 f)) win0.h))
    (s win0.s)
    (v (- 1 win0.v))
  )

  (rgb :ras2 [ras2 ras1]
    (rgb (mix ras2.rgb ras1.rgb 0.2))
  )
