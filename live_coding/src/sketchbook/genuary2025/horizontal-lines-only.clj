(ns lv.demo)
(use 'lv.core)

(layout "grid" [word0 ras1]
               [ras2])

(expr :word0 []
  "horizontal\nlines\nonly."
  (size 130)
)

(hsv :ras1 []
  (v (zero? (mod (/ (+ f j) 4) 4) 1 0))
)

(hsv :ras2 [word0 ras1]
  (v (* word0.v ras1.v))
)

(render 2)
; (render)
