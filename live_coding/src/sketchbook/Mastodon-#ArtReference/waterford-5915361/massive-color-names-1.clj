(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 cline3] [ras4 ras5])
(image "Mastodon-#ArtReference/waterford-5915361/waterford-5915361_1920.jpg")
(render)

(rgb :img0 [c0]
  (ty 0.05)
  (rgb c0.rgb)
)

; find contours center points
; and display color names
(expr :cline3 [img0]
  (count 30)
  (max 1500)
  (thres 0.45)
  (fill -1)
  (names 1)
  (clear 1)
  (paint -1)
  (holes 1)
)

(rgb :ras5 [img0 cline3]
  (rgb (+ img0.rgb cline3.rgb))
)
