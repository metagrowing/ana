(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0])

(image "../../../timelapse/series/20220614-train-1/orbit-FixedVerticalWideCenterSlit_where=406,swidth=5_-20220614-164708.png")
(render 0)

(rgb :img0 [c0]
  (tx (/ (mod f 300) 30))
  (rgb c0.rgb)
)
