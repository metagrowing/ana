(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0]
               [clag1])
(movie "4movies/*.mp4")

; ks0    0.90551
; ks1    0.92126
; ks2    1
; ks3    1
; ks4    0.82677
; ks5    0.56693
; ks6    0.55906
; ks7    1

(hsv :mov0 [c0 c1 c2 c3]
  (sx 1.5)
  (ty 0.1)
  ; (v (pos? (- c2.v ks4) 1 0))
  (v (- 1 (* c1.v ks4)))
  ; (v (* c2.v ks4))
)

(expr :clag1 [mov0]
  "physarum/trail-center-movie/trail-center-movie.cl"
  (paint 1)

  (p0 (+ (* ks0 0.001) 0.00005))
  (p1 (* ks1 0.01))
  (p2 (* ks2 0.2))
  (p3 (* ks3 0.05))
  (p6 (sqrt (/ ks6 16)))
  (p7 (+ ks7 0.1))
)
(render 1)
