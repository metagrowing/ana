(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 ras0]
               [clag1 ras1])
(movie "physarum/*.MOV")

; kp0    0.41732
; kp1    0.80315
; kp7    0.83465

(hsv :mov0 [c0 c1]
  (tx 0.1)
  (ty 0.1)
  (v c1.v)
)

(hsv ras0 []
  ; (v (- kp1 (sqr (+ y (sin (* 0.1 f))))))
  (v (- kp7 (sqr (+ (sin (snoise y (* 0.01 f)))))))
)

(expr :clag1 [mov0]
  "physarum/trail-pulse/s-trail-pulse.cl"
  (paint 1)

  ; (p0 (+ (* ks0 0.001) 0.00005))
  ; (p1 (* ks1 0.01))
  ; (p2 (* ks2 0.2))
  ; (p3 (* ks3 0.05))
  ; (p5 (sign (- (rand) 0.9)))
  ; (p6 (sqrt (/ ks6 16)))
  ; (p7 (+ ks7 0.1))

  (p0 (* 0.59055 0.1))

  ; agent sense distance;
  ;(p1 (* ks1 0.01))
  (p1 (* 0.71654 0.01))

  ; agent sense angle
  ;(p2 (* ks2 1.5))
  (p2 (* 1 1.5))

  ; agent rotate angle
  ;(p3 (* ks3 0.5))
  (p3 (* 0.1811 0.5))

  ; not used
  (p4 0)

  ; trigger to disturbe the trail map
  ;(p5 (sign (- (rand) 0.3)))
  (p5 1)

  ; not color hue
  ;(p6 ks6)
  (p6 0.094488)

  ; none linear scaling from trail map to color
  ;(p7 (+ ks7 0.1))
  (p7 (+ 0.13386 0.1))

)

(hsv ras1 [ras0 clag1]
  (hsv clag1.hsv)
  (h (pos? (- ras0.v 0.8)
           (+ clag1.h kp2)
           (+ clag1.h kp0)))
)

(render 3)
