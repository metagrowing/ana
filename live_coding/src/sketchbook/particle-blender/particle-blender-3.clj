(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 frag1]
               [ras3 clag2])
(movie "4movies/*.mp4")
; (movie "4mt/*.MOV")
(render)
; kp0    0.40625
; kp1    0.75781
; kp2    0.023622
; kp3    0.11024
; kp4    0
; kp5    0.36719
; kp6    0
; kp7    0.64844
;
; ks0    0.39062
; ks1    0.46875
; ks2    0.5
; ks3    0
; ks4    0
; ks5    0
; ks6    0.9375
; ks7    0.99219

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3 c4 c5 c6 c7]
  ; (mirror)
 (sx 1.5)
 (sy 1.5)
 ; (rgb c0.rgb)
 (r (/ (+ c0.r c0.g c0.b) 3))
 ; (r (- 1 c0.g))
)

(expr :frag1 [clag2]
  "particle-blender/dFdxy-2.frag"
)

(expr :clag2 [ras3]
  "particle-blender/particle-blender-1.cl"
  (paint 1)
  (p0 (* ks0 1))   ; vel
  (p7 (+ ks7 0.1)) ; non linear gray scale
)

(rgb :ras3 [clag2 mov0]
  (rot (* -0.01 f))
  (rgb (mix clag2.rgb mov0.rgb ks6))
)
