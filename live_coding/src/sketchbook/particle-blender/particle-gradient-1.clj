(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 frag1]
               [ras3 clag2])
(image "../../../as_seen/pflanzen/botanischer_garten/P1120864-1920x1080.png")
(render)

; (hsv :ras0 []
;   (v (max 0 (snoise x y)))
;   ; (v (max 0 (- 1 r)))
;   ; (v (round (+ x y)))
; )

(hsv :img0 [c0]
  (hsv c0.hsv)
)

(expr :frag1 [img0]
  "particle-blender/dFdxy-3.frag"
  (scale (* ks7 1000))
)

(hsv :ras3 [frag1]
  (h (atan frag1.g frag1.r))
  (s 1)
  (s (sqr frag1.s))
  (v (sqr frag1.v))
)

(expr :clag2 [ras3]
  "particle-blender/particle-blender-1.cl"
  (paint 1)
  (p0 (* ks0 1))   ; vel
  (p7 (+ ks1 0.1)) ; non linear gray scale
)
