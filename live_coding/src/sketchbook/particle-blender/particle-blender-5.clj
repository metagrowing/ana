(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1 clag2]
               [ras3 frag4 ras5])
(render 5)
; kp0    0.40625
; kp1    0.42188
; kp2    0.023622
; kp3    0.11024
; kp4    0
; kp5    0.36719
; kp6    0.71875
; kp7    0.23438
;
; ks0    0.29688
; ks1    0.03125
; ks2    0.5
; ks3    0
; ks4    0
; ks5    0
; ks6    0.95312
; ks7    0.38281

(rgb :ras0 []
  (r (fract (* 2 r)))
  (b 0)
)

(hsv :ras1 [ras0]
  (h (+ ks1 ras0.h))
  (s (* 0.7 ras0.s))
  (v (* (- r ks5) ras0.v))
)

(expr :clag2 [ras3]
  "particle-blender/particle-blender-1.cl"
  (paint 1)
  (p0 (* ks0 1))   ; vel
  (p7 (+ ks7 0.1)) ; non linear gray scale
)

(rgb :ras3 [clag2 ras1]
  (rgb (mix (- 1 clag2.rgb) ras1.rgb ks6))
)

(expr :frag4 [ras3]
  "particle-blender/dFdxy-2.frag"
)

(hsv :ras5 [ras3 frag4]
  (hsv ras3.hsv)
  (h (+ kp1 ras3.h))
  (v (+ ras3.v (* 1 frag4.v)))
)
