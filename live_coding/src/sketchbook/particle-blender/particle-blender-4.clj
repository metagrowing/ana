(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1]
               [clag2 ras3])
(render)
; kp0    0.40625
; kp1    0.75781
; kp2    0.023622
; kp3    0.11024
; kp4    0
; kp5    0.36719
; kp6    0.71875
; kp7    0.23438
;
; ks0    0.29688
; ks1    0.3125
; ks2    0.5
; ks3    0
; ks4    0
; ks5    0.53125
; ks6    0.74219
; ks7    0.38281

(rgb :ras0 []
  ; (mirror)
  ; (rot (* -0.01 f))
  (r (fbm (* 0.3 x) y 1))
  (g (fbm (* 0.1 y) x 2))
)

(hsv :ras1 [ras0]
  (h (+ ks1 ras0.h))
  (s (* 0.7 ras0.s))
  (v (- r ks5))
)

(expr :clag2 [ras3]
  "particle-blender/particle-blender-1.cl"
  (paint 1)
  (p0 (* ks0 1))   ; vel
  (p7 (+ ks7 0.1)) ; non linear gray scale
)

(rgb :ras3 [clag2 ras1]
  (rgb (mix (- 1 clag2.rgb) ras1.rgb ks6))
)
