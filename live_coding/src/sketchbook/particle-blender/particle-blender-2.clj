(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 frag1]
               [ras3 clag2])
(render)
; kp0    0.40625
; kp1    0.75781
; kp2    0.023622
; kp3    0.11024
; kp4    0
; kp5    0.36719
; kp6    0
; kp7    0.64844
;
; ks0    0.39062
; ks1    0.46875
; ks2    0.5
; ks3    0
; ks4    0
; ks5    0
; ks6    0.9375
; ks7    0.99219

(rgb :ras0 []
  ; (r (fbm y x 1))
  (sx (+ 1 (usin (* 0.011 f))))
  (sy (+ 1 (usin (* 0.013 f))))
  (r (snoise (snoise (* 2 r) (* 2 x)) y))
  (g (* 0.05 (snoise (snoise (* 2 r) (* 2 x)) y)))
)
(expr :frag1 [clag2]
  "particle-blender/dFdxy-2.frag"
)

(expr :clag2 [ras3]
  "particle-blender/particle-blender-1.cl"
  (paint 1)
  (p0 (* ks0 1))   ; vel
  (p7 (+ ks7 0.1)) ; non linear gray scale
)

(rgb :ras3 [clag2 ras0]
  ; (rot (* -0.01 f))
  (rgb (mix clag2.rgb ras0.rgb ks6))
)
