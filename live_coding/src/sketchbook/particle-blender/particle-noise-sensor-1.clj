(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras4 mov0]
               [ras2 frag1]
               [clag3 frag1])
(movie "4movies/*.mp4")
(render)
; kp0    0.40625
; kp1    0.42188
; kp2    0.023622
; kp3    0.11024
; kp4    0
; kp5    0.36719
; kp6    0.71875
; kp7    0.23438
;
; ks0    0.99219
; ks1    0.03125
; ks2    0.98438
; ks3    0.3125
; ks4    0.39062
; ks5    0
; ks6    0.95312
; ks7    0.4375

(hsv :mov0 [c0 c1 c2 c3]
  (sx 1.5)
  (sy 1.5)
  (hsv c1.hsv)
)

(hsv :ras2 [frag1]
  (h frag1.h)
  (s 1)
  (v frag1.v)
)

(expr :frag1 [mov0]
  "particle-blender/noise-sensor-2.frag"
)
(expr :clag3 [ras2]
  "particle-blender/particle-blender-2.cl"
  (paint 1)
  (p0 (* ks0 1))   ; vel
  (p3 (* ks3 1))   ; vel
  (p7 (+ ks7 0.1)) ; non linear gray scale
)
(rgb :ras4 [clag3 frag1]
  (rgb (+ frag1.rgb clag3.rgb))
)
