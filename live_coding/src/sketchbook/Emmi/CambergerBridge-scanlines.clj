(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 s3line2]
	             [mopr4 ras5])

; original image by Emmi
; @emmi@mastodon.art
; View of Frankfurt from the Camberger Bridge
; CC Attribution-NonCommercial-ShareAlike 4.0 International
; https://cdn.masto.host/mastodonart/media_attachments/files/106/041/279/816/838/339/original/c627c76b4aeb9877.png
(image "Emmi/CambergerBridge-Frankfurt/*.png")
(render)

(rgb :img0 [c0]
	(rgb c0.rgb)
)

(expr :s3line2 [img0]
	(mode 0)
	; (drift (* 10 (snoise (* 5.1 y) 0.1 (sin (* 0.1 f)))))
	(fov (* 80 (usin (* 0.013 f))))
	(rotx (* 0.02 f))
  (dy 0.005)
  (dx 0.01)
	(jitter (* (usin (* 0.1 f)) 0.02 (+ r g b)))
)

(expr :mopr4 [s3line2] "dbb")

(rgb :ras5 [ras5 mopr4]
	(rgb (* 1.01 (mix ras5.rgb (* 1.5 mopr4.rgb) 0.15)))
)
