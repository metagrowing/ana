(ns lv.demo)
(use 'lv.core)

; ks0    0.086614
; ks1    0.61417

(layout "grid" [mov0 fdiff2]
               [bkg1 ras3])

; (movie "bee/P1140296.MOV")
(movie "bee/P1140300.MOV")
(render)
(hsv :mov0 [c0]
  (inv)
  (rot 1.5)
  (inv)
  (ty (* -1 ks0))
  (sy (* 2 ks1))
  (hsv c0.hsv)
)

(expr :bkg1 [mov0]
  (thres 0)
  (learn 2)
  (reset (- (usin (* 0.3 f)) 0.99))
)

(expr :fdiff2 [mov0])

; (rgb :ras3 [fdiff2 ras3]
;   (rgb (mix ras3.rgb fdiff2.rgb ks2))
; )
(rgb :ras3 [fdiff2 bkg1]
  (rgb (even? (int (/ (+ j i f) 16))
              fdiff2.rgb
              bkg1.rgb))
)
