(ns lv.demo)
(use 'lv.core)

; ks0    0.086614
; ks1    0.61417

(layout "grid" [mov0 fdiff1]
               [fdiff2 ras3])

; (movie "bee/P1140296.MOV")
(movie "bee/P1130726.MOV")

; ks0    0.19685
; ks1    0.055118
; ks2    0.6378
; ks7    0.17323

(hsv :mov0 [c0]
  ; (inv)
  ; (rot 1.5)
  ; (inv)
  ; (tx (* -1 ks0))
  (ty (* -1 ks1))
  (sy (* 2 ks2))
  (h c0.h)
  (s c0.s)
  (v (sqr c0.v))
)

(expr :fdiff1 [mov0])
(expr :fdiff2 [fdiff1])

(rgb :ras3 [fdiff1 mov0]
  (rgb (/ fdiff1.rgb
          (+ ks7 mov0.rgb)))
)

(render 3)
