(ns lv.demo)
(use 'lv.core)

(layout "grid" [load0 load1]
               [load2 ras3])

(expr :load0 []
  "/ssd/ana/bee/beat/*_0003.png"
  (k f)
)
(expr :load1 []
  "/ssd/ana/bee/calm-complement-exp_2021-04-30-06-20-35-490/*.png"
  (k (- f 250))
)
(expr :load2 []
  "/ssd/ana/bee/swarm-complement-oversat_exp_2021-05-30-13-20-16-742/*.png"
  (k (- f 1350))
)
(rgb :ras3 [load0 load1 load2]
  (rgb (+ load0.rgb
          (mix load1.rgb load2.rgb 0.5)))
)
(render)
