(ns lv.demo)
(use 'lv.core)

; ks0    0.71654
; ks1    0
; ks2    0.48819

(layout "grid" [img0 fdiff1]
               [conv2 ras3])
(image "bee/orbit-NonLinearHorizontalSlit_func=log1p,a=0.0,b=1.0_-20210429-103427.png")

(hsv :img0 [c0]
  (tx (* 3.7 (usin (* 0.01 ks0 f))))
  ; (ty (* -1 ks1))
  (sy (* 2 ks2))
  (hsv c0.hsv)
)

; Embossing Filter
(matrix :conv2 [fdiff1]
  2  0  0
  0 -1  0
  0  0 -1
)
; ; Embossing Filter
; (matrix :conv2 [fdiff1]
;   -2 -1  0
;   -1  1  1
;   0  1  2
; )
; ; Edge detection
; (matrix :conv2 [fdiff1]
;   0 -3  0
;  -3 12 -3
;   0 -3  0
; )

(expr :fdiff1 [img0])

(rgb :ras3 [fdiff1 conv2]
  (rgb (even? (int (/ (+ j i f) 16))
              fdiff1.rgb
              conv2.rgb))
)
