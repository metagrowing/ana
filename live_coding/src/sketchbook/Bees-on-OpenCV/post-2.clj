(ns lv.demo)
(use 'lv.core)

; kp6    0.13386
; kp7    0.88976
;
; ks7    0

(layout "grid" [load0 load1 load2]
               [mov0 ras3])

(movie "bee/P1140300.MOV")

(expr :load0 []
  "/ssd/ana/bee/beat/*_0003.png"
  (k f)
)
(expr :load1 []
  "/ssd/ana/bee/calm-complement-exp_2021-04-30-06-20-35-490/*.png"
  (k (- f 250))
)
(expr :load2 []
  "/ssd/ana/bee/swarm-complement-oversat_exp_2021-05-30-13-20-16-742/*.png"
  (k (- f 1200))
)
(hsv :mov0 [c0]
  (ty (* -1 ks7))
  (sy (* 2 kp7))
  (h c0.h)
  (s c0.s)
  (v (pos? (- kp6 (abs y))
           (even? (/ f 100)
                  (- 1 (sqr c0.v))
                  (sqr c0.v))
           0))
)
(rgb :ras3 [load0 load1 load2 mov0]
  (rgb (+ load0.rgb mov0.rgb
          (mix load1.rgb load2.rgb 0.5)))
)
(render 4)
