(ns lv.demo)
(use 'lv.core)

(texpr
  (t0 0.99)
  (t1 1.0)
  (t2 0.01)
  (t3 (sin (* 0.051 f)))
  (t4 (sin (* 0.071 f)))
  ; (t4 (-
  ;       (smoothstep (- t0 t2)
  ;                   (+ t0 t2)
  ;                   t3)
  ;       (smoothstep (- t1 t2)
  ;                   (+ t1 t2)
  ;                   t3)
  ;   ))
)
(layout "grid" [img0 img1]
               [ras2 fdiff1])
(image "bee/P1080829-1080x1080.png")

(hsv :img0 [c0]
  (h c0.h)
  (s c0.s)
  (v (* 3 c0.v (-
        (smoothstep (- t0 t2)
                    (+ t0 t2)
                    t3)
        (smoothstep (- t1 t2)
                    (+ t1 t2)
                    t3))))
)
(hsv :img1 [c0]
  (h (+ 0.5 c0.h))
  (s c0.s)
  (v (* 13 c0.v (-
        (smoothstep (- 0.9 t4)
                    (+ t0 t4)
                    t4)
        (smoothstep (- t1 t4)
                    (+ t1 t4)
                    t4))))
)
(rgb :ras2 [img0 img1]
  (rgb (+ img0.rgb img1.rgb))
)
(expr :fdiff1 [ras2])
(render)
