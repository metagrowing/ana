(ns lv.demo)
(use 'lv.core)

(layout "grid"
  [imgs0 ffb1] [mopr2 ras3])

(imagesequ "../../../timelapse/series/20210126-wind-winter/split-P1140130/*.png")
(render)

(hsv :imgs0 [c0]
  (k sframe)
  (hsv c0.hsv)
)

(expr :ffb1 [imgs0]
  (reduce 1)
  (mode 3)
  (gauss -1)
  (levels 2)
  (win 16)
  (iter 4)
  (polyn 7)
  (polys 1.2)
  (r 24)

  (step 3)
)
(render 1)

(expr :mopr2 [ffb1]
  "dddbb"
)
(rgb :ras3 [imgs0 mopr2]
  (rgb (* (* 2.8 imgs0.rgb)
            (* 1.0 mopr2.rgb)))
)
; (render 3)
