(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 imgs1]
               [imgs2 ras3])

(imagesequ "CalArts-Dance-2019/side-p1/*.png")
(render)
(rgb :imgs0 [c0]
  (k (% (int (+ sframe 0)) slength))
  (rgb c0.rgb)
)
(rgb :imgs1 [c0]
  (k (% (int (- slength sframe)) slength))
  (rgb c0.rgb)
)
(rgb :imgs2 [c0]
  (k (% (int (+ sframe 16)) slength))
  (rgb c0.rgb)
)

(rgb :ras3 [imgs0 imgs1 imgs2]
  (rgb (+ (* 0.2 imgs0.rgb)
          (* 0.6  imgs1.rgb)
          (* 0.2 imgs2.rgb)))
)
(render 3)
