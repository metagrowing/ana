(ns lv.demo)
(use 'lv.core)

(layout "grid" [mopr3 imgs0]
               [ras4 cline1]
               [ras5 ras3])

(imagesequ "CalArts-Dance-2019/blue/*.png")
(render)
(hsv :imgs0 [c0]
  (k (- slength sframe))
  ; (k sframe)
  (h c0.h)
  (s c0.s)
  (v c0.v)
)
(render 0)

(expr :cline1 [imgs0]
  (count 50)
  (max 5000)
  (thres 0.25)
  (fill -1)
  (paint -1)
  (holes -1)
)
(render 1)

(expr :mopr3 [cline1]
  "ddb"
)

(rgb :ras3 [mopr3]
  (sx -1)
  (tx 1)
  (rgb mopr3.rgb)
)

(rgb :ras4 [imgs0 mopr3]
  (rgb (max imgs0.rgb
            (* 2 mopr3.rgb)))
)

(rgb :ras5 [ras3 ras5]
  (rgb (mix (* 3 ras3.rgb)
            (* 1 ras5.rgb) 0.9))
)
(render 5)
(render)
