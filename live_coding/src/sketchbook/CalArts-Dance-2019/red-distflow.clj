(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 dist1 dist2]
               [ ras3 mopr4])

; (imagesequ "CalArts-Dance-2019/blue/*.png")
; (imagesequ "CalArts-Dance-2019/white/*.png")
(imagesequ "CalArts-Dance-2019/red/*.png")
(render)
(hsv :imgs0 [c0]
  (k sframe)
  (hsv c0.hsv)
)

(expr :dist1 [imgs0]
  (step 20)
  (in 0)
  (mode 1)
  (dist 6)
)
(expr :dist2 [imgs0]
  (step 20)
  (in 0)
  (mode 0)
  (dist 4)
)
(rgb :ras3 [dist1 dist2]
  ; (rgb (* 1.5 imgs0.rgb dist1.rgb))
  (rgb (+ (* 0.5 dist1.rgb)
          (* 2.5 dist2.rgb)))
)
(expr :mopr4 [ras3]
  "bb"
)
(render 4)
