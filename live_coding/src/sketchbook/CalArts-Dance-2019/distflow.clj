(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 dist1] [mopr2 ras3])

(imagesequ "CalArts-Dance-2019/blue/*.png")
; (imagesequ "CalArts-Dance-2019/white/*.png")
; (imagesequ "CalArts-Dance-2019/red/*.png")
(render)
(hsv :imgs0 [c0]
  (k sframe)
  (hsv c0.hsv)
)

(expr :dist1 [imgs0]
  (step 20)
  (in 0)
  (mode 0)
  (dist 4)
)
(expr :mopr2 [dist1]
  "bb"
)
(rgb :ras3 [imgs0 mopr2]
  ; (rgb (* 1.5 imgs0.rgb dist1.rgb))
  (rgb (+ (* 0.5 imgs0.rgb)
          (* 1.5 mopr2.rgb)))
)
