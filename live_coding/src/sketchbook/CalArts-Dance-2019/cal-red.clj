(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 ffb1]
               [mopr2 ras3]
               [ffb4 ras5])

(imagesequ "CalArts-Dance-2019/red/*.png")
(hsv :imgs0 [c0]
  ; (k (- slength sframe))
  (k sframe)
  (h c0.h)
  (s c0.s)
  ; (v (sqr c0.v))
  ; (v (sqr (sqr c0.v)))
  (v (* (sqrt (- 1 r)) c0.v))
)

(render 0)

(expr :ffb1 [imgs0]
  (levels 4)
  (win 6)
  (iter 4)
  (polyn 7)
  (polys 1.2)

  (step 8)
  (mode 2)
  (r (* 2 (sqrt z)))
  (res (+ 2 (* 2 z)))
  (sx 1.2) ; TODO
)

(expr :mopr2 [ras3]
  "ddb"
)

(rgb :ras3 [ffb1 ffb4]
  (rgb (+ ffb4.rgb
          ffb1.rgb))
)

; (rgb :ras3 [imgs0 mopr2 ffb4]
;   (rgb (+ (* (* 2 mopr2.rgb)
;              (* 1 imgs0.rgb))
;           ffb4.rgb))
; )
(render 3)

(rgb :imgs4 [c0]
  (k sframe)
  (rgb c0.rgb)
)
(expr :ffb4 [imgs0]
  (levels 2)
  (win 4)
  (iter 2)
  (polyn 7)
  (polys 1.2)

  (step 8)
  (mode 0)
  (r (* 0.1 (sqrt z)))
  (sx 1.2) ; TODO
  (paint -1)
)
(render 4)

(rgb :ras5 [imgs0 mopr2]
  (rgb (+
          (* 2 mopr2.rgb imgs0.rgb)
          ; (* 0.4 mopr2.rgb)
          (* 0.9 imgs0.rgb)
          ))
)
; (rgb :ras5 [imgs0 mopr2 ffb4 ras3]
;   (rgb (* 1.2
;           ; (* (* 4 mopr2.rgb) (* 1 imgs0.rgb))
;           ; (mix ffb4.rgb ras3.rgb 0.8)))
;           (max (* 0.5 ffb4.rgb) (* 1 ras3.rgb))))
; )
(render 5)
(render)
