(ns lv.demo)
(use 'lv.core)
; video: merry-go-round
(layout "grid" [imgs0 ras1] [ras2 ras3])
(imagesequ "../../../timelapse/series/20201231-2-plattenspieler/split/*.png")
(render)

(hsv :imgs0 [c0]
  (k (% (int (+ (* 0.2 j) sframe)) slength))
  (hsv c0.hsv)
)
(render 0)

(rgb :ras1 [imgs0]
  (tx (* 0.005 (snoise f x)))
  (rgb imgs0.rgb)
)
(render 1)

(rgb :ras2 [imgs0]
  (kaleid 5)
  (mirror)
  (tx (* 0.005 (snoise f x)))
  (rgb imgs0.rgb)
)
(render 2)

(rgb :ras3 [ras1 ras2]
  (rgb (mix ras1.rgb ras2.rgb (min 1 (/ f 2048))))
)
(render 3)
