(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 frag1] [ras2 ras3])
(image "daylily/*.png")
(sound "sound/beat1.mp3")
; kp6    0.85156
; kp7    0.69531
; ks0    0.99219
; ks1    0.63281
; ks2    0.49219
; ks3    0.53125
; ks4    0.78125
; ks7    0.50781
; (rgb :img0 [c0 c1 c2 c3 c4]
;   (rgb (* (+ (* ks0 f0    c0.rgb)
;              (* ks1 f1  2 c1.rgb)
;              (* ks2 f2  8 c2.rgb)
;              (* ks3 f3 16 c3.rgb)
;              (* ks4 f4 32 c4.rgb))
;            2 ks7))
; )


(rgb :img0 [c0 c1 c2 c3 c4]
  (rgb (* (max
            (max (min (* ks0 f0    c0.rgb)
                      (* ks1 f1  2 c1.rgb))
                 (min (* ks2 f2 12 c2.rgb )
                      (* ks3 f3 16 c3.rgb)))
            (* ks4 f4 32 c4.rgb))
          2 ks7) )
)

(rgb :ras2 [img0 ras2] (rgb (mix (pow img0.rgb (vec3 2 2 2))
                                 ras2.rgb
                                 kp6)))
; (expr :frag1 [ras2]
;   "particle-blender/dFdxy-2.frag"
; )
(expr :frag1 [ras2]
  "particle-blender/noise-sensor-2.frag"
)

(rgb :ras3 [frag1 ras2] (rgb (mix frag1.rgb
                                  ras2.rgb
                                  kp7)))
