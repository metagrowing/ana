(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 igen1]
               [ras2 ras3])

(image "daylily/*.png")
(sound "sound/beat1.mp3")
; kp0,0.99219
; kp1,0.42188
; kp2,0.023622
; kp3,0.11024
; kp4,0
; kp5,0.36719
; kp6,0.80469
; kp7,0.69531
;
; ks0,0.99219
; ks1,0.99219
; ks2,0.99219
; ks3,0.99219
; ks4,0.99219
; ks5,0.99219
; ks6,0.99219
; ks7,0.82031

(rgb :img0 [c0 c1 c2 c3 c4]
  (sx 1.8)
  (rgb (* (max
            (max (min (* ks0 f0    c0.rgb)
                      (* ks1 f1  2 c1.rgb))
                 (min (* ks2 f2 12 c2.rgb )
                      (* ks3 f3 16 c3.rgb)))
            (* ks4 f4 32 c4.rgb))
          2 ks7) )
)

(expr :igen1 [ras3]
  "r2/r2image.cl"
  (p0 (* kp0 3))
  (p1 (* (usin (* 0.007 f)) 3))
  (p2 (* (usin (* 0.011 f)) 3))
  (p3 (* (usin (* 0.013 f)) 3))
  (p4 (* (usin (* 0.023 f)) 3))
  (p7 (* kp7 3))
)

(rgb :ras2 [img0 igen1 ras2]
  (rgb (mix (* 3 img0.rgb igen1.rgb) ras2.rgb 0.95))
)

(rgb :ras3 [img0 igen1]
  (rgb (+ img0.rgb igen1.rgb))
)
(render)
