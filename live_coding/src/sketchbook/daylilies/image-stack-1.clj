(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1] [img2 img3])
(image "daylily/*.png")
; ks0    0.29688
; ks1    0.6875
; ks2    0.3125
; ks3    0.82031
; ks4    0.61719
; ks5    0.99219

(rgb :img0 [c0 c1 c2 c3 c4]
  (rgb (* (+ c0.rgb c1.rgb c2.rgb c3.rgb c4.rgb) ks0))
)
(rgb :img1 [c0 c1 c2 c3 c4]
  (rgb (* (* c0.rgb c1.rgb c2.rgb c3.rgb c4.rgb) (* 5 ks1)))
)
(hsv :img2 [c0 c1 c2 c3 c4]
  (h (* (+ c0.h c1.h c2.h c3.h c4.h) ks2))
  (s (/ (* c0.s c1.s c2.s c3.s c4.s) ks3))
  (v (/ (* c0.v c1.v c2.v c3.v c4.v) ks4))
)
(rgb :img3 [c0 c1 c2 c3 c4]
  (rgb (* (max (min (max c0.rgb c1.rgb) (max c2.rgb c3.rgb)) c4.rgb) ks5) )
)
