(ns lv.demo)
(use 'lv.core)

(layout "grid" [clvb0 mopr1]
               [ras2])

(expr :clvb0 []
  "particle/particle-3dtrace.cl"
  (al 4096)
  (tl 32)

  (rx (* 0.0071 f))
  (ry (* 0.0053 f))
  (rz (* 0.0023 f))
  ; (rx (* 6.28 kp5))
  ; (ry (* 6.28 kp6))
  ; (rz (* 6.28 kp7))

  ; (p0 5)
  ; (p1 0)
  ; (p2 -5)
  ; (p3 0)

  (p0 (+ 0.25 (* 0.25 (+ 1 lx))))
  (p1 (+ 0.25 (* 0.25 (+ 1 ly))))
  (p2 (+ 0.25 (* 0.25 (+ 1 rx))))
  (p3 (+ 0.25 (* 0.25 (+ 1 ry))))

  (p6 ks6)
  (p7 ks7)
)

(expr :mopr1 [clvb0]
  "dbb"
)

(rgb ras2 [mopr1 ras2]
  (rgb (mix ras2.rgb (* 2.5 mopr1.rgb) 0.1))
)
; (render 2)
(render 2)
