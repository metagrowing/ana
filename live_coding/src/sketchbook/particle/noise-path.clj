(ns lv.demo)
(use 'lv.core)

(layout "grid" [clvb0 mopr1]
               [ras2])

(expr :clvb0 []
  "particle/noise-path.cl"
  (al (* 8 128))
  (tl 12)

  (rx (* 0.0017 f))
  (ry (* 0.0013 f))
  (rz (* 0.00023 f))
  ; (rx (* 6.28 kp0))
  ; (ry (* 6.28 kp1))
  ; (rz (* 6.28 kp2))

  (p0 (+ 0.25 (* 0.25 (+ 1 lx))))
  (p1 (+ 0.25 (* 0.25 (+ 1 ly))))
  (p2 (+ 0.25 (* 0.25 (+ 1 rx))))
  (p3 (+ 0.25 (* 0.25 (+ 1 ry))))

  (p5 ks4)         ; octaves
  (p5 (* 0.1 ks5))  ; noise input scale
  (p6 (* 100 ks6))  ; noise output scale
  (p7 (* 10 ks7))  ; focus force
)

(expr :mopr1 [clvb0]
  "dbb"
)

(rgb ras2 [mopr1 ras2]
  (rgb (mix ras2.rgb (* 2.5 mopr1.rgb) 0.1))
)
; (render 2)
(render 2)
