(ns lv.demo)
(use 'lv.core)

; (layout "grid" [img0 img1] [ras2 ras3])
(layout "grid" [ras0 ras1] [ras2 ras3])

(render)
; ks0    0.11024
; ks1    0.50394
; ks2    0.97638
; ks3    0.70866
; ks4    0.094488
; ks5    0.84252
; ks6    0.56693
; ks7    0.89764

(hsv :ras0 []
  (kaleid (* ks0 1))
  ; (mirror)
  ; (rot (* 0.013 f))
  (v (low? (+ -0.5 y (turb (* 3 x) (* 0.011 f) 1)) 0.02 0 1))
)

(hsv :ras1 []
  (kaleid (* ks1 1))
  ; (rot (* 0.013 f))
  (v (low? (+ -0.5 y (turb (* 3 x) (* 0.013 f) 1)) 0.01 1 0 ))
)

(rgb :ras2 [ras0 ras1]
  (rgb (mix ras0.rgb ras1.rgb 0.5))
)

(hsv :ras3 [ras2 ras1]
  (mirror)
  (v ras2.v)
  (s ras2.v)
  (h ras2.v)
)
