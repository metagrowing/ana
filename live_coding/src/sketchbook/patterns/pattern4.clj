(ns lv.demo)
(use 'lv.core)

; (layout "grid" [img0 img1] [ras2 ras3])
; (layout "grid" [ras0 fdiff1 mopr2] [ras3 ras4 ras3])
(layout "grid" [ras0 fdiff1 mopr2] [ras3 ras4])

(render)
; ks0    0.11024
; ks1    0.50394
; ks2    0.97638
; ks3    0.70866
; ks4    0.094488
; ks5    0.84252
; ks6    0.56693
; ks7    0.89764

(hsv :ras0 []
  ; (kaleid (* ks0 1))
  ; (mirror)
  ; (rot (* 0.013 f))
  (h (low? (+ -0.5 y (turb (* 3.31 x) (* 0.013 f) 1)) 0.05 0.05 0.06))
  (s (low? (+ -0.5 y (turb (* 3.13 x) (* 0.013 f) 1)) 0.02 0.25 0.75))
  (v (low? (+ -0.5 y (turb (* 2.97 x) (* 0.011 f) 1)) 0.02 0 1))
)

(expr :fdiff1 [ras0])

(expr :mopr2 [fdiff1] "ddebb")

(rgb :ras3 [mopr2 ras3]
  (rot (* r -0.05))
  (sx (* r 2.995))
  ; (mirror)
  (rgb (mix mopr2.rgb ras3.rgb ks7))
)

(rgb :ras4 [mopr2 ras3]
  ; (inv)
  ; (mirror)
  ; (inv)
  ; (rot -0.01)
  (rgb (+ mopr2.rgb ras3.rgb))
)
