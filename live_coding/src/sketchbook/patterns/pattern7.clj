(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1])
(sound "sound/Crowander-Mountains.mp3")

(hsv :ras0 []
  (h f0)
  (s (- 1 r))
  (v (low? (sin (* 13 f0))
                (fract (* r 7 (turb (* 11 f1 (* 0.1 y)) (* 17 f2) 2)))
                1 0))
)

; ks7    0.83465
(rgb ras1 [ras0 ras1]
  (rot (- f3 0.01))
  (tx (- f4 0.005))
  (rgb (mix ras0.rgb ras1.rgb ks7))
)
(render 1)
; (expr :fdiff1 [ras1])
