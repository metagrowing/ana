(ns lv.demo)
(use 'lv.core)

; (layout "grid" [img0 img1] [ras2 ras3])
(layout "grid" [img0 img1] [ras2 ras3])

; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(sound "sound/Crowander-Mountains.mp3")
(render)

; kp0    0.62205
; kp1    0.30709
; kp2    0.023622
; kp3    0.11024
; kp4    0
; kp5    0.20472
; kp6    0.055118
; kp7    0.59055
;
; ks0    0.54331
; ks1    0.66929
; ks2    0.97638
; ks3    0.70866
; ks4    0.094488
; ks5    0.84252
; ks6    0.56693
; ks7    0.75591

(hsv :img0 [c0 c1 c2 c3]
  (rot (* 0.023 f))
  (sx 1.8)
  (pixelate-x (* a 20))
  (hsv (pos? (- (distance (vec2 x y)
                          (vec2 (- ks0 0.5) (- ks1 0.5)))
                (* 20 kp0 f2))
             (vec3 0 0 0)
             c1.hsv
             ))
)
(render 0)

(hsv :img1 [c0 c1 c2 c3]
  (rot (* -0.011 f))
  (sx 1.8)
  ; (pixelate-y 20)
  (pixelate-y (* r 20))
  (hsv (pos? (- (distance x
                          (- ks2 0.5))
                (* 10 kp2 f0))
             (vec3 0 0 0)
             (pos? (- (distance y
                                (- ks3 0.5))
                           (* 10 kp3 f1))
                        (vec3 0 0 0)
                        c2.hsv
                        )
             ))
)
(render 1)

(rgb :ras2 [img0 img1]
  (rgb (mix (* 2 img0.rgb)
            (* 4 img1.rgb)
            0.5))
  ; (rgb (max img0.rgb
  ;         img1.rgb))
)
(render 2)

(rgb :ras3 [ras2 ras3]
  (rgb (mix ras2.rgb ras3.rgb ks7))
  ; (rgb ras2.rgb)
)
(render 3)
(render)
