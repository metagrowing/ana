(ns lv.demo)
(use 'lv.core)

; (layout "grid" [img0 img1] [ras2 ras3])
(layout "grid" [img0 ras1] [ras2 ras3])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(sound "sound/Crowander-Mountains.mp3")

(render)

(hsv :img0 [c0 c1 c2 c3]
  (sx 1.8)
  (pixelate-x (* a 20))
  (hsv c3.hsv)
)

(hsv :ras1 []
  (h (* 100 onset))
  (s (* 100 r f2))
  (v (smoothstep (* ks0 f0)
                 (+ (* ks0 f0) 0.01)
                 (length (- (fract (* f1 3 (vec2 (snoise r a) x))) 0.5))))
)

(rgb :img1 [c0 c1 c2 c3]
  (sx 1.8)
  (rot (* -0.02 f))
  (rgb (mix c0.rgb c1.rgb
           (mod (+ (snoise (*  -0.0017 f) x)
                   (floor (* 9 (+ (*  0.0011 f) y)))
                   (floor (* 7 (+ (* -0.0031 f) y))))
                 2)))
)

(rgb :ras2 [ras0 img1]
)

(rgb :ras3 [img0 ras1 ras3]
  (rot (- f2 (* 0.1 kp7)))
  (rgb (mix img0.rgb
            (mix (- r ras1.rgb) ras3.rgb ks7)
            0.9)
  )
)
(render 3)
; (render)
