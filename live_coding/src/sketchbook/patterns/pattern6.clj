(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 mopr2 fdiff1]
               [ras3 ras4 ras5]
               [ras6])
; ks6    0.03937
; ks7    0.1811

(hsv :ras0 []
  (v (low? (sin (* 0.5 0.013 f))
                (fract (* y 7 (turb (* 0.5 0.011 f (* 0.1 x)) y 1)))
                1 0))
)

(expr :fdiff1 [mopr2])

(expr :mopr2 [ras0] "ddddbb")

(rgb :ras3 [mopr2 ras3]
  (sx (* r 2))
  (rot (* x -1))
  (mirror)
  (rgb (mix mopr2.rgb ras3.rgb ks7))
)

(rgb :ras4 []
  (r (snoise x y))
)

(rgb :ras5 [ras4]
  (pixelate-x 250)
  (pixelate-y 250)
  (r ras4.r)
)
(rgb :ras6 [ras3 ras5]
  (rgb (pos? (- ras5.r ks6)
             (* ras3.rgb ras5.rgb)
             ras3.rgb))
)
(render 6)
