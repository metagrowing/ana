; video: a cab ride got wild
(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 imgs1] [ras2 fdiff3])
; assume there are lots of JPEG images in the media folder.
; all imgaes have the same size 1080 x 1080
(imagesequ "rp-b-1080/*.png")
(render)

(hsv :imgs0 [c0]
  (k (% (int (* 1.97 f)) slength))
  (hsv (* (vec3 1 1 2) c0.hsv))
)
(render 0)

(hsv :imgs1 [c0 imgs2]
  (k (+ (* 75 imgs2.v)
        sframe))
  (hsv c0.hsv)
)
(render 1)

(rgb ras2 [imgs0 imgs1]
  (rgb (mix imgs0.rgb imgs1.rgb 0.5))
)
(render 2)

(expr :fdiff3 [ras2])
(render 3)
