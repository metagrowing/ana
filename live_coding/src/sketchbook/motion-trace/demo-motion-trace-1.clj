(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 mt1] [ras2 uspl3])
(movie "4movies/*.mp4")

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
  (sy 1.5)
  (rgb c1.rgb)
)
(render 0)

(expr :mt1 [mov0]
  (mode 1)
  (trace 50)
  (paint -1)
  (count 30)
  (qual 0.01)
  (min 5)
)
(render 1)

(rgb :ras2 [mt1 ras2]
  (rgb (mix (* 16 mt1.rgb)
            ras2.rgb
            0.97))
)
(render 2)

(expr :uspl3 [mt1]
  "cl/processVoronoiPoints.cl"
)
(render 3)

(render)
