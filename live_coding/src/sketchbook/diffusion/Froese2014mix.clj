; width and height must be power of 2
(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1]
               [clpp0 ras1])
(image "Mastodon-#ArtReference/shipwreck-ship-wreckage-2096945/point-reyes-*.png")

(rgb img0 [c0 c1]
  (rgb c0.rgb)
)
(rgb img1 [c0 c1]
  (rgb c1.rgb)
)
(expr :clpp0 []
  "cl/Froese2014.cl"
  (loops 25)
)

(rgb :ras1 [img0 img1 clpp0]
  (rgb (mix img0.rgb
            img1.rgb
            (sqr (log clpp0.b))))
            ; 0.5))
)
(render 3)
