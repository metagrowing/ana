(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 sline1 sline2]
	             [mopr3 ras4 ras5])

(movie "4movies/*.mp4")
(render)
; movie source
(rgb :mov0 [c0 c1 c2 c3]
	(sy 1.5)
  (rgb c3.rgb)
)
; concentric scan lines
(expr :sline1 [mov0]
	(mode 2)
	(clear -1)
  (dy (/ 3.14149 300))
  (dx 0.045)
	(jitter (* -0.05 (+ r g b)))
)
; horizontal scan lines
(expr :sline2 [mov0]
	(mode 0)
	(drift (* 0.2 (snoise (* 5.1 y) 0.1 (sin (* 0.1 f)))))
	(clear 1)
  (dy 0.023)
  (dx 0.030)
	(jitter (* -0.02 (+ r g b)))
)
; morphological operator
; dilate blur blur
(expr :mopr3 [sline2] "dbb")
; compose maximum rgb from 2 filters
(rgb :ras4 [sline1 mopr3]
	(rgb (max sline1.rgb mopr3.rgb))
)
(hsv :ras5 [sline1 sline2]
	; (hsv (max sline1.hsv sline2.hsv))
	(rot (* 0.0071 f))
	(mirror)
	(kaleid 7)
	(rot (* -0.071 f))
	; (inv)
	(h sline1.h)
	(s sline1.h)
	(v (+ sline1.v sline2.v))
)
; (render 5)
