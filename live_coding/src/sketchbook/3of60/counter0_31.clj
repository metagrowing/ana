(ns lv.demo)
(use 'lv.core)
(layout "grid" [bill0 count1]
               [mopr2 conv3])

(expr :bill0 []
  (clear 0)
  (count (* 8 4))
  (x (+ -960 (* (/ 1920 8) (floor (% (+ i (% f 32)) 8)))))
  (y (+ -400 (* (/ 1080 4) (floor (/ (+ i (% f 32)) 8)))))
  (size (/ 1080 4))
)

(expr :count1 [bill0]
  (clear (zero? (% f 17) 0 -1))
  (size 120)
)

; Edge detection
(matrix :conv3 [count1]
  0 -3  0
 -3 12 -3
  0 -3  0
)

; Morphological Operations
; 4 * dilate
; 2 * erode
; 1 * blur
(patch :mopr2 [conv3]
  "bddde"
)

(render)
