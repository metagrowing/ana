(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1] [ras2 ras3])
; show all filters
(render)

; generate animated rings
(hsv :ras0 []
  (ty (* 0.002 f))
  (h (+ 0.49 (* 0.05 (snoise y x))))
  (s 1)
  (v (/ (floor (* 7 (snoise x y)))
        7))
)

; generate a rotating star
(hsv :ras1 []
  (rot (* 0.002 f))
  (h (+ 0.05 (* 0.05 (snoise y x))))
  (s 1)
  (v (/ (floor (* 7 (snoise x y)))
        7))
)

; mix both upstream filters
(rgb :ras2 [ras0 ras1]
  (rgb (+ ras0.rgb ras1.rgb))
)

; mix ras0 and ras2 controlled by ras1
(hsv :ras3 [ras0 ras1 ras2]
  (hsv (pos? (snoise (* 0.01 f)
                     (* 5 ras2.v))
              ras0.hsv
              ras1.hsv))
)

(render 3)
