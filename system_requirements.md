# System requirements
If you plan to compile aNa on your own computer there is no need to have exactly the same environment. Except that the visualization server needs a hardware accelerated graphics card. For recording the animation ANA is using a very memory expensive strategy. But this build-in screen recorder can be disabled. You can use your preference screen reorder.

All satellites are nice to have. Server and editor are the only required subsystems. Maybe the Atom based live coding editor can be replaced. At the current development state there are some essentially required code generation components on the editor side. So that this needs some work to use a different live coding editor.

## List of hard and software used during development of aNa.

### Visualization server
#### Software
- Operating System: 64-Bit Debian GNU/Linux 10
- openFrameworks: of_v0.11.0_linux64gcc6_release
- Compiler: gcc version 8.3.0
- NVIDIA Driver Version: 418.152.00

#### Hardware
- Processors: 16 × Intel® Core™ i9-9900K CPU @ 3.60GHz
- Memory: 32 GiB of RAM
- 3D Accelerator: GeForce RTX 2080 Ti

### Live coding editor.
#### Software
- Clojure 1.10.0
- Atom 1.47.0
- required Atom packages: proto-repl 1.4.24 (proto-repl can use ink 0.12.2. Do not update ink.)
- optional Atom packages: ink 0.12.2

#### Hardware
A Laptop can be used. Or the editor can run together with the server on the same computer.

### SenseHAT satellite
#### Software
- Operating System: 2020-08-20-raspios-buster-armhf-full
- OSC listener: osc4py3

#### Hardware
- Raspberry Pi 3 Model B
- SenseHAT

## List of software used for testing on a virtual PC
- Host Operating System: 64-Bit Debian GNU/Linux 10
- Oracle VM VirtualBox: virtualbox-6.1
- Guest Operating System: 64-Bit Ubuntu 20.04
- Guest Additions: Guest Additions where installed inside the virtualbox.
- OpenGL support used inside the VM: Mesa 20.0.8
- openFrameworks: of_v0.11.0_linux64gcc6_release
- Compiler: gcc (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0
- Clojure 1.10.1
- Atom 1.53.0



