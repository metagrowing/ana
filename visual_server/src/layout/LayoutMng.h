#pragma once
#include "Filter.h"
#include "OscListener.h"
#include "Layout.h"

class LayoutMng {
public:
    LayoutMng();
    virtual ~LayoutMng();
    virtual Layout* update(std::string descr);

    const std::vector<std::string>& getFid() const {
        return fid;
    }

    void setRender(std::string descr);
    void setRender(int r);
    void nextRender();
    void previousRender();
    void draw(std::vector<std::shared_ptr<Filter>> &filterLine);
    std::shared_ptr<Filter> loadInstance(const string fxkey, ofGLWindowSettings window_settings, OscListener * oscParams);
    void loadPipeline(std::vector<std::shared_ptr<Filter>> &filterLine, ofGLWindowSettings window_settings, OscListener * oscParams);

private:
    Layout *layout = nullptr;
    int render = 0;
    int rows = 2;
    int cols = 2;
    std::vector<std::string> fid;
};
