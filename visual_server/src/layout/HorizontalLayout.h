#pragma once
#include "Filter.h"
#include "Layout.h"

class HorizontalLayout: public Layout {
public:
    HorizontalLayout(int r);
    virtual ~HorizontalLayout();
    virtual int numFilters();
    virtual void draw(int render, std::vector<std::shared_ptr<Filter>> &filterLine);

private:
    int rows = 3;
};
