#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"
#include "ofApp.h"
#include "layout/HorizontalLayout.h"

HorizontalLayout::HorizontalLayout(int r) {
    rows = r > 0 ? r : 1;
}

HorizontalLayout::~HorizontalLayout() {
}

int HorizontalLayout::numFilters() {
    return rows;
}

void HorizontalLayout::draw(int render, std::vector<std::shared_ptr<Filter>> &filterLine) {
    int w = ofGetWidth();
    int h = ofGetHeight();
    bool badrange = false;
    if(render >= 0) {
        if((int) filterLine.size() > render) {
            auto s = filterLine[render];
            ofFbo &fbo = s->getFbo();
            int top_border  = std::max(0, (h - asInt(FILTER_HEIGHT)) / 2);
            int left_border = std::max(0, (w - asInt(FILTER_WIDTH)) / 2);
            fbo.draw(left_border, top_border);
        } else {
            badrange = true;
        }
    }
    if(badrange || render < 0) {
        int row_height = asInt(CANVAS_HEIGHT) / rows;
        int top_border  = std::max(0, (h - row_height * rows) / 2);
        int left_border = std::max(0, (w - asInt(FILTER_WIDTH)) / 2);
        int v = 0;
        for(auto s : filterLine) {
            ofFbo &fbo = s->getFbo();
            fbo.draw(left_border, top_border + row_height * v,
                     asInt(FILTER_WIDTH), row_height);
            ++v;
        }
    }
}
