#pragma once

#include <string>
#include <vector>
#include "ofMain.h"

class PreShot {
public:
    PreShot();
    ~PreShot();
    void setup();
    void reset();
    void update();
    bool isEnabled();
    void saveRecording(std::string prefix);
    void clear();

    static std::vector<void*> buffers;
    static unsigned int width;
    static unsigned int height;

private:
    size_t capture_lenght = 0;
    size_t preshot_max = 0;
    size_t preshot_frame = 0;
    size_t capture_index = 0;
    unsigned int epoch = 0;
};

inline bool PreShot::isEnabled() { return preshot_max > 0; }
