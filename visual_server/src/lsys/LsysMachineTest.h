#pragma once
#include <string>
#include <vector>

#include "ana_features.h"

class LsysMachineTest {
public:
    LsysMachineTest();
    virtual ~LsysMachineTest();

#ifdef WITH_testcases
    static void runTestCases();
    static void runProfile();
    static void check(const char *expected,
            bool expected_post,
            std::string axiom,
            std::vector<std::string> predecessor,
            std::vector<std::string> successor,
            std::vector<std::string> leftContext,
            std::vector<std::string> rightContext,
            int depth,
            const bool deterministic = true);
    static void lContext(bool expected, int at, char testChar, std::string pattern);
    static void rContext(bool expected, int at, char testChar, std::string pattern);
    static void prof();

    static int test_exec_failed;
    static int test_syntax_failed;
#endif
};
