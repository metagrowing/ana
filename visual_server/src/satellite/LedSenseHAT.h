#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "calc/ScalarLisp.h"

class LedSenseHAT: public Filter {
public:
    LedSenseHAT(ofGLWindowSettings settings, OscListener *oscParams);
    virtual ~LedSenseHAT();
    virtual void setup(const std::string id, int numSamples);
    virtual void reload(ofxOscMessage const &msg);
    virtual void update();
    virtual void draw(int li);
    virtual void toMermaid(std::stringstream &info);

    ScalarLisp slMode;
    ScalarLisp slH;
    ScalarLisp slS;
    ScalarLisp slV;
    int draw_mode = 0;
    ofxOscSender talkToSenseHAT;

private:
    static const int led_x_led = 8;
    ofFloatColor preview[led_x_led][led_x_led];

    void addCalculatedToRow(size_t ix, size_t iy, int i, ofxOscMessage &senseHAT);
    void addPixelToRow(const ofFloatPixels &pixels, size_t iy, size_t ypos, ofxOscMessage &senseHAT);
};
