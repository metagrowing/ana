#include "ofMain.h"
#include "globalvars.h"
#include "ofApp.h"
#include "Configuration.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "Filter.h"
#include "frag/FragFilter.h"
#include "frag/FragDoubleBufferFilter.h"

FragDoubleBufferFilter::FragDoubleBufferFilter(ofGLWindowSettings settings, OscListener * oscParams) :
    FragFilter (settings, oscParams) {
}

FragDoubleBufferFilter::~FragDoubleBufferFilter() {
}

void FragDoubleBufferFilter::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
    fbo0.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, 0); //TODO use numSamples instead of 0
    fbo1.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, 0); //TODO use numSamples instead of 0
}

void FragDoubleBufferFilter::draw(int li) {
    (ringIndex == 0 ? fbo1 : fbo0).begin();
    if(glslProg.fatalFragProg || glslProg.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
        glslProg.shader.begin();
        sendUniforms(glslProg.shader, li);
        for(auto &u : glslProg.uniforms) {
            glslProg.shader.setUniform1f(u.name, u.slFloat.rpn.execute0());
        }

        auto tx = 0;
        for(std::vector<std::string>::iterator it = upstream_ids.begin(); it != upstream_ids.end(); ++it) {
            std::string in_id = *it;
            if(filterDict.find(in_id) != filterDict.end()) {
                auto f = filterDict[in_id];
                if(f)
                    glslProg.shader.setUniformTexture("tex" + to_string(tx), f->getTexture(), tx);
            }
            ++tx;
        }

        ofDrawRectangle(0, 0, filterWidth, filterHeight);

        glslProg.shader.end();
    }
    (ringIndex == 0 ? fbo1 : fbo0).end();
}

void FragDoubleBufferFilter::finish() {
    ringIndex = (ringIndex == 0 ? 1 : 0);
}

