#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "glslhelper.h"
#include "OscListener.h"


class FragMultipassFilter : public Filter{
	public:
		FragMultipassFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~FragMultipassFilter();
		virtual void setup(const std::string id, int numSamples);
		virtual void reload(ofxOscMessage const &msg);
		virtual void update();
		virtual void draw(int li);

	private:
		ofFbo tmpFbo0;
		ofFbo tmpFbo1;
		ofShader passThroughShader;
        std::vector<GlslProgMng> glslProgPool;
        std::vector<GlslProgMng> fxPipeline;

    void updatePipeline();
    void insertFallback();
};
