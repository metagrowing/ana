#pragma once
#include "ana_features.h"
#include "calc/ScalarLisp.h"

typedef struct {
    std::string name;
    std::string term;
    ScalarLisp slFloat;
//    float value;
} UnifomExpression;

typedef struct {
    std::filesystem::file_time_type fragProgModTime;
    bool fatalFragProg = false;
    ofShader shader;
    std::vector<UnifomExpression> uniforms;
    std::filesystem::path pathFragProg;
    std::string fragProgName;
    std::vector<std::string> inject;
    std::string maindef;
} GlslProgMng;

extern bool reloadFragProg(const bool force, GlslProgMng & glslProg);
extern void compilePrivateShader(GlslProgMng & glslProg);
