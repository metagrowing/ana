#pragma once

#include <string>

class ColorNames {
public:
    static void test();
    static std::string nearestRGB(float r, float g, float b);
};
