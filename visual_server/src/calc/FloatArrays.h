#pragma once
#include "Configuration.h"

class FloatArrays  {
	public:
		FloatArrays();
		float * zeros(const size_t capacity);
		float * constant(float value, size_t capacity);
		float * uniform(float min, float max, size_t capacity);
		float * gaussian(size_t capacity);
};
