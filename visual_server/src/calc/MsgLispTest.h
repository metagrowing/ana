#pragma once
#include <string>

#include "ana_features.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"


class MsgLispTest {
public:
    MsgLispTest();
    virtual ~MsgLispTest();

#ifdef WITH_testcases
    static void runTestCases();
    static void runProfile();
    static void check(std::string expected, std::string input);
    static void errcheck(std::string input);

    static int test_exec_failed;
    static int test_syntax_failed;
    static ScalarLisp sl;

    static void runAstTestCases();
    static void runLispTestCases();
#endif // WITH_testcases
};
