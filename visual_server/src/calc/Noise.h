#pragma once

// JAVA REFERENCE IMPLEMENTATION OF IMPROVED NOISE - COPYRIGHT 2002 KEN PERLIN.
// http://mrl.nyu.edu/~perlin/noise/

// This is a C++ version of Ken Perlins code.

class ImprovedNoise {
public:
    ImprovedNoise();
    void seed(long seed);
    double snoise(double x, double y, double z);

private:
    int p[512];
};
