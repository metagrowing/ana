#pragma once
#include "../ana_features.h"
#ifdef WITH_openCL

#include "ofxOsc.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "calc/FloatArrays.h"
#include "calc/ScalarLisp.h"
#include "cl/ClFilter.h"

class ClAgentPingPong : public ClFilter {
	public:
		ClAgentPingPong(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ClAgentPingPong();
		virtual void setup(const std::string id, int numSamples);
	    virtual ofTexture& getTexture();
		virtual void reload(ofxOscMessage const &msg);
	    virtual bool reloadClProg(const bool force);
		virtual void update();
		virtual std::string dumpState();

    protected:
        msa::OpenCLImage clImageInput;
        bool hasImageInput = false;

        msa::OpenCLBuffer clPositionBuffers[2]; // ping pong buffers with the same size
        msa::OpenCLBuffer clVelocityBuffers[2]; // ping pong buffers with the same size
        msa::OpenCLBuffer clHeadingBuffers[2]; // ping pong buffers with the same size
        msa::OpenCLBuffer clTrailBuffers[2]; // ping pong buffers with the same size

        msa::OpenCLKernelPtr ag_setup_kernel;
        msa::OpenCLKernelPtr trail_setup_kernel;
        msa::OpenCLKernelPtr ag_move_kernel;
        msa::OpenCLKernelPtr trail_disipate_kernel;
//        msa::OpenCLKernelPtr ag_clear_kernel;
        msa::OpenCLKernelPtr ag_draw_kernel;

        FloatArrays floats;
        const size_t agentlength = 250*1000;
//        const size_t agentlength = 1000;
        int  pingPongIndex = 0;
        ofFbo clOutFbo;

        float* position = nullptr;
        float* velocity = nullptr;
        float* heading = nullptr;

        ScalarLisp slUpstream;
        ScalarLisp slP0;
        ScalarLisp slP1;
        ScalarLisp slP2;
        ScalarLisp slP3;
        ScalarLisp slP4;
        ScalarLisp slP5;
        ScalarLisp slP6;
        ScalarLisp slP7;
        double p0 = 0;
        double p1 = 0;
        double p2 = 0;
        double p3 = 0;
        double p4 = 0;
        double p5 = 0;
        double p6 = 0;
        double p7 = 0;

        void run_ag_setup_kernel(msa::OpenCLKernelPtr kernel);
        void run_trail_kernel(msa::OpenCLKernelPtr kernel);
        void run_ag_move_kernel(msa::OpenCLKernelPtr kernel);
        void run_ag_clear_kernel(msa::OpenCLKernelPtr kernel);
        void run_ag_draw_kernel(msa::OpenCLKernelPtr kernel);
};
#endif // WITH_openCL
