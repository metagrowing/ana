#pragma once
#include "../ana_features.h"
#ifdef WITH_openCL

#include "ClPingPong.h"

class ClUpstreamPingPong: public ClPingPong {
public:
    ClUpstreamPingPong(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~ClUpstreamPingPong();
    virtual void setup(const std::string id, int numSamples);
    virtual void reload(ofxOscMessage const &msg);
//    virtual void update();

protected:
    virtual void runKernel(msa::OpenCLKernelPtr kernel);
    bool hasUpstreamImage = false;
    msa::OpenCLImage clInImage;
    msa::OpenCLImage clFallbackImage;
};
#endif // WITH_openCL

