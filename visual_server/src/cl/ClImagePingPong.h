#pragma once
#include "../ana_features.h"
#ifdef WITH_openCL
#ifdef BROKEN_stuff

#include "ofxOsc.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "calc/ScalarLisp.h"
#include "cl/ClFilter.h"

class ClImagePingPong : public ClFilter {
	public:
		ClImagePingPong(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ClImagePingPong();
		virtual void setup(const std::string id, int numSamples);
	    virtual ofTexture& getTexture();
		virtual void reload(ofxOscMessage const &msg);
		virtual void update();
	    virtual void draw(int li);

    protected:
		virtual size_t getKernelWidth();
		virtual size_t getKernelHeight();

        msa::OpenCLImage clImageInput;
        msa::OpenCLImage clImageBuffers[2]; // ping pong buffers with the same size

        int  pingPongIndex = 0;
        bool hasImageInput = false;

        ScalarLisp slUpstream;
        ScalarLisp slLoops;
        ScalarLisp slP0;
        ScalarLisp slP1;
        ScalarLisp slP2;
        ScalarLisp slP3;
        ScalarLisp slP4;
        ScalarLisp slP5;
        ScalarLisp slP6;
        ScalarLisp slP7;

        virtual bool isSaveForKernel();
        virtual void runKernel(msa::OpenCLKernelPtr kernel);
};
#endif // BROKEN_stuff
#endif // WITH_openCL
