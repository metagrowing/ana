#pragma once
#include "../ana_features.h"
#ifdef WITH_openCL

#include <random>
#include "ofxOsc.h"
#include "MSAOpenCL.h"
#include "calc/FloatArrays.h"
#include "calc/ScalarLisp.h"
#include "cl/ClFilter.h"
#include "MSAOpenCL.h"

class ClImageGenerator : public ClFilter {
	public:
		ClImageGenerator(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ClImageGenerator();
		virtual void setup(const std::string id, int numSamples);
        virtual bool reloadClProg(const bool force);
		virtual void reload(ofxOscMessage const &msg);
		virtual void update();
        virtual void draw(int li);

    protected:
		virtual size_t getKernelWidth();
		virtual size_t getKernelHeight();
        virtual bool isSaveForKernel();

        msa::OpenCLBuffer clCountersR;
        msa::OpenCLBuffer clCountersG;
        msa::OpenCLBuffer clCountersB;
        msa::OpenCLImage  clImageInput;
        msa::OpenCLBuffer clGaussian;
        msa::OpenCLBuffer clUniform;

        msa::OpenCLKernelPtr set_zero_kernel;
        msa::OpenCLKernelPtr map_r2_kernel;
        msa::OpenCLKernelPtr write_image_kernel;

        bool hasImageInput = false;

        ofFbo clOutFbo;
        ScalarLisp slLoops;
        ScalarLisp slP0;
        ScalarLisp slP1;
        ScalarLisp slP2;
        ScalarLisp slP3;
        ScalarLisp slP4;
        ScalarLisp slP5;
        ScalarLisp slP6;
        ScalarLisp slP7;

    private:
        FloatArrays floats;
};
#endif // WITH_openCL
