#include "../ana_features.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "MSAOpenCL.h"

#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"
#include "ClFilter.h"
#include "cl/ClUpstreamPoints.h"

const size_t n_max = 1024; // TODO

ClUpstreamPoints::ClUpstreamPoints(ofGLWindowSettings settings, OscListener * oscParams) :
                ClFilter(settings, oscParams) {
}

ClUpstreamPoints::~ClUpstreamPoints() {
}

void ClUpstreamPoints::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    clOutImage.initFromTexture(fbo.getTexture());
    fv_buffer.initBuffer(sizeof(float) * vertices_patch * n_max, CL_MEM_READ_ONLY, NULL);
}

void ClUpstreamPoints::runKernel(msa::OpenCLKernelPtr kernel, int vsize) {
    if(fatalClProg) {
        return;
    }
    kernel->setArg(0, vsize);
    kernel->setArg(1, fv_buffer);
    if(clOutImage) {
        kernel->setArg(2, clOutImage);
    }
    kernel->run2D(filterWidth, filterHeight);
}

void ClUpstreamPoints::update(){
    if(clProgName.empty())
        return;

    if(reloadClProg(false)) {
        runKernel(setup_kernel, 0);
    }

    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f && f->vertices_size > 0) {
                size_t vsize = std::min(n_max, f->vertices_size);
                fv_buffer.write(f->vertices, 0, sizeof(float) * vsize * f->vertices_patch);
                runKernel(update_kernel, (int)vsize);
            }
        }
    }
}
#endif // WITH_openCL
