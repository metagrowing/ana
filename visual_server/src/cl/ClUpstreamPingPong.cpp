#include "../ana_features.h"
#ifdef WITH_openCL

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "globalvars.h"
#include "ofApp.h"
#include "OscListener.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "ClUpstreamPingPong.h"

ClUpstreamPingPong::ClUpstreamPingPong(ofGLWindowSettings settings, OscListener * oscParams) :
ClPingPong(settings, oscParams) {
}

ClUpstreamPingPong::~ClUpstreamPingPong() {
}

void ClUpstreamPingPong::setup(const std::string id, int numSamples) {
    ClPingPong::setup(id, numSamples);

    hasUpstreamImage = false;
    clFallbackImage.initWithTexture(filterWidth, filterHeight, GL_RGBA);
    unsigned char *pixels = new unsigned char[filterWidth * filterHeight * sizeof(unsigned char)];
    clFallbackImage.write(pixels);
    delete [] pixels;
}

void ClUpstreamPingPong::reload(ofxOscMessage const &msg) {
    hasUpstreamImage = false;
    ClPingPong::reload(msg);

    ScalarLispMng mng;
    mng.compile(slLoops, "loops", "1", msg);
    mng.compile(slP0, "p0", "1.0", msg);
    mng.compile(slP1, "p1", "1.0", msg);
    mng.compile(slP2, "p2", "1.0", msg);
    mng.compile(slP3, "p3", "1.0", msg);
    mng.compile(slP4, "p4", "1.0", msg);
    mng.compile(slP5, "p5", "1.0", msg);
    mng.compile(slP6, "p6", "1.0", msg);
    mng.compile(slP7, "p7", "1.0", msg);

    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                clInImage.initFromTexture(f->getTexture());
                hasUpstreamImage = true;
            }
        }
    }

    pingPongIndex = 0;
    const size_t buflen = getKernelWidth() * getKernelHeight();
    for(auto bx = 0u; bx < half_as_much_buffers; ++bx) {
        float * chemical = floats.uniform(0.0f, 1.0f, buflen);
        clBuffers[2 * bx].write(chemical, 0, buflen * sizeof(float));
        // second CL buffer clBuffers[2 * bx + 1], content needs not to be initialized
        delete[] chemical;
    }

    runKernel(setup_kernel);
}

void ClUpstreamPingPong::runKernel(msa::OpenCLKernelPtr kernel) {
    if(!fatalClProg) {
        for (auto bx = 0u; bx < half_as_much_buffers; ++bx) {
            kernel->setArg(2 * bx, clBuffers[2 * bx + pingPongIndex]);
            kernel->setArg(2 * bx + 1, clBuffers[2 * bx + (1 - pingPongIndex)]);
        }
        kernel->setArg(2 * half_as_much_buffers,     (float)slP0.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 1, (float)slP1.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 2, (float)slP2.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 3, (float)slP3.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 4, (float)slP4.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 5, (float)slP5.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 6, (float)slP6.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 7, (float)slP7.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 8, hasUpstreamImage ? clInImage : clFallbackImage);
        kernel->setArg(2 * half_as_much_buffers + 9, clOutImage);
        kernel->run2D(getKernelWidth(), getKernelHeight());
        pingPongIndex = 1 - pingPongIndex;
    }
}

//void ClUpstreamPingPong::update() {
//    if(clProgName.empty())
//        return;
//
//    if(reloadClProg(false)) {
//        runKernel(setup_kernel);
//    }
//    size_t loops = std::max(1.0, slLoops.rpn.execute0());
//    for(size_t li=0; li<loops; ++li) {
//        runKernel(update_kernel);
//    }
//}
#endif // WITH_openCL
