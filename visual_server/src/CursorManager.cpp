#include <thread>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include "CursorManager.h"
#include <util/stringhelper.h>

#include "Configuration.h"

CursorManager::~CursorManager() {
}

void CursorManager::setup() {
    hideCursor = asBool(HIDE_CURSOR);
    if(hideCursor) {
        ofHideCursor();
        isCursorVisible = false;
    } else {
        show();
    }
}

void CursorManager::nextFrame() {
    if(hideCursor) {
        if(hideCursorCounter == 1) {
            ofHideCursor();
            isCursorVisible = false;
            hideCursorCounter = 0;
        } else {
            if(hideCursorCounter > 0) {
                --hideCursorCounter;
            }
        }
    }
}

void CursorManager::moved() {
    if(!isCursorVisible) {
        ofShowCursor();
        isCursorVisible = true;
    }
    hideCursorCounter = 60;
}

void CursorManager::show() {
    ofShowCursor();
    isCursorVisible = true;
}
