#pragma once
#include <string>

#include "../ana_features.h"
#include "ofMain.h"
#include "ofxOsc.h"
#include "OscListener.h"
#ifdef WITH_openCL
#include "MSAOpenCL.h"
#endif // WITH_openCL

typedef float *FilterVertices;

class Filter {
public:
    Filter(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~Filter();
    virtual void setup(const std::string id, int numSamples);
    virtual void setInputTexture(std::string cmd);
    virtual std::string* getDefaultFrag();
    virtual ofTexture& getTexture();
    virtual ofFbo& getFbo();
#ifdef WITH_openCL
    virtual msa::OpenCLImage& getAnalysisImage();
#endif // WITH_openCL
    virtual void reload(ofxOscMessage const &msg) = 0;
    virtual void compileShader(std::string msg_id, const std::string src);
    virtual void update();
    virtual void draw(int li);
    virtual void finish();

    virtual const std::string& differentiation() const;

    virtual void exportCsv(int li, std::ofstream &ostrm);

    const std::string& getId() const {
        return id;
    }

    virtual void toMermaid(std::stringstream & info);
    virtual std::string dumpState();

    FilterVertices vertices = nullptr;
    const size_t vertices_patch = 8;
    size_t vertices_size = 0;
    size_t vertices_capacity = 0;
    inline float * FVx(const size_t at) { return &vertices[at*vertices_patch]; }
    inline float * FVy(const size_t at) { return &vertices[at*vertices_patch+1]; }
    inline float * FVz(const size_t at) { return &vertices[at*vertices_patch+2]; }
    inline float * FVw(const size_t at) { return &vertices[at*vertices_patch+3]; }

    inline float * FVh(const size_t at) { return &vertices[at*vertices_patch+4]; }
    inline float * FVs(const size_t at) { return &vertices[at*vertices_patch+5]; }
    inline float * FVv(const size_t at) { return &vertices[at*vertices_patch+6]; }
    inline float * FVa(const size_t at) { return &vertices[at*vertices_patch+7]; }

    void reallocateVertices(size_t capacity);

    inline void clearVertices() {
        if(vertices != nullptr) {
            vertices_capacity = 0;
            vertices_size = 0;
            delete[] vertices;
            vertices = nullptr;
        }
    }

protected:
    std::string id;

    OscListener * oscParams;

    ofEasyCam *cam = nullptr;

    std::vector<std::string> upstream_ids;

    ofShader shader;
    ofFbo fbo;
#ifdef WITH_openCL
    msa::OpenCLImage clAnalysisImage;
#endif // WITH_openCL
};

extern void sendUniforms(const ofShader & shader, int li);
