#pragma once

#include <stddef.h>
#include "ofMain.h"

class Palette {
public:
    Palette();
    virtual ~Palette();
    inline size_t size() {
        return psize;
    }

    inline ofFloatColor at(size_t ix) {
        return pal[(ix < 0 ? -ix : ix) % psize];
    }

    inline float r(size_t ix) {
        return pal[(ix < 0 ? -ix : ix) % psize].r;
    }

    inline float g(size_t ix) {
        return pal[(ix < 0 ? -ix : ix) % psize].g;
    }

    inline float b(size_t ix) {
        return pal[(ix < 0 ? -ix : ix) % psize].b;
    }

    inline float a(size_t ix) {
        return pal[(ix < 0 ? -ix : ix) % psize].a;
    }

    void setDefault();
    void setDefaultColors();
    void setDefaultTransparents();
private:
    size_t psize = 0;
    ofFloatColor * pal = nullptr;
};
