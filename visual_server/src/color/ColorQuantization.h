#pragma once
#include <stdio.h>
#include <string>
#include <vector>

#include "ofMain.h"

// see: https://rosettacode.org/wiki/Color_quantization/C
typedef struct {
	int w, h;
	unsigned char *pix;
} image_t;
 
extern image_t * read_ppm(std::string fn);
extern int write_ppm(image_t * im, std::string fn);
extern void color_quant_image(image_t * im, int n_colors, int dither);
extern void color_quant_palette(image_t * im, int n_colors, std::vector<ofColor>& pal);
