#pragma once
#include <string>
#include <vector>

#include "ana_features.h"

#ifdef WITH_memoryusage
extern void os_memusage(std::string &vmX);
#endif // WITH_memoryusage

#ifdef WITH_deteced_ipv4
extern std::string os_list_ipv4_addr();
#endif // WITH_deteced_ipv4
