#pragma once
#include <string>
#include <vector>

extern bool file_exists(const std::string &name);
extern std::vector<std::string> file_names(const std::string pattern, bool verbose = true);
extern bool makeDirectory(std::string epath);
extern std::vector<std::filesystem::path> buildFileList(std::string descr);
