#include <string>
#include <vector>
#include <sstream>

#include "ofMain.h"

#include "ana_features.h"
#include "util/stringhelper.h"

void split(std::string const &blob, const char separator, std::vector<std::string> &parts) {
    std::stringstream b(blob);
    std::string p;
    while(std::getline(b, p, separator))
        parts.push_back(p);
}

std::string trim(std::string const &str, const char start, const char end) {
    if(str.empty())
        return str;

    std::size_t headPos = str.find_first_not_of(start);
    std::size_t first = headPos == std::string::npos ? str.length() : headPos;
    std::size_t last = str.find_last_not_of(end);
    return str.substr(first, last - first + 1);
}

std::string trim(std::string const &str, std::string const &start, std::string const & end) {
    if(str.empty())
        return str;

    std::string s;
    std::size_t headPos = str.find(start);
    if(headPos != std::string::npos) {
        s = str.substr(start.length());
    }
    else {
        s = str;
    }
    std::size_t tailPos = s.rfind(end);
    if(tailPos != std::string::npos) {
        return s.substr(0, tailPos);
    }
    return s;
}

void findAndReplaceAll(std::string &data, std::string toSearch, std::string replaceStr) {
    size_t pos = data.find(toSearch);
    while(pos != std::string::npos) {
        data.replace(pos, toSearch.size(), replaceStr);
        pos = data.find(toSearch, pos + replaceStr.size());
    }
}

bool hasEnding(std::string const &fullString, std::string const &ending) {
    if(fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

#ifdef WITH_testcases
void stringhelper_equals(std::string expected, std::string calculated) {
    bool eq = expected == calculated;
    if(!eq)
        ofLog(OF_LOG_ERROR) << "'" << expected << "' != '" << calculated << "'";
}

void stringhelper_runTestCases() {
    stringhelper_equals("", trim(""));
    stringhelper_equals("", trim("  "));
    stringhelper_equals(".", trim(" . "));
    stringhelper_equals(".", trim("  .  "));

    stringhelper_equals("", trim("[]", '[', ']'));
    stringhelper_equals(" ", trim("[ ]", '[', ']'));
    stringhelper_equals("a b", trim(trim("[ a b ]", '[', ']')));

    stringhelper_equals("", trim("", "", ""));
    stringhelper_equals("cd", trim("(ab cd)", "(ab ", ")"));
    stringhelper_equals("(ab ", trim("(ab cd)", "", "cd)"));
    stringhelper_equals(">", trim("->>", "->", ""));
    stringhelper_equals("(zero? x (zero? y 1 0) 0)", trim("(v (zero? x (zero? y 1 0) 0))", "(v ", ")"));
}
#endif // WITH_testcases
