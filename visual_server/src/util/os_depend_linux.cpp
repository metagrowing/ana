#include <string>
#include <array>
#include <memory>
#include <sstream>

#include "ofMain.h"

#include "ana_features.h"
#include "util/stringhelper.h"
#include "util/os_depend.h"

#ifdef WITH_memoryusage
void os_memusage(std::string &vmX) {
    FILE *file = fopen("/proc/self/status", "r");
    char line[128];

    vmX.clear();
    while(fgets(line, 128, file) != NULL) {
        std::string l = line;
        if(l.find("Vm") == 0)
            vmX += l;
    }
    fclose(file);
}
#endif // WITH_memoryusage

#ifdef WITH_deteced_ipv4
std::string os_list_ipv4_addr() {
    std::array<char, 512> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen("hostname -I", "r"), pclose);
    if (!pipe) {
        ofLog(OF_LOG_ERROR) << "'hostname -I' failed.";
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    if(result.find(' ', 0) != result.npos)
        result = result.substr(0, result.find(' ', 0));
    return trim(result, "", "\n");
}
#endif // WITH_deteced_ipv4
