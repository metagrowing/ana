#include "ana_features.h"
#ifdef WITH_js_dev_input
#include "input/Joystick_dev_input_js.h"

JoystickDevice::JoystickDevice() {
    reset();
}

JoystickDevice::~JoystickDevice() {
    clear();
    reset();
}

void JoystickDevice::reset() {
    fd = -1;
    axis_count = button_count = 0;
    axes = nullptr;
    button_state = nullptr;
    button_last_rising = nullptr;
    version = 0x000800;
    name = "";
}

void JoystickDevice::clear() {
    if(fd >= 0) {
        ::close(fd);
        ofLog(OF_LOG_NOTICE) << "JoystickDevice: closing device";
    }
    axis_count = 0;
    if(axes != nullptr) {
        delete[] axes;
        axes = nullptr;
    }
    button_count = 0;
    if(button_state != nullptr) {
        delete[] button_state;
        button_state = nullptr;
    }
    if(button_last_rising != nullptr) {
        delete[] button_last_rising;
        button_last_rising = nullptr;
    }
}

void JoystickDevice::setup(std::string device) {
    clear();
    reset();
    if(device.empty())
        return;

    if((fd = open(device.c_str(), O_RDONLY)) < 0) {
        ofLog(OF_LOG_WARNING) << "JoystickDevice: can not open " << device.c_str();
    } else {
#define JOYSTICK_NAME_LENGTH 128
        char gname[JOYSTICK_NAME_LENGTH + 1];
        memset(gname, 0, (JOYSTICK_NAME_LENGTH + 1) * sizeof(char));

        ioctl(fd, JSIOCGVERSION, &version);
        ioctl(fd, JSIOCGAXES, &axis_count);
        ioctl(fd, JSIOCGBUTTONS, &button_count);
        ioctl(fd, JSIOCGNAME(JOYSTICK_NAME_LENGTH), gname);
        name = gname;

        getaxmap(fd, axmap);
        getbtnmap(fd, btnmap);

        axes = new float[axis_count];
        memset(axes, 0, axis_count * sizeof(float));
        button_state = new unsigned char[button_count];
        memset(button_state, 0, button_count * sizeof(unsigned char));
        button_last_rising = new unsigned char[button_count];
        memset(button_last_rising, 0, button_count * sizeof(unsigned char));

        fcntl(fd, F_SETFL, O_NONBLOCK);
        ofLog(OF_LOG_NOTICE) << "found joystick device '" << name << "' at " << device << " axes " << axis_count << " buttons " << button_count;
    }
}

std::string JoystickDevice::getName() {
    return name;
}

uint32_t JoystickDevice::countAxes() {
    return axis_count;
}

uint32_t JoystickDevice::countButtons() {
    return button_count;
}

void JoystickDevice::update() {
    if(fd < 0) {
        return;
    }

    while(read(fd, &js, sizeof(struct js_event)) == sizeof(struct js_event)) {
        switch(js.type & ~JS_EVENT_INIT) {
        case JS_EVENT_BUTTON:
            if(js.number < button_count) {
                button_last_rising[js.number] = button_state[js.number];
                button_state[js.number] = js.value;
            }
            break;

        case JS_EVENT_AXIS:
            if(js.number < axis_count)
                axes[js.number] = (float) js.value / 32768.0f;
            break;
        }
    }
}

bool JoystickDevice::isButtonPressed(uint32_t n) {
    return n < button_count && button_state[n];
}

bool JoystickDevice::isRisingEdge(uint32_t n) {
    if(n >= button_count)
        return false;

    if(!button_last_rising[n] && button_state[n]) {
        button_last_rising[n] = true;
        return true;
    }
    return false;
}

float JoystickDevice::readAxis(uint32_t n) {
    return n < axis_count ? axes[n] : 0.0f;
}

void JoystickDevice::logAxes() {
    for(auto ax = 0u; ax < axis_count; ++ax)
        ofLog(OF_LOG_NOTICE) << "JoystickDevice: axis   " << ax << " = " << readAxis(ax);
}

void JoystickDevice::logButtons() {
    for(auto bx = 0u; bx < button_count; ++bx)
        ofLog(OF_LOG_NOTICE) << "JoystickDevice: button " << bx << " = " << isButtonPressed(bx);
}

void JoystickDevice::logPressedButtons() {
    for(auto bx = 0u; bx < button_count; ++bx)
        if( isButtonPressed(bx))
            ofLog(OF_LOG_NOTICE) << "JoystickDevice: button " << bx << " = " << isButtonPressed(bx);
}
#endif // WITH_js_dev_input
