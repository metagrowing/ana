#include "ofMain.h"

#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "fv/FvRemapPoints.h"

const size_t n_max = 1024; // TODO

FvRemapPoints::FvRemapPoints(ofGLWindowSettings settings, OscListener * oscParams) :
                Filter(settings, oscParams) {
}

FvRemapPoints::~FvRemapPoints() {
}

void FvRemapPoints::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void FvRemapPoints::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slX, "x", "0", msg);
    mng.compile(slY, "y", "0", msg);
    mng.compile(slRotate, "rot", "0", msg);
    mng.compile(slSize, "size", "0", msg);
    mng.compile(slH, "h", "0", msg);
    mng.compile(slS, "s", "0", msg);
    mng.compile(slV, "v", "1", msg);
    mng.compile(slResolution, "res", "22", msg);
    mng.compile(slFill, "fill", "-1", msg);
    mng.compile(slClear, "clear", "1", msg);
}

void FvRemapPoints::update(){
    upf = nullptr;
    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                upf = f;
            }
        }
    }
}

void FvRemapPoints::draw(int li) {
    fbo.begin();
    float clear = slClear.rpn.execute0();
    if(clear >= 0.0) {
        glClearColor(clear, clear, clear, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    if(upf) {
        if(upf->vertices_size > vertices_capacity) {
            clearVertices();
            reallocateVertices(upf->vertices_size);
        }
        ofPushStyle();
        for(float i = 0; i < upf->vertices_size; ++i) {
            float in_x = *upf->FVx(i);
            float in_y = *upf->FVy(i);
            float in_z = *upf->FVz(i);
            float in_w = *upf->FVw(i);
            float x = slX.rpn.execute3d1i(in_x, in_y, in_z, i);
            *FVx(i) = x;
            float y = slY.rpn.execute3d1i(in_x, in_y, in_z, i);
            *FVy(i) = y;
            float z = slSize.rpn.execute3d1i(in_x, in_y, in_z, i);;
            *FVz(i) = z;
            float rot = slRotate.rpn.execute3d1i(in_x, in_y, in_w, i); // TODO add w, 4 float and 1 int  parameters
            *FVw(i) = rot;

            float h = slH.rpn.execute3d1i(in_x, in_y, in_z, i);
            *FVh(i) = h;
            float s = slS.rpn.execute3d1i(in_x, in_y, in_z, i);
            *FVs(i) = s;
            float v = slV.rpn.execute3d1i(in_x, in_y, in_z, i);
            *FVv(i) = v;
            ofFloatColor rgb = ofFloatColor::fromHsb(h, s, v);
            ofSetColor(rgb);

            if(slFill.rpn.execute3d1i(in_x, in_y, in_z, i) >= 0.0)
                ofFill();
            else
                ofNoFill();

            int resolution = std::round(std::max(0.0, slResolution.rpn.execute3d1i(in_x, in_y, in_z, i)));
            ofSetCircleResolution(resolution);

            ofPushMatrix();
            ofTranslate(x, y, 0);
            ofRotateDeg(rot);
            ofDrawCircle(0, 0, z);
            ofPopMatrix();
        }
        vertices_size = upf->vertices_size;
        ofPopStyle();
    }

    fbo.end();
}
