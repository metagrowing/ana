#pragma once
#include <unordered_map>
#include <string>

#include "ofxOsc.h"

class OscListener {
public:
    OscListener();
    virtual ~OscListener();
    virtual void setup();
    void update();
    std::string msg_toString(ofxOscMessage &msg);

private:
    void reloadByID(std::string const &id, ofxOscMessage const &msg);
    void processCommand(const std::string &path, const ofxOscMessage &msg);

    ofxOscReceiver receiver;
    bool dump_osc_input = false;
    bool dump_frag_src = false;
};
