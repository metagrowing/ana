#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class ShapeWords : public Filter {
	public:
		ShapeWords(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ShapeWords();
        virtual void setup(const std::string id, int numSamples);
        virtual void reload(ofxOscMessage const &msg);
        virtual void draw(int li);

	private:
		ofTrueTypeFont	digit_font;
	    ScalarLisp slClear;
        ScalarLisp slSize;
        float xp = 0;
        float yp = 0;
        std::string words = "%f";
};
