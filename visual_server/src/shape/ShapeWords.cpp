#include "ofMain.h"

#include "globalvars.h"
#include "calc/ScalarLispMng.h"
#include "shape/ShapeWords.h"

ShapeWords::ShapeWords(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

ShapeWords::~ShapeWords(){
}

void ShapeWords::setup(const std::string id, int numSamples){
    Filter::setup(id, numSamples);
}


void ShapeWords::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg >= 2) {
        words = msg.getArgAsString(2);
    }

    ScalarLispMng mng;
    mng.compile(slSize,  "size", "80", msg);
    mng.compile(slClear, "clear", "0", msg);

    int font_size = slSize.rpn.execute0();
    bool loaded = digit_font.load("NotoSansMono-CondensedSemiBold.ttf", font_size, true, true);
    if(!loaded) {
        loaded = digit_font.load("verdana.ttf", font_size, true, true);
    }
    if(!loaded)
        ofLog(OF_LOG_ERROR) << "can't load font '" << "verdana.ttf" << "'";
    else {
    	ofRectangle bb = digit_font.getStringBoundingBox(words, 0, 0);
    	ofRectangle bbh = digit_font.getStringBoundingBox("M", 0, 0);
    	xp = bb.width > filterWidth ? 0 : (filterWidth - bb.width) / 2;
    	yp = font_size + bbh.height > filterHeight ? 0 : (filterHeight - bb.height) / 2;
    }
}

void ShapeWords::draw(int li){
	fbo.begin();
    float clear = slClear.rpn.execute0();
    if(clear >= 0.0) {
        glClearColor(clear, clear, clear, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    ofSetColor(ofColor::white);
    digit_font.drawString(words, xp, yp);

	fbo.end();
}
