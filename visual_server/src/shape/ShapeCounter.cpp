#include <ShapeCounter.h>
#include "ofMain.h"

#include "globalvars.h"
#include "calc/ScalarLispMng.h"

//static const int border = 20;
//static const int font_size = 80;

ShapeCounter::ShapeCounter(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

ShapeCounter::~ShapeCounter(){
}

void ShapeCounter::setup(const std::string id, int numSamples){
    Filter::setup(id, numSamples);
}


void ShapeCounter::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slSize,  "size", "80", msg);
    mng.compile(slClear, "clear", "0", msg);
    int font_size = slSize.rpn.execute0();
    bool loaded = digit_font.load("verdana.ttf", font_size, true, true);
    if(loaded) {
//  digit_font.setLineHeight(34.0f);
//  digit_font.setLetterSpacing(1.035);
    }
    else
        ofLog(OF_LOG_ERROR) << "can't load font '" << "verdana.ttf" << "'";
}

void ShapeCounter::update(){
    upf = nullptr;
    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                upf = f;
            }
        }
    }
}

void ShapeCounter::draw(int li){
	fbo.begin();
    float clear = slClear.rpn.execute0();
    if(clear >= 0.0) {
        glClearColor(clear, clear, clear, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    if(upf) {
//        ofColor c = ofColor(*upf->FVh(i), *upf->FVs(i), *upf->FVv(i));
//        ofSetColor(c);
        ofPushMatrix();
        ofTranslate(0.5*filterWidth, 0.5*filterHeight);
        for(size_t i = 0; i < upf->vertices_size; ++i) {
            ofFloatColor c = ofFloatColor(*upf->FVh(i), *upf->FVs(i), *upf->FVv(i));
            ofSetColor(c);
            digit_font.drawString(to_string(i), *upf->FVx(i), *upf->FVy(i));
        }
        ofPopMatrix();
    }

//    int distance = (filterWidth - 2 * border - font_size) / (num_counters-1);
//	for(int i = 0; i < num_counters; ++i){
//		digit_font.drawString(to_string(i), i * distance + border, 0.5 * filterHeight);
//	}

	fbo.end();
}
