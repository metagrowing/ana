#include "ofMain.h"

#include "stringhelper.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "gl/GlFrameDiff.h"

GlFrameDiff::GlFrameDiff(ofGLWindowSettings settings, OscListener *oscParams) :
        Filter(settings, oscParams) {
}

GlFrameDiff::~GlFrameDiff() {
    if(p0) {
        p0->clear();
        delete p0;
    }
    if(p1) {
        p1->clear();
        delete p1;
    }
}

static std::string builtinVert = STRINGIFY(#version 330\n
uniform mat4 modelViewProjectionMatrix;\n
in vec4 position;\n
void main()\n
{\n
    gl_Position = modelViewProjectionMatrix * position;\n
}\n
);

static std::string builtinFrag = STRINGIFY(#version 330\n
uniform sampler2DRect tex0;\n
uniform sampler2DRect tex1;\n
out vec4 outputColor;\n
void main()\n
{\n
    outputColor = abs(texture(tex0, gl_FragCoord.xy) - texture(tex1, gl_FragCoord.xy));\n
    outputColor.a = 1.0;\n
}\n
);

void GlFrameDiff::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    p0 = new ofFbo();
    p0->allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, numSamples);
    p0->begin();
    ofClear(0, 0, 0, 0);
    p0->end();

    p1 = new ofFbo();
    p1->allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, numSamples);
    p1->begin();
    ofClear(0, 0, 0, 0);
    p1->end();

    ofLog(OF_LOG_NOTICE) << "compile build in shaders " << id;
    shader.setupShaderFromSource(GL_VERTEX_SHADER, builtinVert);
    shader.setupShaderFromSource(GL_FRAGMENT_SHADER, builtinFrag);
    shader.linkProgram();
}

void GlFrameDiff::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    toggle = false;
}

void GlFrameDiff::draw(int li) {
    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                if(toggle) {
                    p0->begin();
                    f->getTexture().draw(0, 0);
                    p0->end();
                } else {
                    p1->begin();
                    f->getTexture().draw(0, 0);
                    p1->end();
                }
            }
        }
    }

    fbo.begin();
    shader.begin();
    sendUniforms(shader, li);
    if(toggle) {
        shader.setUniformTexture("tex0", p0->getTexture(), 0);
        shader.setUniformTexture("tex1", p1->getTexture(), 1);
    } else {
        shader.setUniformTexture("tex0", p1->getTexture(), 0);
        shader.setUniformTexture("tex1", p0->getTexture(), 1);
    }

    ofDrawRectangle(0, 0, filterWidth, filterHeight);

    shader.end();
    fbo.end();
}

void GlFrameDiff::finish() {
    toggle = !toggle;
}
