#include <math.h>

#include "ofMain.h"
#include "stringhelper.h"
#include "Configuration.h"
#include "globalvars.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "gl/GlBillboard.h"

// A billboard example from openFrameworks
// see: https://github.com/openframeworks/openFrameworks/tree/patch-release/examples/gl/billboardExample

GlBillboard::GlBillboard(ofGLWindowSettings settings, OscListener *oscParams) : Filter(settings, oscParams) {
}

GlBillboard::~GlBillboard() {
    billboardShader.unload();
}

static std::string builtinVert = STRINGIFY(#version 150\n
uniform mat4 orientationMatrix;\n
uniform mat4 projectionMatrix;\n
uniform mat4 modelViewMatrix;\n
uniform mat4 textureMatrix;\n
uniform mat4 modelViewProjectionMatrix;\n
\n
in vec4  position;\n
in vec4  color;\n
in vec3  normal;\n
\n
out vec4 colorVarying;\n
\n
void main() {\n
    vec4 eyeCoord = modelViewMatrix * position;\n
    gl_Position = projectionMatrix * eyeCoord; /* to be accurate this should premultiply by orientationMatrix */\n
    float dist = sqrt(eyeCoord.x*eyeCoord.x + eyeCoord.y*eyeCoord.y + eyeCoord.z*eyeCoord.z);\n
    float att    = 600.0 / dist;\n
    gl_PointSize = normal.x * att;\n
    colorVarying = color;\n
}\n
);

static std::string builtinFrag = STRINGIFY(#version 150\n
uniform sampler2D tex;\n
in vec4 colorVarying;\n
out vec4 fragColor;\n
void main (void) {\n
    fragColor = texture(tex, gl_PointCoord) * colorVarying;\n
}\n
);

void GlBillboard::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    ofBackground(0, 0, 0);

    cameraRotation = { 0, 0, 0 };

    billboards.setUsage(GL_DYNAMIC_DRAW);
    billboards.setMode(OF_PRIMITIVE_POINTS);

    // load the billboard shader
    // this is used to change the
    // size of the particle
    ofLog(OF_LOG_NOTICE) << "compile build in shaders " << id;
    billboardShader.setupShaderFromSource(GL_VERTEX_SHADER, builtinVert);
    billboardShader.setupShaderFromSource(GL_FRAGMENT_SHADER, builtinFrag);
    billboardShader.linkProgram();

    // we need to disable ARB textures in order to use normalized texcoords
    ofDisableArbTex();
    texture_loaded = textureImg.load(asString(MEDIA_PATH) + "/" + alpha_billboard);  //TODO
    if(!texture_loaded) {
        ofLog(OF_LOG_ERROR) << "can not load " << alpha_billboard;
    }
    ofEnableArbTex();
}

void GlBillboard::fill(size_t num) {
    size_t test_num = std::min(num, MAX_BILLBOARDS);
    if(num_billboards == test_num)
        return;

    num_billboards = test_num;
    billboards.getVertices().resize(num_billboards);
    billboards.getColors().resize(num_billboards);
    billboards.getNormals().resize(num_billboards, glm::vec3(0));

    // ------------------------- billboard particles
    for(int i = 0; i < num_billboards; i++) {
        billboards.getVertices()[i] = { 0, 0, 0 };

        switch(i % 3) {
        case 1:
            billboards.getColors()[i].set(ofColor::fromHsb(ofRandom(27, 33), ofRandom(250, 255), ofRandom(250, 255)));
            break;
        case 2:
            billboards.getColors()[i].set(ofColor::fromHsb(ofRandom(129, 136), ofRandom(250, 255), ofRandom(117, 120)));
            break;
        default:
            billboards.getColors()[i].set(ofColor::fromHsb(ofRandom(139, 145), ofRandom(250, 255), ofRandom(67, 73)));

        }

    }

    if(num_billboards > vertices_capacity) {
        clearVertices();
        reallocateVertices(num_billboards);
    }
}

void GlBillboard::reload(ofxOscMessage const &msg) {
    ScalarLispMng mng;
    auto narg = msg.getNumArgs();
    if(narg > 1)
        Filter::setInputTexture(msg.getArgAsString(1));
    mng.compile(slCount, "count", "0", msg);
    mng.compile(slX, "x", "0", msg);
    mng.compile(slY, "y", "0", msg);
    mng.compile(slZ, "z", "0", msg);
    mng.compile(slSize, "size", "10", msg);
    mng.compile(slRotX, "rotx", "0", msg);
    mng.compile(slRotY, "roty", "0", msg);
    mng.compile(slRotZ, "rotz", "0", msg);
    mng.compile(slClear, "clear", "1", msg);
}

void GlBillboard::update() {
    fill(std::round(std::max(0.0, slCount.rpn.execute0())));
    cameraRotation.x = slRotX.rpn.execute0();
    cameraRotation.y = slRotY.rpn.execute0();
    cameraRotation.z = slRotZ.rpn.execute0();
    for(size_t i = 0; i < num_billboards; i++) {
        float vx = slX.rpn.execute3d1i(0.0, 0.0, 0.0, i);
        float vy = slY.rpn.execute3d1i(vx,  0.0, 0.0, i);
        float vz = slZ.rpn.execute3d1i(vx,  vy,  0.0, i);
        *FVx(i) = vx;
        *FVy(i) = vy;
        *FVz(i) = vz;
        auto c = billboards.getColors()[i];
        *FVh(i) = c.r;
        *FVs(i) = c.g;
        *FVv(i) = c.b;
        glm::vec3 position(vx, vy, vz);
        billboards.getVertices()[i] = position;
        billboards.setNormal(i, glm::vec3(slSize.rpn.execute3d1i(position.x, position.y, position.z, i), 0, 0));
    }
    vertices_size = num_billboards;
}

void GlBillboard::draw(int li) {
    if(!texture_loaded)
        return;

    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0)
        ofClear(0, 0, 0, 255);

    ofEnableAlphaBlending();
    ofSetColor(255);

    ofPushMatrix();
    ofTranslate(filterWidth / 2, filterHeight / 2, 0);
    ofRotateDeg(cameraRotation.x, 1, 0, 0);
    ofRotateDeg(cameraRotation.y, 0, 1, 0);
    ofRotateDeg(cameraRotation.z, 0, 0, 1);

    // bind the shader so that we can change the
    // size of the points via the vertex shader
    billboardShader.begin();

    ofEnablePointSprites();
    textureImg.getTexture().bind();
    billboards.draw();
    textureImg.getTexture().unbind();
    ofDisablePointSprites();

    billboardShader.end();

    ofDisableAlphaBlending();
    ofPopMatrix();

    fbo.end();
}

void GlBillboard::exportCsv(int li, std::ofstream &ostrm) {
    for(int bi = 0; bi < num_billboards; bi++) {
        auto pos = billboards.getVertices()[bi];
        auto x = pos.x;
        auto y = pos.y;
        auto z = pos.z;
        auto scale = billboards.getNormal(bi).x;
        auto rgb = billboards.getColors()[bi];
        ostrm << frame << ',' // current frame
                << li << ','    // processing node indices
                << bi << ','    // object indices
                << x << ','     // x position
                << y << ','     // y position
                << z << ','     // z position
                << scale << ',' // sx
                << scale << ',' // sy
                << scale << ',' // sz
                << std::atan2(y, x) << ',' // rotation z
//    	        << std::atan2(z, std::sqrt(x*x + y*y)) + M_PI + M_PI_2 << ',' // rotation y
                << std::atan2(z, std::sqrt(x * x + y * y)) << ',' // rotation y
                << rgb.r << ',' // red
                << rgb.g << ',' // green
                << rgb.b << ',' // blue
                << '\n';
    }
}

