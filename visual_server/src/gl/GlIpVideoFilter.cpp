#include "ofMain.h"
#include "IPVideoGrabber.h"

#include "stringhelper.h"
#include "globalvars.h"
#include "Configuration.h"
#include "OscListener.h"
#include "gl/GlIpVideoFilter.h"

GlIpVideoFilter::GlIpVideoFilter(ofGLWindowSettings settings, OscListener *oscParams) :
        Filter(settings, oscParams) {
}

GlIpVideoFilter::~GlIpVideoFilter() {
}

void GlIpVideoFilter::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void GlIpVideoFilter::setInputTexture(std::string inputs) {
    split(trim(inputs, '[', ']'), ' ', upstream_ids);
    for (std::vector<std::string>::iterator it = upstream_ids.begin() ; it != upstream_ids.end(); ++it) {
        std::string idx = *it;
        findAndReplaceAll(idx, "c", "");
        if(idx.length() > 0) {
            channel_ids.push_back(std::stoul(idx));
        }
    }
}

void GlIpVideoFilter::reload(ofxOscMessage const &msg) {
    upstream_ids.clear();
    channel_ids.clear();
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        setInputTexture(msg.getArgAsString(1));
        if(narg > 2) {
            Filter::compileShader(msg.getArgAsString(0), msg.getArgAsString(3));
        }
    }
}

void GlIpVideoFilter::draw(int li) {
    fbo.begin();
    ofClear(0, 0, 0, 255);

    shader.begin();
    sendUniforms(shader, li);

    auto tx = 0;
    for (std::vector<size_t>::iterator it = channel_ids.begin() ; it != channel_ids.end(); ++it) {
        size_t channel = *it;
        if(channel < ipVideoGrabbers.size() && ipVideoGrabbers[channel]) {
            auto texture = ipVideoGrabbers[channel]->getTexture();
            if(texture.isAllocated())
                shader.setUniformTexture("tex" + to_string(tx), texture, tx);
            ++tx;
        }
    }

    ofDrawRectangle(0, 0, filterWidth, filterHeight);

    shader.end();
    fbo.end();
}

void GlIpVideoFilter::toMermaid(std::stringstream & info) {
    auto tx = 0;
    for (std::vector<size_t>::iterator it = channel_ids.begin() ; it != channel_ids.end(); ++it) {
        size_t channel = *it;
        if(channel < ipVideoGrabbers.size() && ipVideoGrabbers[channel]) {
            info << "c" << tx << ">" << ipVideoGrabbers[channel]->getURI() << "]" << " -- c" << tx << " --> " << id << "\n"; //TODO quote [ ]
            ++tx;
        }
    }
    Filter::toMermaid(info);
}
