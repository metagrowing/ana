#pragma once

#include "Filter.h"
#include "OscListener.h"

class GlFrameDelayFilter : public Filter {
	public:
		GlFrameDelayFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlFrameDelayFilter();
        virtual void reload(ofxOscMessage const &msg);
		virtual void setup(const std::string id, int numSamples);
		virtual const std::string& differentiation() const;
		virtual void update();
		virtual void draw(int li);
	    virtual void finish();
	    virtual void toMermaid(std::stringstream & info);
	private:
        unsigned int frame_write_index = 0;
	    size_t max_delay = 1024;
	    size_t max_recorded_frames = 0;
	    size_t recorded_frames = 0;
	    GLuint frameSequ_textureID = -1;
        ofPixels pixels;
};
