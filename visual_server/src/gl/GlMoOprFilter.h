#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"

class GlMoOprFilter : public Filter{
	public:
		GlMoOprFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlMoOprFilter();
		virtual void setup(const std::string id, int numSamples);
		virtual void reload(ofxOscMessage const &msg);
		virtual void draw(int li);

	private:
		ofFbo tmpFbo0;
		ofFbo tmpFbo1;
		std::vector<ofShader *> opr_sequ;
		ofShader dilateShader;
		ofShader erodeShader;
		ofShader blurShader;
};
