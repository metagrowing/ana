#include "ofMain.h"

#include "stringhelper.h"
#include "globalvars.h"
#include "OscListener.h"
#include "calc/MsgLisp.h"
#include "Filter.h"
#include "gl/GlConvolutionFilter.h"

GlConvolutionFilter::GlConvolutionFilter(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

GlConvolutionFilter::~GlConvolutionFilter(){
}

static std::string builtinVert = STRINGIFY(#version 330\n
uniform mat4 modelViewProjectionMatrix;\n
in vec4 position;\n
void main()\n
{\n
	gl_Position = modelViewProjectionMatrix * position;\n
}\n
);

static std::string builtinFrag = STRINGIFY(#version 330\n
uniform vec3 row0;\n
uniform vec3 row1;\n
uniform vec3 row2;\n
uniform sampler2DRect tex0;\n
out vec4 outputColor;\n
void main()\n
{\n
	outputColor = vec4(0.0);\n
\n
	outputColor += row0.x * texture(tex0, gl_FragCoord.xy + vec2(-1,  1));\n
	outputColor += row0.y * texture(tex0, gl_FragCoord.xy + vec2( 0,  1));\n
	outputColor += row0.z * texture(tex0, gl_FragCoord.xy + vec2( 1,  1));\n
\n
	outputColor += row1.x * texture(tex0, gl_FragCoord.xy + vec2(-1,  0));\n
	outputColor += row1.y * texture(tex0, gl_FragCoord.xy);\n
	outputColor += row1.z * texture(tex0, gl_FragCoord.xy + vec2( 1,  0));\n
\n
	outputColor += row2.x * texture(tex0, gl_FragCoord.xy + vec2(-1, -1));\n
	outputColor += row2.y * texture(tex0, gl_FragCoord.xy + vec2( 0, -1));\n
	outputColor += row2.z * texture(tex0, gl_FragCoord.xy + vec2( 1, -1));\n
\n
	outputColor.a = 1.0;\n
}\n
);

void GlConvolutionFilter::setup(const std::string id, int numSamples) {
	Filter::setup(id, numSamples);

	convolutionKernel.push_back(0.0f); convolutionKernel.push_back(0.0f); convolutionKernel.push_back(0.0f);
	convolutionKernel.push_back(0.0f); convolutionKernel.push_back(1.0f); convolutionKernel.push_back(0.0f);
	convolutionKernel.push_back(0.0f); convolutionKernel.push_back(0.0f); convolutionKernel.push_back(0.0f);

	ofLog(OF_LOG_NOTICE) << "compile build in shader " << id;
	shader.setupShaderFromSource(GL_VERTEX_SHADER, builtinVert);
	shader.setupShaderFromSource(GL_FRAGMENT_SHADER, builtinFrag);
	shader.linkProgram();
}

void GlConvolutionFilter::reload(ofxOscMessage const & msg) {
	auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
        if(narg > 2) {
            MsgLisp l1;
            ast_m* res = l1.compile(msg.getArgAsString(2));
            bool ok = res
                    && res->count() == 2
                    && res->at(0)->tok == "matrix";
            if(ok) {
                ast_m* r1 = res->at(1);
                if(r1->tok == "[" && r1->count() == 9) {
                    for(int idx=0; idx < 9; ++idx) {
                        convolutionKernel[idx] = std::stof(r1->at(idx)->tok);
                    }
                }
                else {
                    ofLog(OF_LOG_ERROR) << "This convolution filter " << id << " needs a 3x3 kernel.";
                }
            }
        }
    }
}

void GlConvolutionFilter::draw(int li) {
	fbo.begin();
	ofClear(0, 0, 0, 255);

	shader.begin();
	sendUniforms(shader, li);
	shader.setUniform3f("row0", convolutionKernel[0], convolutionKernel[1], convolutionKernel[2]);
	shader.setUniform3f("row1", convolutionKernel[3], convolutionKernel[4], convolutionKernel[5]);
	shader.setUniform3f("row2", convolutionKernel[6], convolutionKernel[7], convolutionKernel[8]);

	if(upstream_ids.size() >= 1) {
		auto in_id = upstream_ids[0];
		if(filterDict.find(in_id) != filterDict.end()) {
			auto f = filterDict[in_id];
			if(f) {
				shader.setUniformTexture("tex0", f->getTexture(), 0);
			}
		}
	}

	ofDrawRectangle(0, 0, filterWidth, filterHeight);

	shader.end();
	fbo.end();
}
