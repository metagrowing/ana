#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class GlLoadFilter : public Filter {
	public:
		GlLoadFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlLoadFilter();
	    virtual void setup(const std::string id, int numSamples);
        virtual void reload(ofxOscMessage const &msg);
        virtual void update();
		virtual void draw(int li);
	    virtual void toMermaid(std::stringstream & info);

	    std::string img_pattern;
	    std::vector<std::string> img_list;
	    int list_size = 0;
	    ofFloatImage img;
        ScalarLisp slKf;
        int k = 0;
        bool no_input = true;
};
