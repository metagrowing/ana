#pragma once

#include "Filter.h"
#include "OscListener.h"

class GlImageFilter : public Filter {
	public:
		GlImageFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlImageFilter();
        virtual void reload(ofxOscMessage const &msg);
		virtual void draw(int li);
	    virtual void toMermaid(std::stringstream & info);
};
