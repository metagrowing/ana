#include <filesystem>
#include <algorithm>

#include "ofMain.h"
#include "globalvars.h"
#include "pathhelper.h"
#include "Filter.h"
#include <gl/GlFrameDelayFilter.h>

GlFrameDelayFilter::GlFrameDelayFilter(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

GlFrameDelayFilter::~GlFrameDelayFilter(){
    if(frameSequ_textureID > 0) {
        glDeleteTextures(1, &frameSequ_textureID);
        frameSequ_textureID = -1;
    }
}

void GlFrameDelayFilter::setup(const std::string id, int numSamples){
	Filter::setup(id, numSamples);
	usedImages = 0;
    int gi = 0;
    glGetIntegerv(GL_MAX_SHADER_STORAGE_BLOCK_SIZE, &gi);
    size_t max_shader_storage_block_size = gi;
    ofLog(OF_LOG_NOTICE) << "GL_MAX_SHADER_STORAGE_BLOCK_SIZE " << max_shader_storage_block_size;

    glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &gi);
    size_t max_array_texture_layers = gi;
    ofLog(OF_LOG_NOTICE) << "GL_MAX_ARRAY_TEXTURE_LAYERS " << max_array_texture_layers;

    //TODO 3u or 4u
    max_recorded_frames = std::min(max_array_texture_layers, std::min(max_delay, max_shader_storage_block_size / (size_t)(filterWidth * filterHeight * 4u)));
    size_t desired_mem_size = max_recorded_frames * filterWidth * filterHeight * 4u;

    if(frameSequ_textureID > 0)
        glDeleteTextures(1, &frameSequ_textureID);
    glGenTextures(1, &frameSequ_textureID);
    glBindTexture(GL_TEXTURE_2D_ARRAY, frameSequ_textureID);
    // Allocate the storage.
    ofLog(OF_LOG_NOTICE) << "glTexStorage3D " << filterWidth << "x" << filterHeight << "x" << max_recorded_frames  << " Desired GPU memory size " << desired_mem_size;
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGB8, filterWidth, filterHeight, max_recorded_frames);

    recorded_frames = 0;
}

void GlFrameDelayFilter::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
        if(narg > 2) {
            Filter::compileShader(msg.getArgAsString(0), msg.getArgAsString(3));
        }
    }
//    sframe = 0; TODO
//    recorded_frames = 0;
}

const std::string& GlFrameDelayFilter::differentiation() const {
	static std::string defs = "#version 330\n#undef TEXTURECHANNELS\n#define TEXTUREARRAY\n";
	return defs;
}

void GlFrameDelayFilter::update() {
    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                f->getTexture().readToPixels(pixels);
                size_t pw = pixels.getWidth();
                size_t ph = pixels.getHeight();
                if(pw == filterWidth && ph == filterHeight) {
                    // Upload pixel data.
                    // The first 0 refers to the mipmap level (level 0, since there's only 1)
                    // The following 2 zeroes refers to the x and y offsets in case you only want to specify a subrectangle.
                    // The frame_write_index refers to the layer index offset.
                    // Altogether you can specify a 3D box subset of the overall texture_array, but only one mip level at a time.
                    glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, frame_write_index,
                                    filterWidth, filterHeight, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixels.getData());
//                    frame_write_index = (frame_write_index + 1) % max_delay;
//                    if(recorded_frames < max_recorded_frames)
//                        recorded_frames = recorded_frames + 1;
                }
                else {
                    ofLog(OF_LOG_ERROR) << "need all images with same size. expected: " << filterWidth << "x" << filterHeight << " " << " got " << pw << "x" << ph;
                }
            }
        }
    }
}

void GlFrameDelayFilter::draw(int li){
//    ofLog(OF_LOG_NOTICE) << "sframe " << frame_write_index << ", slength " << recorded_frames << ", max_recorded_frames " << max_recorded_frames;
	fbo.begin();

	if(recorded_frames > 0) {
		shader.begin();
		sendUniforms(shader, li);
		shader.setUniformTexture("textures", GL_TEXTURE_2D_ARRAY, frameSequ_textureID, 0);
		shader.setUniform1i("sframe", frame_write_index);
		shader.setUniform1i("slength", recorded_frames);

		ofDrawRectangle(0, 0, filterWidth, filterHeight);

		shader.end();
	}

	fbo.end();
}

void GlFrameDelayFilter::finish() {
    frame_write_index = (frame_write_index + 1) % max_recorded_frames;
    if(recorded_frames < max_recorded_frames)
        recorded_frames = recorded_frames + 1;
}

void GlFrameDelayFilter::toMermaid(std::stringstream & info) {
    info << "c0>" << imagesequ0 << "]" << " -- c0 --> " << id << "\n"; //TODO quote [ ]
    Filter::toMermaid(info);
}
