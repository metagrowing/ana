__kernel void image_setup(
__read_only image2d_t image_in,
__read_only image2d_t image_src,
__write_only image2d_t image_dst,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    // setup code not used in this exsample
}

__kernel void image_change(
__read_only image2d_t image_in,
__read_only image2d_t image_src,
__write_only image2d_t image_dst,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int x = get_global_id(0);
    const int y = get_global_id(1);

    sampler_t  smp = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;

    const int2 dst = (int2)(x, y);

    // upstream color
    const int2 up  = (int2)(p1, y);
    float4 color_up = read_imagef(image_in, smp, up);
    
    float4 color_src = read_imagef(image_src, smp, dst) * (float4)(0.999);
    write_imagef(image_dst, dst, x == (int)p0 ? color_up : color_src);
}

