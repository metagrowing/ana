__kernel void image_setup(
__read_only image2d_t image_in,
__read_only image2d_t image_src,
__write_only image2d_t image_dst,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    // setup code not used in this exsample
}

__kernel void image_change(
__read_only image2d_t image_in,
__read_only image2d_t image_src,
__write_only image2d_t image_dst,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int x = get_global_id(0);
    const int y = get_global_id(1);

    sampler_t smp = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;
    const int2   center   = (int2)(x, y);
    // upstream color
    float4 color_up = read_imagef(image_in,  smp, center);
    
    float4 color_src = read_imagef(image_src, smp, center);
    write_imagef(image_dst, center, ((x-512) * (x-512) + (y-512) * (y-512)) > p0 ? color_up : color_src);
//     write_imagef(image_dst, center, color_up);
}

