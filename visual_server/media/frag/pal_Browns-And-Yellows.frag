#version 330
uniform sampler2DRect tex0;
out vec4 outputColor;
void main()
{
    vec3 color = texture(tex0, gl_FragCoord.xy).rgb;
    float gray = dot(color, vec3(0.2126, 0.7152, 0.0722));
    int i = int(gray * 22);
    int r, g, b;
    if(i < 1) {
r = 189; g = 183; b = 107; // Dark Khaki
    }
    else if(i < 2) {
r = 240; g = 230; b = 140; // Khaki
    }
    else if(i < 3) {
r = 238; g = 232; b = 170; // Pale Goldenrod
    }
    else if(i < 4) {
r = 250; g = 250; b = 210; // Light Goldenrod Yellow
    }
    else if(i < 5) {
r = 255; g = 255; b = 224; // Light Yellow
    }
    else if(i < 6) {
r = 255; g = 255; b =   0; // Yellow
    }
    else if(i < 7) {
r = 255; g = 215; b =   0; // Gold
    }
    else if(i < 8) {
r = 238; g = 221; b = 130; // Light Goldenrod
    }
    else if(i < 9) {
r = 218; g = 165; b =  32; // Goldenrod
    }
    else if(i < 10) {
r = 184; g = 134; b =  11; // Dark Goldenrod
    }
    else if(i < 11) {
r = 188; g = 143; b = 143; // Rosy Brown
    }
    else if(i < 12) {
r = 139; g =  69; b =  19; // Saddle Brown
    }
    else if(i < 13) {
r = 160; g =  82; b =  45; // Sienna
    }
    else if(i < 14) {
r = 205; g = 133; b =  63; // Peru
    }
    else if(i < 15) {
r = 222; g = 184; b = 135; // Burlywood
    }
    else if(i < 16) {
r = 245; g = 245; b = 220; // Beige
    }
    else if(i < 17) {
r = 245; g = 222; b = 179; // Wheat
    }
    else if(i < 18) {
r = 244; g = 164; b =  96; // Sandy Brown
    }
    else if(i < 19) {
r = 210; g = 180; b = 140; // Tan
    }
    else if(i < 20) {
r = 210; g = 105; b =  30; // Chocolate
    }
    else if(i < 21) {
r = 255; g = 165; b =   0; // Orange
    }
    else {
r = 255; g = 140; b =   0; // Dark Orange
    }
    
    outputColor.rgb = vec3(r, g, b) / 256.0;
    outputColor.a = 1.0;
}
