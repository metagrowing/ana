#version 330
// see: https://michaelwalczyk.com/blog-ray-marching.html
// original code from Michael Walczyk
uniform float r = 0.7;
uniform float h = 0.2;
uniform float c = 0.5;
uniform float mix = 0.9;
uniform float rx = 0.0;
uniform float ry = 0.0;
uniform float rz = 0.0;
uniform sampler2DRect tex0;
uniform ivec2 panel;

out vec4 o_color;

//
// Description : Array and textureless GLSL 2D/3D/4D simplex 
//               noise functions.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20201014 (stegu)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
// 
// https://raw.githubusercontent.com/ashima/webgl-noise/master/src/noise3D.glsl

vec3 _sdf_mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 _sdf_mod289(vec4 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 _sdf_permute(vec4 x) {
     return _sdf_mod289(((x*34.0)+10.0)*x);
}

vec4 taylorInvSqrt(vec4 r0)
{
  return 1.79284291400159 - 0.85373472095314 * r0;
}

float _sdf_snoise(vec3 v)
  { 
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

// First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

// Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //   x0 = x0 - 0.0 + 0.0 * C.xxx;
  //   x1 = x0 - i1  + 1.0 * C.xxx;
  //   x2 = x0 - i2  + 2.0 * C.xxx;
  //   x3 = x0 - 1.0 + 3.0 * C.xxx;
  vec3 x1 = x0 - i1 + C.xxx;
  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
  vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y

// Permutations
  i = _sdf_mod289(i); 
  vec4 p = _sdf_permute( _sdf_permute( _sdf_permute( 
             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
           + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

// Gradients: 7x7 points over a square, mapped onto an octahedron.
// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
  float n_ = 0.142857142857; // 1.0/7.0
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;
  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;
  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

//Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

// Mix final noise value
  vec4 m = max(0.5 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 105.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
                                dot(p2,x2), dot(p3,x3) ) );
}

mat4 _sdf_rotateX(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4( 1,  0,  0,  0),
        vec4( 0,  c, -s,  0),
        vec4( 0,  s,  c,  0),
        vec4( 0,  0,  0,  1)
    );
}

mat4 _sdf_rotateY(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4( c,  0,  s,  0),
        vec4( 0,  1,  0,  0),
        vec4(-s,  0,  c,  0),
        vec4( 0,  0,  0,  1)
    );
}

mat4 _sdf_rotateZ(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4( c, -s,  0,  0),
        vec4( s,  c,  0,  0),
        vec4( 0,  0,  1,  0),
        vec4( 0,  0,  0,  1)
    );
}


float distance_from_sphere(in vec3 p0, in vec3 c0, float r0, float s0)
{
    return length(p0 - c0) - r0 + s0 * _sdf_snoise(p0);
}

float _sdf_map_the_world(in vec3 p)
{
    vec3 px  = (_sdf_rotateX(-rx) * vec4(p,  1.0)).xyz;
    vec3 py  = (_sdf_rotateY(-ry) * vec4(px, 1.0)).xyz;
    vec3 pz  = (_sdf_rotateZ(-rz) * vec4(py, 1.0)).xyz;
    return distance_from_sphere(pz, vec3(0, 0, 0), r, h);
}

vec3 _sdf_calculate_normal(in vec3 p)
{
    const vec3 small_step = vec3(0.01, 0.0, 0.0);

    float gradient_x = _sdf_map_the_world(p + small_step.xyy) - _sdf_map_the_world(p - small_step.xyy);
    float gradient_y = _sdf_map_the_world(p + small_step.yxy) - _sdf_map_the_world(p - small_step.yxy);
    float gradient_z = _sdf_map_the_world(p + small_step.yyx) - _sdf_map_the_world(p - small_step.yyx);

    return normalize(vec3(gradient_x, gradient_y, gradient_z));
}

vec3 _sdf_ray_march(in vec3 ro, in vec3 rd)
{
    float total_distance_traveled = 0.0;
    const int NUMBER_OF_STEPS = 32;
    const float MINIMUM_HIT_DISTANCE = 0.01;
    const float MAXIMUM_TRACE_DISTANCE = 1000.0;
    const vec3 ambientColor  = vec3(0.05, 0.05, 0.10);
    const vec3 specularColor = vec3(0.7, 0.7, 0.75);

    for (int i = 0; i < NUMBER_OF_STEPS; ++i)
    {
        vec3 current_position = ro + total_distance_traveled * rd;

        float distance_to_closest = _sdf_map_the_world(current_position);

        if (distance_to_closest < MINIMUM_HIT_DISTANCE) 
        {
            vec3 normal = _sdf_calculate_normal(current_position);
            vec3 light_position = vec3(3.0, 5.0, 3.0);
            vec3 direction_to_light = normalize(current_position - light_position);
            float diffuse_intensity = dot(normal, direction_to_light);
            vec3 reflectionDirection = normalize(reflect(-normalize(direction_to_light), normalize(normal)));
            vec3 diffuseColor = texture(tex0, panel.x*(reflectionDirection.xy)).xyz;
            if(diffuse_intensity > 0.0) {
                float specular = dot(normalize(normal), reflectionDirection);
                if(specular > 0.0) {
                    return vec3(ambientColor + diffuse_intensity * diffuseColor + pow(specular, 128.0) * specularColor);
                }
                else {
                    return vec3(ambientColor + diffuse_intensity * diffuseColor);
                }
            }
            else {
                return ambientColor * diffuseColor;
            }
        }

        if (total_distance_traveled > MAXIMUM_TRACE_DISTANCE)
        {
            break;
        }
        total_distance_traveled += distance_to_closest;
    }
    return vec3(0.0);
}

void main()
{
    vec2 uv = (gl_FragCoord.xy / panel.x) * 2.0 - 1.0;
    const vec3 camera_position = vec3(0.0, 0.0, -1.5);
    o_color = vec4(_sdf_ray_march(camera_position, vec3(uv, 1.0)), 1.0);
}
