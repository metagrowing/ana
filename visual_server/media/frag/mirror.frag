#version 330
in vec2 texCoordVarying;
out vec4 outputColor;

uniform float xm = 0.5;
uniform float ym = 0.5;
uniform ivec2 panel;
uniform sampler2DRect tex0;

void main()
{
    vec2 pos = vec2(gl_FragCoord.x < xm * panel.x ? panel.x - gl_FragCoord.x : gl_FragCoord.x,
                    gl_FragCoord.y < ym * panel.y ? panel.y - gl_FragCoord.y : gl_FragCoord.y);
    outputColor = vec4(texture(tex0, pos).xyz, 1.0);
}

