#version 330
uniform sampler2DRect tex0;
uniform int frame;
uniform ivec2 panel;
uniform float threshold = 0.1;
uniform float dirX = 1.0;
uniform float dirY = 0.0;
out vec4 outputColor;
void main()
{
  vec2 uv2 = gl_FragCoord.xy/vec2(panel.xy);
  float fParity = mod(float(frame), 2.) * 2. - 1.;
  float vp = mod(floor(uv2.x * float(panel.x)), 2.0) * 2. - 1.;
  vec2 dir = vec2(dirX, dirY);
  dir *= fParity * vp;
  dir /= panel.xy;
  vec4 curr = texture2D(tex0, uv2);
  vec4 comp = texture2D(tex0, uv2 + dir);
  float gCurr = (curr.r+curr.g+curr.b)/3.; // gscale(curr.rgb);
  float gComp = (comp.r+comp.g+comp.b)/3.; // gscale(comp.rgb);
  if (uv2.x + dir.x < 0.0 || uv2.x + dir.x > 1.0) {
    outputColor = curr;
  }
  if (dir.x < 0.0) {
    if (gCurr > threshold && gComp > gCurr) {
      outputColor = comp;
    } else {
      outputColor = curr;
    }
  }
  else {
    if (gComp > threshold && gCurr >= gComp) {
      outputColor = comp;
    } else {
      outputColor = curr;
    }
  }

//   outputColor.xy = uv2;
  outputColor.a = 1.0;
}
