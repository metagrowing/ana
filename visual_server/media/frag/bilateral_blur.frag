#version 330
uniform sampler2DRect tex0;
uniform float csigma = 10.0;
uniform float bsigma =  0.7;
out vec4 outputColor;

// Bilateral filter.
// Based on https://www.shadertoy.com/view/4dfGDH#
// see: https://raw.githubusercontent.com/millerjv/sites/gh-pages/glimp/bilateral.html

#define MSIZE 15

float normpdf(in float x, in float sigma)
{
  return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
}

float normpdf3(in vec3 v, in float sigma)
{
  return 0.39894*exp(-0.5*dot(v,v)/(sigma*sigma))/sigma;
}

void main()
{
    const int kSize = (MSIZE-1)/2;
    float kernel[MSIZE];
    vec3 bfinal_colour = vec3(0.0);
    float bZ = 0.0;
    vec3 c = texture(tex0, gl_FragCoord.xy).rgb;

    //create the 1-D kernel
    for (int j = 0; j <= kSize; ++j) {
        kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), csigma);
    }

    vec3 cc;
    float gfactor;
    float bfactor;
    float bZnorm = 1.0/normpdf(0.0, bsigma);
    //read out the upstream texture buffer
    for (int i=-kSize; i <= kSize; ++i) {
        for (int j=-kSize; j <= kSize; ++j) {
            // color at a pixel in the neighborhood
            vec3 cc = texture(tex0, gl_FragCoord.xy + vec2(i, j)).rgb;

            // compute both the gaussian smoothed and bilateral
            gfactor = kernel[kSize+j] * kernel[kSize+i];
            bfactor = normpdf3(cc-c.rgb, bsigma) * bZnorm * gfactor;
            bZ += bfactor;

            bfinal_colour += bfactor * cc;
        }
    }
    outputColor = vec4(bfinal_colour / bZ, 1.0);
}
