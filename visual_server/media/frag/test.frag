#version 330
uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform float r = 1.0;
uniform float g = 1.0;
uniform float b = 1.0;
out vec4 outputColor;
void main()
{
    outputColor.rgb = vec3(1.0) - texture(tex1, gl_FragCoord.xy).rgb;
    outputColor.rgb = outputColor.rgb * vec3(r, g, b);

    outputColor.a = 1.0;
}
