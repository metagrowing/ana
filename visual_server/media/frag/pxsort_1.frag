#version 330
uniform sampler2DRect tex0;
uniform int frame;
uniform ivec2 panel;
uniform float threshold = 0.1;
uniform float dirX = 1.0;
uniform float dirY = 0.0;
out vec4 outputColor;
void main()
{
  vec2 uv2 = gl_FragCoord.xy/vec2(panel.xy);
  int fParity = int(mod(float(frame), 2.0)) * 2 - 1;
  int vp = int(mod(gl_FragCoord.x * panel.x, 2)) * 2 - 1;
  ivec2 dir = ivec2(1, 0);
  dir *= fParity * vp;
  vec4 curr = texture(tex0, gl_FragCoord.xy);
  vec4 comp = texture(tex0, gl_FragCoord.xy + dir);
  float gCurr = (curr.r+curr.g+curr.b)/3.; // gscale(curr.rgb);
  float gComp = (comp.r+comp.g+comp.b)/3.; // gscale(comp.rgb);
  if (gl_FragCoord.x + dir.x < 0 || gl_FragCoord.x + dir.x > 1) {
    outputColor = curr;
  }
  if (dir.x < 0) {
    if (gCurr > threshold && gComp > gCurr) {
      outputColor = comp;
    } else {
      outputColor = curr;
    }
  }
  else {
    if (gComp > threshold && gCurr >= gComp) {
      outputColor = comp;
    } else {
      outputColor = curr;
    }
  }

  outputColor.a = 1.0;
}
