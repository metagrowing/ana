#version 330
uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform ivec2 panel;
uniform vec4 ks0123;
uniform vec4 ks4567;
uniform vec4 dt0123;
uniform vec4 dt4567;
out vec4 outputColor;

#define ks0 ks0123.x
#define ks1 ks0123.y
#define ks2 ks0123.z
#define ks3 ks0123.w
#define ks4 ks4567.x
#define ks5 ks4567.y
#define ks6 ks4567.z
#define ks7 ks4567.w

#define dt0 dt0123.x
#define dt1 dt0123.y
#define dt2 dt0123.z
#define dt3 dt0123.w
#define dt4 dt4567.x
#define dt5 dt4567.y
#define dt6 dt4567.z
#define dt7 dt4567.w

// like signed distance functions published by Inigo Quilez
// BUT only 2D
// see: https://iquilezles.org/articles/distfunctions/
float distance_from_sphere(in vec2 p0, in vec2 c0, in float r0)
{
    return distance(p0, c0) - r0;
}

float distance_from_box(in vec2 p0, in vec2 c0, in float r0, in float r1) {
  vec2 b = vec2(r0*0.5, r1*0.5) + c0;
  vec2 q = abs(p0) - b;
  return length(max(q, 0.0)) + min(max(q.x, q.y), 0.0);
}

float combine_union(in float d0, in float d1) {
  float k = 0.2*ks5;
  float h = clamp(0.5 + 0.5*(d1-d0)/k, 0.0, 1.0);
  return mix(d1, d0, h) - k*h*(1.0-h);
}

float combine_sub(in float d0, in float d1) {
  float k = 0.2*ks5;
  float h = clamp(0.5 - 0.5*(d1-d0)/k, 0.0, 1.0);
  return mix(d1, -d0, h) + k*h*(1.0-h);
}

#define combine_mul(d0,d1) ((d0) * (d1))

float combine_inter(in float d0, in float d1) {
  float k = 0.2*ks5;
  float h = clamp(0.5 - 0.5*(d1-d0)/k, 0.0, 1.0);
  return mix(d1, d0, h) + k*h*(1.0-h);
}

float usin(float a) {
  return 0.5+0.5*sin(25.0*a);
}

void main() {
  vec2 _xy = 2.0 * gl_FragCoord.xy / panel - 1.0;
  float dist = -1;
  _xy = atan(_xy);
  dist = combine_mul(combine_sub(distance_from_sphere(_xy, vec2(-0.1,  0.3), usin(dt0)),
                                 distance_from_box   (_xy, vec2( 0.0,  0.0), usin(dt1), usin(dt2))),
                     combine_union(distance_from_sphere(_xy, vec2(-0.1,  0.3), usin(dt4)),
                                   distance_from_box   (_xy, vec2( 0.0,  0.0), usin(dt5), usin(dt6))));
  outputColor = vec4(vec3(step(0, dist)), 1.0);
}
