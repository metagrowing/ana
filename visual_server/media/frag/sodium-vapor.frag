#version 330
uniform sampler2DRect tex0;
uniform float hue = 0.2;
uniform float band = 0.01;
out vec4 outputColor;

// --- hsv rgb --------------------------------------------------------
// Fast branchless HSV to RGB conversion in GLSL
// see: http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

void main()
{
    vec3 rgb = texture(tex0, gl_FragCoord.xy).rgb;
    vec3 hsv = rgb2hsv(rgb);
//     if(hsv.x > hue-band && hsv.x < hue+band)
//         outputColor = vec4(rgb, 1.0);
//     else
//         outputColor = vec4(0.1*rgb, 1.0);
//     float hd = clamp(1.0 - abs(hsv.x - hue), 0.0, 1.0);
    float hd = (1.0 - distance(hsv.x, hue));
    float v = 1.0 * hd*hsv.z;
    outputColor = vec4(hsv2rgb(vec3(hsv.x, hsv.y, v)), 1.0);
}
