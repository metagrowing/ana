#version 330
out vec4 outputColor;

uniform ivec2 panel;
uniform float t = 1.0;

void main()
{
    vec2 _xy = 2.0 * gl_FragCoord.xy / panel - 1.0;
    for(int l=0; l<30; ++l) {
        _xy = vec2(_xy.y + sin(t * _xy.x), _xy.x);
    }
    outputColor = vec4(_xy, 1.0, 1.0);
}

