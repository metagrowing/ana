#version 330
in vec2 texCoordVarying;
out vec4 outputColor;

uniform ivec2 panel;
uniform float t = 1.0;

// --- hsv rgb --------------------------------------------------------
// Fast branchless HSV to RGB conversion in GLSL
// see: http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

#define cx_add(a,b) vec2(a.x + b.x, a.y + b.y)
#define cx_sub(a,b) vec2(a.x - b.x, a.y - b.y)
#define cx_mul(a,b) vec2(a.x*b.x-a.y*b.y, a.x*b.y+a.y*b.x)
#define cx_div(a,b) vec2(((a.x*b.x+a.y*b.y)/(b.x*b.x+b.y*b.y)),((a.y*b.x-a.x*b.y)/(b.x*b.x+b.y*b.y)))
#define cx_sin(a) vec2(sin(a.x) * cosh(a.y), cos(a.x) * sinh(a.y))

void main()
{
//     vec2 _xy = 2.0 * gl_FragCoord.xy / panel - 1.0;
    vec2 _xy = 6.28 * gl_FragCoord.xy / panel - 3.14;
    for(int l=0; l<30; ++l) {
        _xy = vec2(_xy.y + sin(t * _xy.x), _xy.x);
    }
    float h = clamp(atan(_xy.y, _xy.x), 0.0, 1.0);
    float v = max(log(length(_xy)), 1.0);
    outputColor = vec4(hsv2rgb(vec3(h, 1.0, v)), 1.0);
//     outputColor = vec4(_xy, 1.0, 1.0);
}

