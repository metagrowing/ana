#version 330
in vec2 texCoordVarying;
out vec4 outputColor;

uniform float r = 0.5;
uniform float g = 0.5;
uniform float b = 0.5;
uniform sampler2DRect tex0;
uniform sampler2DRect tex1;

void main()
{
    vec3 cin0 = texture(tex0, gl_FragCoord.xy).xyz;
    vec3 cin1 = texture(tex1, gl_FragCoord.xy).xyz;
    outputColor = vec4(cross(cin0, cin1), 1.0);
}

