#pragma OPENCL EXTENSION all : enable

#define TRAIL(xy) (clamp((int)(img_y_max*xy.y), 0, img_y_max) * img_x_max \
                 + clamp((int)(img_x_max*xy.x), 0, img_x_max))

const sampler_t SAMPLER = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_REPEAT | CLK_FILTER_LINEAR;

__kernel void image_setup() {}
__kernel void image_change() {}

// ---------------------------------------------------------
__kernel void ag_setup(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global float2* pos,
__global float2* pos_out,
__global float2* vel,
__global float2* vel_out,
__global int* state,
__global int* state_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    const int ai = get_global_id(0);
    pos_out[ai] = fmod(pos[ai], (float2)(1.0f));
    vel_out[ai] = (float2)(0);
    state_out[ai] = 0;
    state_out[ai] = ai % 1000;
}

// ---------------------------------------------------------
__kernel void trail_setup(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global int* trailR,
__global int* trailR_out,
__global int* trailG,
__global int* trailG_out,
__global int* trailB,
__global int* trailB_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    const int ti = get_global_id(0);
    trailR_out[ti] = 0;
    trailG_out[ti] = 0;
    trailB_out[ti] = 0;
}


// ---------------------------------------------------------
__kernel void ag_move(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global float2* pos,
__global float2* pos_out,
__global float2* vel,
__global float2* vel_out,
__global int* state,
__global int* state_out,
__global int* trailR,
volatile __global int* trailR_out,
__global int* trailG,
volatile __global int* trailG_out,
__global int* trailB,
volatile __global int* trailB_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int ai = get_global_id(0);
    
    float2 old_pos = pos[ai];

    const float4 color = read_imagef(image_in, SAMPLER, old_pos);
    const float angle = atan2(color.y-0.5, color.x-0.5);
    const float cos_a = cos(angle);
    const float sin_a = sin(angle);
    const float2 new_ac = (float2)(p0 * length(color.xyz))
                          * (float2)(cos_a + sin_a,
                                     cos_a - sin_a);

    float2 delta_vel;
    float damping;
    damping = 0.95f + 0.05f * (1.0f - length(color.xyz));
    delta_vel = (float2)(p1) * new_ac;
    state_out[ai] = state[ai]-1;
    
    vel_out[ai] = damping*vel[ai]+delta_vel;
    pos_out[ai] = pos[ai] + (float2)(p2)*vel_out[ai];

    pos_out[ai] = fmod(pos_out[ai], 1.0f);
    int ti = TRAIL(old_pos);
    atomic_add(&trailR_out[ti], 5+(int)(color.x*1024));
    atomic_add(&trailG_out[ti], 5+(int)(color.y*1024));
    atomic_add(&trailB_out[ti], 5+(int)(color.z*1024));
}


// ---------------------------------------------------------
__kernel void trail_disipate(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global int* trailR,
__global int* trailR_out,
__global int* trailG,
__global int* trailG_out,
__global int* trailB,
__global int* trailB_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int ti = get_global_id(0);
    const int tmax = img_x_max * img_y_max;
    
    int W = ti-1;         if(W<0)                    W += img_x_max;
    int O = ti+1;         if(O>=img_x_max)           O -= img_x_max;
    int S = ti+img_x_max; if(S>=tmax)                S -= tmax;
    int N = ti-img_x_max; if(N<0)                    N += tmax;
    trailR_out[ti] = (float)(trailR[W]
                    +trailR[O]
                    +trailR[S]
                    +trailR[N]
                    + 2*trailR[ti]
                    ) / (6 + p4);
    trailG_out[ti] = (float)(trailG[W]
                    +trailG[O]
                    +trailG[S]
                    +trailG[N]
                    + 2*trailG[ti]
                    ) / (6 + p4);
    trailB_out[ti] = (float)(trailB[W]
                    +trailB[O]
                    +trailB[S]
                    +trailB[N]
                    + 2*trailB[ti]
                    ) / (6 + p4);
}


// ---------------------------------------------------------
__kernel void ag_draw(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global int* trailR,
__global int* trailG,
__global int* trailB,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__write_only image2d_t image_out
)
{
    const int px = get_global_id(0);
    const int py = get_global_id(1);
    const int ti = py * img_x_max + px;
    
    const float r = pow((float)trailR[ti] / (float)1024, p7);
    const float g = pow((float)trailG[ti] / (float)1024, p7);
    const float b = pow((float)trailB[ti] / (float)1024, p7);
    write_imagef(image_out, (int2)(px, py), (float4)(r, g, b, 1));
}
