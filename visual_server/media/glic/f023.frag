#version 330
uniform ivec2 panel;
uniform sampler2DRect tex0;
uniform float p0;
out vec4 outputColor;

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

void main() {
//     vec3 ci = texture2D(tex0, vec2(gl_FragCoord.x/float(panel.x),1.0-gl_FragCoord.y/float(panel.y)).rgb;
    vec3 ci = texture(tex0, gl_FragCoord.xy).xyz;
    vec3 hsv = rgb2hsv(ci);
    hsv.x += p0;
    vec3 false_rgb = hsv2rgb(hsv);
    false_rgb = vec3(1.0-false_rgb.r, false_rgb.g, false_rgb.b);

    hsv = rgb2hsv(false_rgb);
    hsv.x -= p0;
    outputColor = vec4(hsv2rgb(hsv), 1.0);
//     outputColor = vec4(ci, 1.0);
}
