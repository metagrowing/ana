#version 330
uniform ivec2 panel;
uniform sampler2DRect tex0;
uniform float p0;
uniform float p1;
out vec4 outputColor;

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

void main() {
    vec3 cfx = vec3(0.0);
    #define move_px(dx) vec2((gl_FragCoord.x+50.0*dx),gl_FragCoord.y)
    #define move_mx(dx) vec2((gl_FragCoord.x-50.0*dx),gl_FragCoord.y)

    vec3 ci_r = texture(tex0, move_px(0.0)).rgb;
    vec3 hsv_r = rgb2hsv(ci_r);
    hsv_r.x += p1;
    vec3 cf_r = hsv2rgb(hsv_r);
    cfx.r = cf_r.r;

    vec3 ci_g = texture(tex0, move_mx(p0)).rgb;
    vec3 hsv_g = rgb2hsv(ci_g);
    hsv_g.x += p1;
    vec3 cf_g = hsv2rgb(hsv_g);
    cfx.g = cf_g.g;

    vec3 ci_b = texture(tex0, move_px(p0)).rgb;
    vec3 hsv_b = rgb2hsv(ci_b);
    hsv_b.x += p1;
    vec3 cf_b = hsv2rgb(hsv_b);
    cfx.b = cf_b.b;

    vec3 hsv = rgb2hsv(cfx);
    hsv.x -= p1;
    outputColor = vec4(hsv2rgb(hsv), 1.0);
}
