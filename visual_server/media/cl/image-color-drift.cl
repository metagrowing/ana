__kernel void image_setup(
__read_only image2d_t image_in,
__read_only image2d_t image_src,
__write_only image2d_t image_dst,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    // setup code not used in this exsample
}

__kernel void image_change(
__read_only image2d_t image_in,
__read_only image2d_t image_src,
__write_only image2d_t image_dst,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int x = get_global_id(0);
    const int y = get_global_id(1);

    sampler_t smp = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;
    const int2   center   = (int2)(x, y);
    // upstream color
    float4 color_up = read_imagef(image_in,  smp, center);
    
    // colors from the current source buffer
    const float2 drift1   = (float2)(x+p1, y+p2);
    const float2 drift2   = (float2)(x+p3, y+p4);
    float4 color_bc = read_imagef(image_src, smp, center);
    float4 color_d1 = read_imagef(image_src, smp, drift1);
    float4 color_d2 = read_imagef(image_src, smp, drift2);

    // merge upstream color to the feedback loop
    float a = p0;
    float b = 1.0 - a;
    float c = b / 2.975;
    write_imagef(image_dst, center, (float4)(
        a*color_up.x + c*color_d1.x + c*color_bc.x + c*color_d2.x,
        a*color_up.y + c*color_d1.y + c*color_bc.y + c*color_d2.y,
        a*color_up.z + c*color_d1.z + c*color_bc.z + c*color_d2.z,
        1.0
    ));
}

