//#pragma OPENCL EXTENSION all : enable

const sampler_t SAMPLER = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_REPEAT | CLK_FILTER_LINEAR;

typedef struct {
    float mass;
    float rad;
    int group;
    float d3;
} AgentAttributes;

typedef struct {
    float4 pos;
    float4 vel;
} AgentState;


// This is a simple pseudo random number generator as used for example in shadertoys.
// It is based on scrambling the bits of the coordinates of the currently sampled point.
float rand_float(int n) {
  n = (n << 13) ^ n; 
  return (float)((n * (n*n*15731+789221) + 1376312589) & 0x7fffffff) / (float)(0x7fffffff);
}

// convert from hsv color to rgb color
float3 hsv2rgb(float3 c)
{
    float4 K = (float4)(1.0f, 2.0f / 3.0f, 1.0f / 3.0f, 3.0f);
    float3 dummy;
    float3 p = fabs(fract(c.xxx + K.xyz, &dummy) * (float3)(6.0, 6.0, 6.0) - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, (float3)(0.0, 0.0, 0.0), (float3)(1.0, 1.0, 1.0)), c.y);
}


__kernel void image_setup() {}
__kernel void image_change() {}

#define DAMPING         0.99f
#define FOCUS_FORCE     1.0f
#define CENTER_FORCE    0.2f
#define VISC            6.0f
#define DRAG            0.6f
#define MAX_VELOCITY    0.5f
    
// ---------------------------------------------------------
__kernel void ag_setup(
const int img_x_max,
const int img_y_max,
const int agent_length,
const int trace_length,
const int trace_index,
__global AgentState* astate_in,
__global AgentState* astate_out,
__global AgentAttributes* aattrib,
__global float16* traceBuffer, 
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    const int ai = get_global_id(0);

    __global AgentState *s_out = &astate_out[ai];
    s_out->pos = (float4)(10*(rand_float(ai+17)-0.5),
                          10*(rand_float(ai+31)-0.5),
                          10*(rand_float(ai+53)-0.5),
                          1);
    s_out->vel = (float4)(rand_float(ai+23) - 0.5,
                          rand_float(ai+13) - 0.5,
                          rand_float(ai+29) - 0.5,
                          0);
    s_out->vel *= MAX_VELOCITY;
    
    __global AgentAttributes *at = &aattrib[ai];
    at->mass = 5;
    at->rad = 0.5;
    at->group = ai % 2;
    
    int ti = ai*trace_length;
    for(int pi = trace_length-1; pi >= 0; --pi) {
        traceBuffer[ti+pi].xyzw  = s_out->pos;
        traceBuffer[ti+pi].s4567 = (float4)(1, 0, 0, 0); // normal
        traceBuffer[ti+pi].s89ab = (float4)(1, 0.5, 0, 1); // color
    }
}

// ---------------------------------------------------------
__kernel void ag_move(
const int img_x_max,
const int img_y_max,
const int agent_length,
const int trace_length,
const int trace_index,
__global AgentState* astate_in,
__global AgentState* astate_out,
__global AgentAttributes* aattrib,
__global float16* traceBuffer, 
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int ai = get_global_id(0);
    __global AgentAttributes *at = &aattrib[ai];
    __global AgentState *s_in  = &astate_in[ai];
    __global AgentState *s_out = &astate_out[ai];
    
    float3 focusL = (float3)(150*p0, 150*p1, 5);
    float3 focusR = (float3)(150*p2, 150*p3, -5);
//     float3 focusL = (float3)(p0, p1,  1);
//     float3 focusR = (float3)(p2, p3, -1);

    float3 f = (float3)(0);
    
    for(int ni=0; ni<agent_length; ++ni) {
        if(ni != ai) {
            const float3 diff = astate_in[ni].pos.xyz - s_in->pos.xyz;
            f += diff * (CENTER_FORCE * p6);
        }
    }

    if(at->group == 0) {
        const float3 diffL = focusL - s_in->pos.xyz;
        const float invDistSQL = 1.0f / dot(diffL, diffL);
        f += diffL * (FOCUS_FORCE * invDistSQL * p7);
    }
    
    if(at->group == 1) {
        const float3 diffR = focusR - s_in->pos.xyz;
        const float invDistSQR = 1.0f / dot(diffR, diffR);
        f += diffR * (FOCUS_FORCE * invDistSQR * p7);
    }
    
    const float speed = length(s_in->vel.xyz);
    const float df = 0.5 * VISC * speed * speed * (3.14159 * at->rad * at->rad) * DRAG;
    f += df * normalize(-s_in->vel.xyz);

    const float dt = 1.0 / 25.0;
    float3 a = f / at->mass;

    float3 vvel = a * dt;
    float3 nvel = normalize(vvel);
    float  vel = min(MAX_VELOCITY, length(vvel));
    s_out->vel.xyz = s_in->vel.xyz + nvel * vel * DAMPING;
    
    s_out->pos.xyz = s_in->pos.xyz + s_out->vel.xyz * dt;

    int ti = ai*trace_length;
    for(int pi= trace_length - 2; pi >= 0; --pi) {
        traceBuffer[ti+pi+1] = traceBuffer[ti+pi];
    }
    traceBuffer[ti].xyzw  = (float4)(s_out->pos.xyz, 1);
    
//     traceBuffer[ti].s4567 = (float4)(hsv2rgb((float3)(vel, 0.8, 25*vel)), 0.5);
//     traceBuffer[ti].s456 =normalize(cross(s_out->vel.xyz, (float3)(0, 0, 1)));
//     traceBuffer[ti].s456 =normalize(cross(s_out->vel.xyz, (float3)(1, 1, 0)));
    traceBuffer[ti].s456 =normalize(cross(traceBuffer[ti].xyz, traceBuffer[ti+1].xyz));
    traceBuffer[ti].s7 = 1;
    
    traceBuffer[ti].s89ab = (float4)(hsv2rgb((float3)(0.3*vel-0.12, 0.8, 5*vel*vel)), 1);
}
