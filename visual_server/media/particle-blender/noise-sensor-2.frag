#version 330
uniform sampler2DRect tex0;
out vec4 outputColor;

#define scale 25.0
void main()
{
    vec3 c00 = texture(tex0, gl_FragCoord.xy).rgb;
    vec3 c10 = texture(tex0, gl_FragCoord.xy + vec2(1,  0)).rgb;
    vec3 c01 = texture(tex0, gl_FragCoord.xy + vec2(0,  1)).rgb;
    float r = scale * (distance(c00.r, c01.r) + distance(c00.r, c10.r));
    float g = scale * (distance(c00.g, c01.g) + distance(c00.g, c10.g));
    float b = scale * (distance(c00.b, c01.b) + distance(c00.b, c10.b));
    outputColor = vec4(r, g, b, 1.0);
}
