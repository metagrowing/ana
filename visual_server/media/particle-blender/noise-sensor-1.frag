#version 330
uniform sampler2DRect tex0;
out vec4 outputColor;

#define scale 25.0
void main()
{
    vec3 c00 = texture(tex0, gl_FragCoord.xy).rgb;
    vec3 c10 = texture(tex0, gl_FragCoord.xy + vec2(1,  0)).rgb;
    vec3 c01 = texture(tex0, gl_FragCoord.xy + vec2(0,  1)).rgb;
    float gray = scale * (distance(c00, c01) + distance(c00, c10));
    outputColor = vec4(gray, gray, gray, 1.0);
}
