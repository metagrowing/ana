// float edist(int2 a, float2 b){
//     float2 d = (float2)((a.x - b.x), (a.y - b.y));
// //     return d.x * d.x + d.y * d.y;
//      return fabs(d.x) * fabs(d.y);
// //     return fmin(fabs(d.x), fabs(d.y));
// //     return fabs(d.x) - fabs(d.y);
// }

float mdist(int2 a, float2 b){
	return fabs(a.x - b.x) + fabs(a.y - b.y);
}

float mmdist(int2 a, float2 b){
	return fmod(fabs(a.x - b.x) + fabs(a.y - b.y), 1 * 1080);
}

float edist(int2 a, float2 b){
	float2 d = (float2)((a.x - b.x), (a.y - b.y));
	return d.x * d.x + d.y * d.y;
}

float pdist(int2 a, float2 b){
	return fabs((float)a.x - b.x) * fabs((float)a.y - b.y);
}

float powdist(int2 a, float2 b){
	return pow(fabs(a.x - b.x), 1 + 0.1f * fabs(a.y - b.y));
}

float qdist(int2 a, float2 b){
	return fabs(a.x - b.x) / (1 + fabs(a.y - b.y));
}

float atan2dist(int2 a, float2 b){
	return atan2(a.x - b.x, a.y - b.y);
}

float maxdist(int2 a, float2 b){
	return fmax(fabs(a.x - b.x), fabs(a.y - b.y));
}

float mindist(int2 a, float2 b){
	return fmin(fabs(a.x - b.x), fabs(a.y - b.y));
}

float medist(int2 a, float2 b){
	const float fa = 0.1f;
	const float fb = 1.0f - fa;
	return fa * mindist(a, b) + fb * sqrt(maxdist(a, b));
}

float3 hsv2rgb(float3 c)
{
    float4 K = (float4)(1.0f, 2.0f / 3.0f, 1.0f / 3.0f, 3.0f);
    float3 dummy;
    float3 p = fabs(fract(c.xxx + K.xyz, &dummy) * (float3)(6.0, 6.0, 6.0) - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, (float3)(0.0, 0.0, 0.0), (float3)(1.0, 1.0, 1.0)), c.y);
}

__kernel void image_change(const int npoints,
                        __constant float8 * fv,
                        __write_only image2d_t iout){
    const int2 pos = (int2)(get_global_id(0), get_global_id(1));
    float min_dist = 1000000.0;
    int min_at_i = 0;
    for(int i = 0; i < npoints; ++i) {
        float dist = pdist(pos, fv[i].xy + (float2)(540,540));
        if(dist < min_dist) {
            min_dist = dist;
            min_at_i = i;
        }
    }

    
    const float influence = 27;
    float damping = clamp((influence-sqrt(min_dist)) / influence, 0.0f, 1.0f);
    float3 rgb = hsv2rgb(fv[min_at_i].s456);
    rgb.x *= damping;
    rgb.y *= damping;
    rgb.z *= damping;
    write_imagef(iout, pos, (float4)(rgb.x, rgb.y, rgb.z, 1));
}

__kernel void image_setup(const int npoints,
                        __constant float8 * fv,
                        __write_only image2d_t iout){
}
