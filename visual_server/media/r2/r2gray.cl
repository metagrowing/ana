__kernel void image_setup(
__global int* counters,
__write_only image2d_t image2
) 
{
    // dummy
}

__kernel void image_change(
__global int* counters,
__write_only image2d_t image2
) 
{
    // dummy
}

//-----------------------------
float2 sinusoidal(float2 v, float amount) {
    return amount * sin(v);
}

float2 spherical(float2 v, float amount) {
    float r = length(v);
    return (amount * 1.0f / (r * r)) * v;
}

float2 expotential(float2 v, float amount) {
    return (amount * exp(v.x-1)) * (float2)(cos(M_PI_F*v.y), sin(M_PI_F*v.y));
}

float2 hyperbolic(float2 v, float amount) {
    float r = length(v) + 1.0e-10;
    float theta = atan2(v.x, v.y);
    return amount * (float2)(sin(theta) / r, cos(theta) * r);
}

// parametrization P={p1, p2, p3, p4}
float2 pdj(float2 v, float amount, float p1, float p2, float p3, float p4) {
    return amount *(float2)(sin(p1 * v.y) - cos(p2 * v.x),
                            sin(p3 * v.x) - cos(p4 * v.y));
}

float2 rect(float2 v, float amount, float p1, float p2) {
    float2 pp = (float2)(M_PI_F*p1, M_PI_F*p2);
    return amount * sign(v) * (((2.0f * fabs(v / pp) + 1.0f) * pp) - v);
}

float2 sech(float2 v, float amount) {
  float d = cos(2.0f * v.y) + cosh(2.0f * v.x);
//   if (d != 0)
  if (fabs(d) > 0.0001)
    d = amount * 2.0f / d;
  return (float2)(d * cos(v.y) * cosh(v.x), -d * sin(v.y) * sinh(v.x));
}

//-----------------------------
float2 remap_sin(float2 v) {
    return M_PI_F * sin(v);
}

float2 remap_torus(float2 v) {
    return fmod(v, (float2)(M_PI_F, M_PI_F));
}

//-----------------------------
#define a_index(ix,iy) (SX*iy + ix)
#define g_index(ix,iy) (2*SX*iy + 2 * ix)
__kernel void set_zero(
__global int* countersR,
__global int* countersG,
__global int* countersB
) 
{
    const int ix = get_global_id(0);
    const int iy = get_global_id(1);
    const int SX = get_global_size(0);
    const int i_here = a_index(ix, iy);

    countersR[i_here] = 0;
    countersG[i_here] = 0;
    countersB[i_here] = 0;
}

__kernel void map_r2(
__global int* countersR,
__global int* countersG,
__global int* countersB,
__read_only image2d_t image_in,
__global float* gaussian,
__global float* uniform,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
const float dt0,
const float dt1,
const float dt2,
const float dt3,
const float dt4,
const float dt5,
const float dt6,
const float dt7
) 
{
    const int ix = get_global_id(0);
    const int iy = get_global_id(1);
    const int SX = get_global_size(0);
    const int SY = get_global_size(1);
    const int i_here = a_index(ix, iy);

    float px = (float)ix / (float)SX;
    float py = (float)iy / (float)SY;
    const float w0 = M_PI_F;
    float2 pos = (float2)((2.0f*px-1) * w0,
                          (2.0f*py-1) * w0);
    
    const float ga0 = p7 * 0.004;
    const int g_here = g_index(ix, iy);
    pos += ga0 * (float2)(gaussian[g_here], gaussian[g_here+1]);

#define A6

#ifdef A1
    for(uint l=0; l<7; ++l) {
        pos = sinusoidal(pos, p0);
        pos = spherical(pos, p1);
        pos = expotential(pos, p2);
    }
#endif

#ifdef A2
    for(uint l=0; l<3; ++l) pos = sinusoidal(pos, p0);
    for(uint l=0; l<5; ++l) pos = spherical(pos, p1);
    for(uint l=0; l<7; ++l) pos = expotential(pos, p2);
#endif

#ifdef A3
    pos = sinusoidal(pos, p0) * spherical(pos, p1) - expotential(pos, p2);
#endif

#ifdef A4
    for(uint l=0; l<1; ++l) pos = rect(pos, p0, p1, p2);
#endif

#ifdef A5
    for(uint l=0; l<3; ++l) {
        pos = sech(pos, p0);
        pos = pdj(pos, p0, p1, p2, p3, p4);
    }
#endif

#ifdef A6
    for(uint l=0; l<10; ++l) pos = hyperbolic(pos, p0);
#endif

    float2 out_pos = 0.5f + pos / ( w0 * 2);
    int ox_pos = SX*out_pos.x;
    int oy_pos = SY*out_pos.y;
    if(ox_pos >= 0 && ox_pos < SX
        && oy_pos >= 0 && oy_pos < SY) {
        const int o_here = a_index(ox_pos, oy_pos);
        atomic_inc(&countersR[o_here]);
    }
}

__kernel void write_image(
__global int* countersR,
__global int* countersG,
__global int* countersB,
__write_only image2d_t image2
) 
{
    const int ix = get_global_id(0);
    const int iy = get_global_id(1);
    const int SX = get_global_size(0);
    const int i_here = a_index(ix,iy);

    float result = countersR[i_here] > 0 ? tanh((float)countersR[i_here]) : 0;
    write_imagef(image2, (int2)(ix, iy), (float4)(result, result, result, 1));
}

