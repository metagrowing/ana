__kernel void image_setup(
__global int* counters,
__write_only image2d_t image2
) 
{
    // dummy
}

__kernel void image_change(
__global int* counters,
__write_only image2d_t image2
) 
{
    // dummy
}

//-----------------------------
float2 remap_sin(float2 v) {
    return M_PI_F * sin(v);
}

float2 remap_torus(float2 v) {
    return fmod(v, (float2)(M_PI_F, M_PI_F));
}

//-----------------------------
#define REPEAT 4
#define SNAP 5
#define a_index(ix,iy) (SX*iy + ix)
#define g_index(ix,iy) (2*SX*iy + 2 * ix)
__kernel void set_zero(
__global int* countersR,
__global int* countersG,
__global int* countersB
) 
{
    const int ix = get_global_id(0);
    const int iy = get_global_id(1);
    const int SX = get_global_size(0);
    const int i_here = a_index(ix, iy);

    countersR[i_here] = 0;
    countersG[i_here] = 0;
    countersB[i_here] = 0;
}

__kernel void map_r2(
__global int* countersR,
__global int* countersG,
__global int* countersB,
__read_only image2d_t image_in,
__global float* gaussian,
__global float* uniform,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
const float dt0,
const float dt1,
const float dt2,
const float dt3,
const float dt4,
const float dt5,
const float dt6,
const float dt7
) 
{
    const int ix = get_global_id(0);
    const int iy = get_global_id(1);
    const int SX = get_global_size(0);
    const int SY = get_global_size(1);
    const int i_here = a_index(ix, iy);

    sampler_t smp = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

    const float px = (float)ix / (float)SX;
    const float py = (float)iy / (float)SY;
    const float w0 = M_PI_F;
    
for(int lx=0; lx<REPEAT; ++lx) {
    float2 pos = (float2)((2.0f*px-1) * w0,
                          (2.0f*py-1) * w0);
    pos.x = floor(SNAP*pos.x) / SNAP;
    const float ga0 = p7 * 0.04;
//      const int bounds = 1;
//     const int bounds = SX * SY;
    const int bounds = SX;
    const int g_here = g_index((ix+lx), (iy+lx));
    pos += ga0 * (float2)(gaussian[g_here % bounds], gaussian[(g_here+1) % bounds]);

    for(uint l=0; l<1; ++l) {
        { //kaleidoscope
            const float sides = 5;
            const float r = length(pos);
            float a = p2 * atan2(pos.y, pos.x);
            int s = (int)(uniform[g_here] * sides);
            a += s * M_PI_F * 2.0f / sides;
            pos = p1 * r * (float2)(cos(a), sin(a));
        }
        { // mirror
            if(gaussian[g_here % bounds] < 0)
                pos.x = -pos.x;
            if(gaussian[(g_here+1) % bounds] < 0)
                pos.y = -pos.y;
            pos *= p0;
        }
    }

//     pos = remap_torus(pos);
    
    float2 out_pos = 0.5f + pos / ( w0 * 2);
    int ox_pos = SX*out_pos.x;
    int oy_pos = SY*out_pos.y;
    if(ox_pos >= 0 && ox_pos < SX
       && oy_pos >= 0 && oy_pos < SY) {
        const float4 color_in = read_imagef(image_in, smp, (int2)(ix, iy));
        const int red =   (int)(256*color_in.x);
        const int green = (int)(256*color_in.y);
        const int blue =  (int)(256*color_in.z);
        const int o_here = a_index(ox_pos, oy_pos);
        atomic_add(&countersR[o_here], red);
        atomic_add(&countersG[o_here], green);
        atomic_add(&countersB[o_here], blue);
    }
} // end REPEAT
}

__kernel void write_image(
__global int* countersR,
__global int* countersG,
__global int* countersB,
__write_only image2d_t image2
) 
{
    const int ix = get_global_id(0);
    const int iy = get_global_id(1);
    const int SX = get_global_size(0);
    const int i_here = a_index(ix,iy);

    const int cscale = 256 * REPEAT;
    float red =   countersR[i_here] > 0 ? tanh((float)(countersR[i_here]/cscale)) : 0;
    float green = countersG[i_here] > 0 ? tanh((float)(countersG[i_here]/cscale)) : 0;
    float blue =  countersB[i_here] > 0 ? tanh((float)(countersB[i_here]/cscale)) : 0;
    write_imagef(image2, (int2)(ix, iy), (float4)(red, green, blue, 1));
}
