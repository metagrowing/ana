#version 330
uniform sampler2DRect tex0;
uniform ivec2 panel;
uniform int frame;
uniform int sframe = 0;
uniform int slength = 1;
uniform int li;
uniform float b0;
uniform float volume;
uniform float onset;

uniform vec4 fft03;
uniform vec4 fft47;

uniform vec4 u0123;
uniform vec4 u4567;

uniform vec4 t0123;
uniform vec4 t4567;

uniform vec4 kp0123;
uniform vec4 kp4567;
uniform vec4 ks0123;
uniform vec4 ks4567;
uniform vec4 kb0123;
uniform vec4 kb4567;

uniform vec4 js0xy;
uniform vec2 js0lr;

#define _p0 const float p0 = 0.9
#define _p1 const float p1 = 0
#define _p2 const float p2 = 0
/*-pi-*/
/*
vec4 rand0(float time) {
  // see: http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/
  const highp float a = 12.9898;
  const highp float b = 78.233;
  const highp float c = 43758.5453;
  highp float dt = dot(floor((_st * resolution) / size), vec2(time) + vec2(a ,b));
  highp float sn = mod(dt, 3.141592653589793);
  highp float d = fract(sin(sn) * c);
  return vec4(d, d, d, 1);
}
*/
/*
highp float rand(vec2 st)
{
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
//     highp float dt= dot(gl_FragCoord.xy, vec2(a,b));
    highp float dt= dot(st, vec2(a,b));
    highp float sn= mod(dt, 3.14);
    return fract(sin(sn) * c);
}
*/

out vec4 outputColor;
/*
void main()
{
    outputColor  = texture(tex0, gl_FragCoord.xy);
    if(rand(vec2(p1, p2)) > p0)
        outputColor = vec4(1.0) - outputColor;

    outputColor.a = 1.0;
}
*/

// see: https://www.shadertoy.com/view/3d3fR7
// Free for any purpose, commercial or otherwise. 
// But do post here so I can see where it got used!
// If using on shadertoy, do link to this project on yours :)

float bwnoise(vec2 pos, highp float evolve) {
    // Loop the evolution (over a very long period of time).
    highp float e = fract(evolve * 0.0131);

    // Coordinates
    highp float cx  = pos.x * e;
    highp float cy  = pos.y * e;

    // Generate a "random" black or white value
    return fract(23.0*fract(2.0/fract(fract(cx*2.4/cy*23.0+pow(abs(cy/22.4),3.3))*fract(cx*evolve/pow(abs(cy),0.050)))));
}


void main()
{
/*-maindef-*/
    _p0;
    _p1;
    _p2;
    
    // Generate a black to white pixel
    vec4 bw = vec4(bwnoise(vec2(2) + gl_FragCoord.xy / panel.xy, p0));
    
    outputColor  = bw + texture(tex0, gl_FragCoord.xy);

    outputColor.a = 1.0;
}
