#version 330
uniform sampler2DRect tex0;
uniform ivec2 panel;
uniform int frame;
uniform int sframe = 0;
uniform int slength = 1;
uniform int li;
uniform float b0;
uniform float volume;
uniform float onset;

uniform vec4 fft03;
uniform vec4 fft47;

uniform vec4 u0123;
uniform vec4 u4567;

uniform vec4 t0123;
uniform vec4 t4567;

uniform vec4 kp0123;
uniform vec4 kp4567;
uniform vec4 ks0123;
uniform vec4 ks4567;
uniform vec4 kb0123;
uniform vec4 kb4567;

uniform vec4 js0xy;
uniform vec2 js0lr;

#define _p0 const float p0 = 0.5
#define _p1 const float p1 = 2.0
#define _p2 const float p2 = 0.1
/*-pi-*/

out vec4 outputColor;

// --- hsv rgb --------------------------------------------------------
// Fast branchless HSV to RGB conversion in GLSL
// see: http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

void main()
{
/*-maindef-*/
    _p0;
    _p1;
    _p2;
    vec3 hsv_color = rgb2hsv(texture(tex0, gl_FragCoord.xy).rgb);
    vec3 temp_color = hsv_color;
    
    temp_color.b = pow(temp_color.b, p1);
    temp_color.b = (temp_color.b < p0) ? (1.0 - temp_color.b / p0) : (temp_color.b - p0) / p0;
    temp_color.g = temp_color.g * hsv_color.b * p2;
    
    outputColor.rgb = hsv2rgb(temp_color);

    outputColor.a = 1.0;
}
