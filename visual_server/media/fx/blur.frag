#version 330
uniform sampler2DRect tex0;
out vec4 outputColor;
void main()
{
    outputColor  = 0.077847 * texture(tex0, gl_FragCoord.xy + vec2(-1,  1));
    outputColor += 0.123317 * texture(tex0, gl_FragCoord.xy + vec2( 0,  1));
    outputColor += 0.077847 * texture(tex0, gl_FragCoord.xy + vec2( 1,  1));

    outputColor += 0.123317 * texture(tex0, gl_FragCoord.xy + vec2(-1,  0));
    outputColor += 0.195346 * texture(tex0, gl_FragCoord.xy);
    outputColor += 0.123317 * texture(tex0, gl_FragCoord.xy + vec2( 1,  0));

    outputColor += 0.077847 * texture(tex0, gl_FragCoord.xy + vec2(-1, -1));
    outputColor += 0.123317 * texture(tex0, gl_FragCoord.xy + vec2( 0, -1));
    outputColor += 0.077847 * texture(tex0, gl_FragCoord.xy + vec2( 1, -1));

    outputColor.a = 1.0;
}
