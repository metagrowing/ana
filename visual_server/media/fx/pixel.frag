#version 330
uniform sampler2DRect tex0;
uniform ivec2 panel;
uniform int frame;
uniform int sframe = 0;
uniform int slength = 1;
uniform int li;
uniform float b0;
uniform float volume;
uniform float onset;

uniform vec4 fft03;
uniform vec4 fft47;

uniform vec4 u0123;
uniform vec4 u4567;

uniform vec4 t0123;
uniform vec4 t4567;

uniform vec4 kp0123;
uniform vec4 kp4567;
uniform vec4 ks0123;
uniform vec4 ks4567;
uniform vec4 kb0123;
uniform vec4 kb4567;

uniform vec4 js0xy;
uniform vec2 js0lr;

#define _p0 const float p0 = 20
#define _p1 const float p1 = 20
/*-pi-*/

out vec4 outputColor;
void main()
{
/*-maindef-*/
    _p0;
    _p1;
    vec2 pixel_size = vec2(p0, p1);
    outputColor  = texture(tex0, floor(gl_FragCoord.xy / pixel_size) * pixel_size + 0.5 * pixel_size);

    outputColor.a = 1.0;
}
