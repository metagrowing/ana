#version 330
uniform sampler2DRect tex0;
out vec4 outputColor;
void main()
{
    outputColor  = vec4(1.0) - texture(tex0, gl_FragCoord.xy);

    outputColor.a = 1.0;
}
