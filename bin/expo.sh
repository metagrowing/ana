#!/bin/bash
mkdir /dev/shm/expo
#echo -x
uvcdynctrl -s "Power Line Frequency" 1
uvcdynctrl -s  Sharpness 7
uvcdynctrl -s "Focus, Auto" 0
sleep 1
for i in `seq 64`
do
uvcdynctrl -s "Brightness" $i
fn=$(printf "%05d" $i)
fswebcam --no-banner -d /dev/video0 -r 3264x2448 /dev/shm/expo/expo_$fn.jpg
#sleep 1
done
#echo +x
