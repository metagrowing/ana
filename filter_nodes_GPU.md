# GPU filter nodes

GPU Filter nodes are processing image data at every single pixel for each frame. Most of this calculation are executed on the GPU as OpenGL fragment shaders. This is an *single instruction multiple data* approach.

[[_TOC_]]

## Media input filters
### Loading and processing images
The visualization server can load a limited amount of images and stores them in texture units on the GPU. The amount of texture units is small. But 4 to 8 images are enough for great visual effects.

Assume there are 4 PNG files in directory `visual_server/media/flower-sampler/`. You can load them with the following statement:

```clojure
(image "flower-sampler/*.png")
```
Or you can load one specific image from a directory containing many images.
```clojure
(image "flower-sampler/P1120404-1080x1080.png")
```

Hint:
- Use names like `c0` to `c7` for inputs channels.
- Identifying input channels is done by position in the parameter list. Names and numbering did not care. But this may be changed in a next release of aNa.

#### HSV processing of a single image
```clojure
(hsv :img0 [c0] ... ; hsv + img: this a source filter for HSV expressions
```
##### Example with one single image.
Colors are processed in hsv color space.
```clojure
(ns lv.demo)
(use 'lv.core)

(layout "vertical" [img0])
; assume there are up to 4 PNG images in the media folder.
(image "flower-sampler/*.png")

; deform the first channel c0
(hsv :img0 [c0]
  (rot (* f 0.005))
  (sx (+ 2 (snoise (* f 0.01) y)))
  (sy (+ 2 (snoise x (* f 0.01))))
  (hsv c0.hsv)
)
```

#### HSV processing of 3 from 4 images
```clojure
(rgb :img0 [c0 c1 c2 c3] ... ; rgb + img: this a source filter for RGB expressions
```
##### Example mixing 3 of 4 images in rgb color space.
```clojure
(ns lv.demo)
(use 'lv.core)

(layout "vertical" [img0])
; assume there are up to 4 PNG images in the media folder.
(image "flower-sampler/*.png")

; mix colors in rgb space from image c1, c2 and c3
; ignore the already loaded image c0
(rgb :img0 [c0 c1 c2 c3]
  (rgb (mix c1.rgb
            (mix c2.rgb
                 c3.rgb
                 (sin (* c1.g f)))
            (snoise x (sin (* 0.05 f)))))
)
```
![](./stuff/doc/mix-image-3of4.jpg)

### Loading of an image sequence to an OpenGL texture storage array
The visualization server can load lots of images and stores them in one texture array unit on the GPU. All images in the sequence should have the same dimensions.

This filter was developed for slit scan photographic effects.

Yet only one texture storage array is supported.

The index `k` decides from with of 2D images in the array the data will be used. `k` is an integer value used as index into the texture storage array.

| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| k | 1 | select image to read form | (k (- slength sframe))<br>(k (* slength (unoise x y) )) |

| read from | description |
| --- | --- |
| slength | number of images in a texture array.<br>img**s** filters only |
| sframe | current frame counter modulo number of images in a texture array.<br>img**s** filters only |

Assume there are many JPEG files in directory `visual_server/media/1080/`. You can load them with the following statement:

```clojure
(imagesequ "1080/*.jpg")
```
> :warning: This will pause the animation while loading.

Hint:
- This filter has the letter 's' in its name: img**s**. Do not confuse with the image loader described above.
- Use only `c0` as input channels
- Scale all images to the correct size before loading. RAM space on your graphics card is limited and loading time matters during live coding.

##### Example Two filters reading the same texture array
```clojure
(layout "grid" [imgs0] [imgs1])
; assume there are lots of JPEG images in the media folder.
; all imgaes have the same size 1080 x 1080
(imagesequ "1080/*.jpg")
(render)

(hsv :imgs0 [c0]
  (rot (* 0.01 f))
  (k (* slength (unoise (* 0.01 f)
                        (snoise a x))))
  (h c0.h)
  (s c0.s)
  (v (* (- 1 (sqr r)) c0.v))
)
(render 0)

(hsv :imgs1 [c0]
  (tx 0.25) (ty 0.5)
  (sx 2)    (sy 2)
  (k (* slength
        (unoise (* 0.01 f)
                (snoise (* 0.001 f) r))))
  (h c0.h)
  (s c0.s)
  (v (* (- 1 (sqr r)) c0.v))
)
(render 1)
```
![](./live_coding/src/demo/image-sequence/image-sequence.jpg)

### Live stream from a camera
Only a live stream from a single camera is supported yet.

#### HSV processing of a camera input stream
```clojure
(hsv :vid0 [c0] ... ; hsv + vid: this a input stream with HSV expressions
```
##### A example for shifting colors in the input stream.
The marble has small colorful bands.
But the painting is in brown and black on white porcelain.
```clojure
(ns lv.demo)
(use 'lv.core)

; assume a camera is connected
; and can be read by openFrameworks
; on linux gstreamer reads from /dev/video0
(layout "vertical" [vid0])

; shift hue. f = current frame number
(hsv :vid0 [c0]
  (h (+ (* x (sin f)) c0.h))
  (s c0.s)
  (v c0.v)
)
```
![](./stuff/doc/camera-color-shift.jpg)

#### RGB processing of a camera input stream
```clojure
(rgb :vid0 [c0] ... ; rgb + vid: this a input stream with HSV expressions
```

##### A kaleidoscope example.
Only the geometry is deformed.
```clojure
(ns lv.demo)
(use 'lv.core)

; assume a camera is connected
; and can be read by openFrameworks
; on linux gstreamer reads from /dev/video0
(layout "vertical" [vid0])

; Kaleidoscope from camera input
(rgb :vid0 [c0]
  (kaleid 7)
  (rot (* 0.005 f))
  (rgb c0.rgb)
)
```
![](./stuff/doc/camera-kaleidoscope.jpg)


## GPU *fragment* processing filters
### Generate content without using inputs

No input data stream required. Instead you can work with the lisp like expressions only to colorize the pixels.

Expressions in a fragment shader can read from these build in variables:
  - per pixel geometry
    - Cartesian coordinates
      - `x` The pixels x-coordinate
      - `y` The pixels y-coordinate
    - Circle coordinates
      - `r` Distance of the pixel from the center of the canvas
      - `a` Counter clock wise angel.
  - joystick axes
    - `lx` joystick left x axis
    - `ly` joystick left y axis
    - `rx` joystick rigth x axis
    - `ry` joystick rigth y axis
    - `l2` joystick left down axis
    - `r2` joystick rigth down axis
  - microphone input
    - `vol` volume
    - `onset` simple onset detection
    - `f0` fft bin 0
    - `f1` fft bin 1
    - `f2` fft bin 2
    - `f3` fft bin 3
    - `f4` fft bin 4
    - `f5` fft bin 5
    - `f6` fft bin 6
    - `f7` fft bin 7
  - sound player
    - `b0` where we are inside a sound file TODO
  - per filter uniforms
    - `li` TODO
  - per frame uniforms TODO
    - `u0`
    - `u1`
    - `u2`
    - `u3`
    - `u4`
    - `u5`
    - `u6`
    - `u7`

TODO describe coordinate system.

#### Generating HSV color gradients
```clojure
(hsv :ras2 [] ... ; hsv + ras: this a processing filter with HSV expressions
```

##### Color wheel
Example of a simple color wheel from circle coordinates.
```clojure
(ns lv.demo)
(use 'lv.core)

(layout "vertical" [ras0])

; color wheel
(hsv :ras0 []
  ; convert from rotation angle to rainbow colors
  (h (/ a 2 pi))
  ; full saturated
  (s 1)
  ; set to black outside the unite circle.
  (v (pos? (- 1 r) 1 0))
)
```
![](./stuff/doc/generated-color-wheel.jpg)

#### Generating RGB patterns
```clojure
(rgb :ras0 [] ... ; rgb + ras: this is a processing filter for RGB expressions
```
##### Example with moving colored Perlin noise.
```clojure
(ns lv.demo)
(use 'lv.core)

(layout "vertical" [ras0])
; moving colored noise
(rgb :ras0 []
  ; translate in x
  (tx (* 0.001 f))
  ; set red component
  (r (usin (snoise x y)))
  ; translate in x again
  (tx (* 0.005 f))
  ; set green component
  (g (usin (snoise x y)))
  ; translate in x again and again
  (tx (* 0.1 f))
  ; set blue component
  (b (usin (snoise x y)))
)
```
![](./stuff/doc/generated-noise.jpg)

### Reading and processing data from other filters
TODO

```clojure
(hsv :ras3 [img0 ras2] ... ; hsv + ras and inputs:
                           ; this a processing filter for HSV expressions
                           ; reading from other filters
```

```clojure
(rgb :ras3 [img0 ras2] ... ; rgb + ras and inputs:
                           ; this a processing filter for RGB expressions
                           ; reading from other filters
```

##### Example in gray scales by using only the brightness `v`.
```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1] [ras2 ras3])
; show all filters
(render)

; generate animated rings
(hsv :ras0 []
  (v (sin (+ (* 0.05 f)
             (* -50 r))))
)

; generate a rotating star
(hsv :ras1 []
  (rot (* 0.002 f))
  (v (sin (* 50 a)))
)

; mix both upstream filters
(hsv :ras2 [ras0 ras1]
  ; (mirror)
  (kaleid 13)
  (v (* ras0.v ras1.v))
)

; mix ras0 and ras2 controlled by ras1
(hsv :ras3 [ras0 ras1 ras2]
  (v (mix ras0.v ras2.v ras1.v))
)

; only show filter 3
(render 3)
```

```mermaid
graph LR
:ras0 -- ras0 --> :ras2
:ras1 -- ras1 --> :ras2
:ras0 -- ras0 --> :ras3
:ras1 -- ras1 --> :ras3
:ras2 -- ras2 --> :ras3
```

![](./stuff/doc/pipeline-1.jpg)

#### A colorful example.
Same data flow as above, but different expressions.
```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1] [ras2 ras3])
; show all filters
(render)

; generate animated rings
(hsv :ras0 []
  (ty (* 0.002 f))
  (h (+ 0.49 (* 0.05 (snoise y x))))
  (s 1)
  (v (/ (floor (* 7 (snoise x y)))
        7))
)

; generate a rotating star
(hsv :ras1 []
  (rot (* 0.002 f))
  (h (+ 0.05 (* 0.05 (snoise y x))))
  (s 1)
  (v (/ (floor (* 7 (snoise x y)))
        7))
)

; mix both upstream filters
(rgb :ras2 [ras0 ras1]
  (rgb (+ ras0.rgb ras1.rgb))
)

; mix ras0 and ras2 controlled by ras1
(hsv :ras3 [ras0 ras1 ras2]
  (hsv (pos? (snoise (* 0.01 f)
                      (* 5 ras2.v))
              ras0.hsv
              ras1.hsv))
)

(render 3)
```

![](./stuff/doc/pipeline-2.png)

## GPU convolution filters
TODO

##### Example Camera input and very noisy edge detection

```clojure
(matrix :conv1 [vid0]
 ... ; matrix + conv: this a processing filter for convolution kernels
```

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "horizontal" [vid0 conv1
                      conv2 conv3])
(render)

; camera as input stream
(rgb :vid0 [c0]
  (rgb c0.rgb)
)

; Embossing Filter
(matrix :conv1 [vid0]
 -2 -1  0
 -1  1  1
  0  1  2
)

; Edge detection
(matrix :conv2 [con1]
  0  0  0
 -4  8 -4
  0  0  0
)

; Edge detection
(matrix :conv3 [conv2]
  0 -3  0
 -3 12 -3
  0 -3  0
)
```
![](./live_coding/src/demo/convolution/convolution-camera.jpg)

##### Example static image

![](./live_coding/src/demo/convolution/convolution.jpg)

## Color difference between two frames
Calculates the color difference between the current upstream frame and the previous upstream frame. Internally this filter uses rgb color space. This filter did not support parameters.

```clojure
(expr :fdiff1 [ras0])
 ... ; expr + fdiff: this a color difference filter
```
