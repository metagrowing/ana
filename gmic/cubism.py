#!/bin/python3

i_start = 0
i_end   = 210

def prefix(i):
    name = str(i)
    if len(name) == 1: name = "000" + name
    if len(name) == 2: name = "00" + name
    if len(name) == 3: name = "0" + name
    return name
    
def remap(val, src, dst):
    """
    map the given value from the range of src to the range of dst.
    """
    return ((val - src[0]) / (src[1]-src[0])) * (dst[1]-dst[0]) + dst[0]

def stepeffect(i):
    angle0     = remap(i, (i_start, i_end), (0, 60))
    angle1     = remap(i, (i_start, i_end), (0, 120))
    density0   = remap(i, (i_start, i_end), (0, 40))
    density1   = remap(i, (i_start, i_end), (0, 20))
    thickness0 = remap(i, (i_start, i_end), (0.25, 30))
    thickness1 = remap(i, (i_start, i_end), (0.10, 10))
    opacity0   = remap(i, (i_start, i_end), (0, 0.75))
    opacity1   = remap(i, (i_start, i_end), (0, 0.50))
    name = str(i)
    print("gmic -srand 1512 \\")
    print("     -input ~/ana/visual_server/media/Emmi/TheSquaire-FrankfurtAirport/50907192183_8032bffb40_1920x1080.png \\")
    print("     -cubism " + str(density0) + "," + str(thickness0) + "," + str(angle0) + "," + str(opacity0) + " \\")
    print("     -cubism " + str(density1) + "," + str(thickness1) + "," + str(angle0) + "," + str(opacity1) + " \\")
    print("     -output out/" + prefix(i) + ".png")
    print("ls -l out/" + prefix(i) + ".png")

print("#!/bin/bash")
print("timestamp=$(date +%s)")
for a in range(i_start,i_end):
    stepeffect(a)

print("ffmpeg -framerate 30 -pattern_type glob -i 'out/*.png' -vcodec libx265 -preset slow -crf 25 video-$timestamp.mp4")
print("ls -lh *.mp4")
