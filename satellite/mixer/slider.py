#!/usr/bin/python3
import threading
import time
import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib, GObject

from osc4py3.as_eventloop import *
from osc4py3 import oscbuildparse
from osc4py3 import oscmethod as osm

import os.path
import csv

talk_to_host = "127.0.0.1"
talk_to_port = 57220
slider_state = "slider.state"
visual_server       = "visual_server"

osc_vars = {}

def read_state():
    if os.path.isfile(slider_state):
        try:
            with open(slider_state) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    osc_vars[row[0]].slider.set_value(float(row[1]))
                    osc_vars[row[0]].send();

        except IOError as e:
            print("error reading state file " + slider_state)
            print(e)
    else:
        print("state file " + slider_state + " not accessible")
            
def save_state():
    with open(slider_state, "w") as file_object:
        for k, v in osc_vars.items():
            file_object.write(v.name)
            file_object.write(",")
            file_object.write(str(v.slider.get_value()))
            file_object.write("\n")

class Handler:
    def onDeleteWindow(self, *args):
        print("bye")
        osc_terminate()
        Gtk.main_quit(*args)

    def onSendAll(self, widget):
        print("bye")
        for k, v in osc_vars.items():
            v.send();
        osc_process()

    def onSave(self, widget):
        save_state()
        
    def onFrameCountClear(self, widget):
        msg = oscbuildparse.OSCMessage("/ana_f_clear", ",", [])
        osc_send(msg, visual_server)
        osc_process()

class OscSlider():
    def __init__(self, box_container, name, value, min, max):
        self.title = Gtk.Label(name)
        box_container.pack_start(self.title, True, True, 0)
        self.title.show_all()

        adj = Gtk.Adjustment(value=value, lower=min, upper=max, step_increment=0.01, page_increment=0.01, page_size=0)
        self.slider = Gtk.Scale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=adj)
        self.slider.set_digits(5)
        self.slider.set_hexpand(True)
        self.slider.set_valign(Gtk.Align.START)
        self.coid = self.slider.connect("value-changed", self.on_slider_changed)
        box_container.pack_start(self.slider, True, True, 0)
        self.slider.show_all()

        self.name = name
        osc_vars[self.name] = self
        #print(osc_vars)

    def clean(self, box_container):
        self.slider.disconnect(self.coid)
        box_container.remove(self.title)
        box_container.remove(self.slider)
        self.title.destroy
        self.slider.destroy

    def send(self):
        msg = oscbuildparse.OSCMessage("/ana_" + self.name, ",f", [self.slider.get_value()])
        osc_send(msg, visual_server)
        osc_process()
        print("m-> " + self.name + " = " + str(msg))

    def on_slider_changed(self, event):
        self.send()



builder = Gtk.Builder()
builder.add_from_file("slider.glade")
builder.connect_signals(Handler())

sliderBox = builder.get_object("idSliderBox")
OscSlider(sliderBox, "u0", 0, -1, 1)
OscSlider(sliderBox, "u1", 0, -1, 1)
OscSlider(sliderBox, "u2", 0, -1, 1)
OscSlider(sliderBox, "u3", 0, -1, 1)
OscSlider(sliderBox, "u4", 0, -1, 1)
OscSlider(sliderBox, "u5", 0, -1, 1)
OscSlider(sliderBox, "u6", 0, -1, 1)
OscSlider(sliderBox, "u7", 0, -1, 1)

window = builder.get_object("window1")
window.show_all()

osc_startup()
osc_udp_client(talk_to_host, talk_to_port, visual_server)
if len(sys.argv) >= 2:
    slider_state = sys.argv[1]
read_state()

def keep_osc_running():
    osc_process()
    return True

GLib.timeout_add(50, keep_osc_running)

Gtk.main()
